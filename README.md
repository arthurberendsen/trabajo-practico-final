## MoustAPP: Software de gestion de compra/venta de vehiculos usados.

## Trabajo Práctico Final - Ingenieria del Software 4
   Prof. Mauricio Merín

## Integrantes
   - Arturo Berendsen
   - Joaquin Biedermann
   - Gonzalo Propp

## Vista Previa
http://181.120.126.211:8080/ing4/moustapp/

## Pasos para instalar el Sistema

1.	Instalar PostgreSQL 13
2.	Crear una base de datos con el pgAdmin (Nosotros le llamamos moustapp)
3.	Hacer un git clone https://gitlab.com/arthurberendsen/trabajo-practico-final o descargar el .zip y descomprimirlo dentro del correspondiente directorio del servidor web.
4.	Descargar e instalar el Composer (https://getcomposer.org/download/)
5.	Ejecutar dentro del directorio descargado el comando: composer install
6.	Copiar el archivo .env.example a solo .env
7.	Configurar los datos del servidor PostgreSQL
8.	Ejecutar el comando: php artisan key:generate
9.	Ejecutar el comando: php artisan migrate
10.	Ejecutar el comando: php artisan db:seed --class=UsuariosSeeder
11.	Migrar con el pgAdmin 4 los 3 archivos .sql en el siguiente orden:
	11.1. moustapp_db.sql
	11.2. Triggers Ejemplos.sql
	11.3. Moustapp_SQL_datos_prueba.sql


