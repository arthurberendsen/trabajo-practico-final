<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CuotaDetalle extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cuotas_detalle';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'cuota_detalle_id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The model's default values for attributes.
     * 'estado' => true,
     * @var array
     */
    protected $attributes = [

    ];    
}
