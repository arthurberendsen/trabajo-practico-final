<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

use Response,File;

class PerfilesController extends Controller
{

    public function perfil_index($id)
    {
        $usuario = User::findOrFail($id);
        return view('perfiles', ['var' => 'perfil_index', 'usuario' => $usuario]);
    }

    public function perfil_update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'email' => 'required',
        ]);

        $usuario = User::findOrFail($id);

        if (isset($request->password) && isset($request->repassword)) {
            if ($request->password === $request->repassword) {
                $usuario->password = Hash::make($request->password);
            } else {
                return redirect(route('perfil_index', $id))->with('errorMessage', 'Los campos de Contraseña no coinciden. Favor verificar');
            }
        } elseif (empty($request->password) xor empty($request->repassword)) {
            return redirect(route('perfil_index', $id))->with('errorMessage', 'Uno de los campos de Contraseña no coincide. Favor volver a escribir');
        }
        
        $usuario->nombre = $request->nombre;
        $usuario->email = $request->email;
        $usuario->save();
        return redirect(route('perfil_index', $id))->with('successMessage', 'El perfil se ha modificado correctamente.');
    }

    public function avatar_update(Request $request, $id)
    {
        $this->validate($request, [
            'fileUpload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $usuario = User::findOrFail($id);

        $imagen_anterior = Str::after($usuario->avatar, 'public/'); //recorta el public/ del campo
        if (!(Str::endsWith($imagen_anterior, 'no_avatar.png'))){
           File::delete(public_path($imagen_anterior)); 
        }

        $imagen = $request->fileUpload;      
        $destino = 'public/avatar/'; // upload path
        $avatar = date('YmdHis') . "." . $imagen->getClientOriginalExtension();
        $imagen->move($destino, $avatar);
        
        $usuario->avatar = $destino . $avatar;
        $usuario->save();
         
        return redirect(route('perfil_index', $id))->with('successMessage', 'La imagen se ha modificado correctamente.');
 
    }

    public function avatar_destroy($id)
    {
        $usuario = User::findOrFail($id);
        
        $imagen_anterior = Str::after($usuario->avatar, 'public/'); //recorta el public/ del campo
        if (!(Str::endsWith($imagen_anterior, 'no_avatar.png'))){
           File::delete(public_path($imagen_anterior)); 
           $destino = 'public/avatar/'; // directorio de upload
           $usuario->avatar = $destino . 'no_avatar.png';     
        }

        $usuario->save();
        return redirect(route('perfil_index', $id))->with('successMessage', 'La imagen se ha borrado correctamente.');
    }
}