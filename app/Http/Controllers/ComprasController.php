<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use DateTime;

use App\Models\Auditoria;
use App\Models\Proveedor;
use App\Models\FacturaCompra;
use App\Models\Deposito;

use App\Models\Ciudad;
use App\Models\FormaPago;
use App\Models\Cheque;
use App\Models\Cuota;
use App\Models\Color;
use App\Models\CuotaDetalle;
use App\Models\DepositoBancario;
use App\Models\Moneda;
use App\Models\Modelo;
use App\Models\Marca;
use App\Models\Vehiculo;
use App\Models\Recibo;

class ComprasController extends Controller
{
    public function proveedores_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $proveedores = Proveedor::select('*');

        if (!(blank($buscar))) {
            $proveedores = $proveedores->orWhere('proveedores.razon_social', 'ILIKE', '%'. $buscar . '%')
                                        ->orWhere('proveedores.ruc_ci', 'ILIKE', '%'. $buscar . '%')
                                        ->orWhere('proveedores.telefono', 'ILIKE', '%'. $buscar . '%')
                                        ->orWhere('proveedores.correo', 'ILIKE', '%'. $buscar . '%');
        }

        $proveedores = $proveedores->orderBy('proveedor_id')->paginate($items);
        return view('compras', ['var' => 'proveedores_index', 'proveedores' => $proveedores, 'items' => $items, 'buscar' => $buscar]);
    }

    public function proveedores_create()
    {
        $ciudades = Ciudad::all()->sortBy('nombre');
        return view('compras', ['var' => 'proveedores_create', 'ciudades' => $ciudades]);
    }

    public function proveedores_store(Request $request)
    {
        $this->validate($request, [
            'razon_social' => 'required',
            'ruc_ci' => 'required',
            'ciudad_id' => 'required'
        ]);

        $proveedor = new Proveedor;
        $proveedor->razon_social = $request->razon_social;
        $proveedor->ruc_ci = $request->ruc_ci;
        $proveedor->direccion = $request->direccion;
        $proveedor->ciudad_id = $request->ciudad_id;
        $proveedor->telefono = $request->telefono;
        $proveedor->correo = $request->correo;
        $proveedor->observacion = $request->observacion;
        $proveedor->estado = True;
        $proveedor->save();

        $this->auditoria($proveedor, 'I');
        return redirect(route('proveedores_index'))->with('successMessage', 'Proveedor añadido correctamente.');
    }

    public function proveedores_show($id)
    {
        $proveedor = Proveedor::findOrFail($id);
        $ciudad = Ciudad::findOrFail($proveedor->ciudad_id);
        return view('compras', ['var' => 'proveedores_show', 'proveedor' => $proveedor, 'ciudad' => $ciudad]);
    }

    public function proveedores_edit($id)
    {
        $proveedor = Proveedor::findOrFail($id);
        $ciudades = Ciudad::all()->sortBy('nombre');
        return view('compras', ['var' => 'proveedores_edit', 'proveedor' => $proveedor, 'ciudades' => $ciudades]);
    }

    public function proveedores_update(Request $request, $id)
    {
        $this->validate($request, [
            'razon_social' => 'required',
            'ruc_ci' => 'required',
            'ciudad_id' => 'required'
        ]);

        $proveedor = Proveedor::findOrFail($id);
        $viejo = Proveedor::findOrFail($id);
        $proveedor->razon_social = $request->razon_social;
        $proveedor->ruc_ci = $request->ruc_ci;
        $proveedor->direccion = $request->direccion;
        $proveedor->ciudad_id = $request->ciudad_id;
        $proveedor->telefono = $request->telefono;
        $proveedor->correo = $request->correo;
        $proveedor->observacion = $request->observacion;
        $proveedor->estado = $request->boolean('estado');
        $proveedor->save();

        $this->auditoria($proveedor, 'U', $viejo);
        return redirect(route('proveedores_index'))->with('successMessage', 'Proveedor actualizado correctamente.');
    }

    public function proveedores_destroy($id)
    {
        try { 
            $proveedor = Proveedor::findOrFail($id);
            $proveedor->delete();
            $this->auditoria($proveedor, 'D');
            return redirect(URL::previous())->with('successMessage', 'Proveedor borrado correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'El Proveedor no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }


    public function recibosc_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $recibos = DB::table('recibos as r')
        ->select(['r.recibo_id', 'r.numero', 'r.fecha', 'r.estado', 'fp.monto', 'cd.vencimiento', 'fp.nombre', 'p.razon_social', 'fc.factura_nro', 'm.sigla'])
        ->leftJoin('cuotas_detalle as cd', function($join){
            $join->on('cd.forma_pago_id', '=', 'r.forma_pago_id');
            $join->on('cd.cuota_detalle_id','=','r.cuota_detalle_id'); 
        })
        ->leftJoin('formas_pagos as fp', 'r.recibo_forma_pago_id', '=', 'fp.forma_pago_id')
        ->leftJoin('facturas_compras as fc', 'fc.forma_pago_id', '=', 'r.forma_pago_id')
        ->leftJoin('proveedores as p', 'p.proveedor_id', '=', 'fc.proveedor_id')
        ->leftJoin('monedas as m', 'm.moneda_id', '=', 'r.moneda_id');

        if (!(blank($buscar))) {
            $recibos = $recibos->orWhere('r.numero', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('p.razon_social', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('fc.factura_nro', 'ILIKE', '%'. $buscar . '%');
        }

        if ($this->validarFecha($buscar, 'd/m/Y')) {
            $buscar_fecha = str_replace('/', '-', $buscar); // Truco para que haga bien la conversion a Y-m-d
            $buscar_fecha = date('Y-m-d', strtotime($buscar_fecha));
            $recibos = $recibos->orWhere('r.fecha', 'ILIKE', '%'. $buscar_fecha . '%')
                            ->orWhere('cd.vencimiento', 'ILIKE', '%'. $buscar_fecha . '%');
        }

        $recibos = $recibos->whereNotNull('p.razon_social')
                            ->orderBy("r.recibo_id")->paginate($items);

        return view('compras', ['var' => 'recibosc_index', 'recibos' => $recibos, 'items' => $items, 'buscar' => $buscar]);
    }

    public function recibosc_create()
    {
        $proveedores = DB::table('proveedores as c')
        ->select(['c.razon_social', 'fc.proveedor_id'])
        ->leftJoin('facturas_compras as fc', 'fc.proveedor_id', '=', 'c.proveedor_id')
        ->where('fc.es_contado', False)->get();

        $facturasCompras = DB::table('facturas_compras as fc')
        ->select(['fc.proveedor_id', 'fc.factura_nro', 'fc.fecha_factura', 'fc.moneda_id', 'cu.cantidad_cuotas', 'cd.*'])
        ->leftJoin('cuotas as cu', 'fc.forma_pago_id', '=', 'cu.forma_pago_id')
        ->leftJoin('cuotas_detalle as cd', 'cd.forma_pago_id', '=', 'cu.forma_pago_id')
        ->where('fc.es_contado', False)
        ->where('fc.estado', True)
        ->where('cd.esta_pagado', False)
        ->orderBy('fc.factura_compra_id')
        ->orderBy('cd.forma_pago_id')
        ->orderBy('cd.cuota_detalle_id')
        ->get();

        $monedas = Moneda::all()->sortBy('nombre');
        
        return view('compras', ['var' => 'recibosc_create', 'facturasCompras' => $facturasCompras, 'proveedores' => $proveedores, 'monedas' => $monedas]);
    }

     public function recibosc_store(Request $request)
    {
        $this->validate($request, [

            'numero' => 'required',
            'fecha' => 'required',
            'moneda_id' => 'required'
        ]);

        $monto_limpio = Str::of($request->monto)->replace(',', '-')->replace('.', '')->replace('-', '.');

        $recibo = new Recibo;
        $recibo->numero = $request->numero;
        $recibo->fecha = $request->fecha;
        $recibo->moneda_id = $request->moneda_id;
        $recibo->forma_pago_id = $request->forma_pago_id;
        $recibo->cuota_detalle_id = $request->cuota_detalle_id;
        $recibo->estado = True;

        $forma_pago_seleccionada = $request->forma_pago_nombre;
        
        $formaPago = new FormaPago;
        $formaPago->monto = $monto_limpio;

        switch($forma_pago_seleccionada) {
            case 'efectivo':
                $formaPago->nombre = 'Efectivo';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                break;

            case 'cheques':
                $formaPago->nombre = 'Cheque';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                $cheque = new Cheque;
                $cheque->forma_pago_id = $formaPago->forma_pago_id;
                $cheque->numero_cheque = $request->numero_cheque;
                $cheque->fecha_cheque = $request->fecha_cheque;
                $cheque->banco = $request->banco;
                $cheque->save();
                $this->auditoria($cheque, 'I');
                break;

            case 'depositos_bancarios':
                $formaPago->nombre = 'DepositoBancario';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                $depositoBancario = new DepositoBancario;
                $depositoBancario->forma_pago_id = $formaPago->forma_pago_id;
                $depositoBancario->numero_transaccion = $request->numero_transaccion;
                $depositoBancario->fecha_deposito = $request->fecha_deposito;
                $depositoBancario->banco_receptor = $request->banco_receptor;
                $depositoBancario->save();
                $this->auditoria($depositoBancario, 'I');
                break;

            default:
                //default;
        };

        $recibo->recibo_forma_pago_id = $formaPago->forma_pago_id;
        $recibo->save();

        $this->auditoria($recibo, 'I');
        return redirect(route('recibosc_index'))->with('successMessage', 'Recibo añadido correctamente.');
    }

    public function recibosc_show($id)
    {
        $recibo = Recibo::findOrFail($id);
        //$cuotaDetalle = CuotaDetalle::firstWhere('forma_pago_id', $formaPago->forma_pago_id);
        $facturaVenta = DB::table('facturas_compras as fc')
        ->select(['fc.factura_nro', 'fc.fecha_factura', 'fc.proveedor_id', 'p.razon_social as razon_social', 'cu.cantidad_cuotas', 'cd.cuota_detalle_id', 'cd.monto_cuota', 'cd.vencimiento', 'm.sigla'])
        ->leftJoin('monedas as m', 'm.moneda_id', '=', 'fc.moneda_id')
        ->leftJoin('recibos as r', 'r.forma_pago_id', '=', 'fc.forma_pago_id')
        ->leftJoin('proveedores as p', 'p.proveedor_id', '=', 'fc.proveedor_id')
        ->leftJoin('cuotas as cu', 'cu.forma_pago_id', '=', 'fc.forma_pago_id')
        ->leftJoin('cuotas_detalle as cd', 'cd.forma_pago_id', '=', 'cu.forma_pago_id')
        ->where('r.recibo_id', $id)
        ->first();;

        $moneda = Moneda::find($recibo->moneda_id);

        return view('compras', ['var' => 'recibosc_show', 'facturaVenta' => $facturaVenta, 'recibo' => $recibo, 'moneda' => $moneda]);
    }

    public function recibosc_destroy($id)
    {
         try {
            $recibo = Recibo::findOrFail($id); // Captura el valor de la forma_pago_id antes de borrar la Factura.
            $recibo->delete();
            $this->auditoria($recibo, 'D');
            $forma_pago = FormaPago::findOrFail($recibo->recibo_forma_pago_id);
            $forma_pago->delete();
            $this->auditoria($forma_pago, 'D');
            return redirect(URL::previous())->with('successMessage', 'El Recibo fue borrado correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'El Recibo no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }

    public function recibosc_disable($id)
    {
        try {
            $recibo = Recibo::findOrFail($id); // Captura el valor de la forma_pago_id antes de borrar la Factura.
            $v_estado = $recibo->estado;

            if ($v_estado) {
                $recibo->estado = False;
                $recibo->save();
                return redirect(route('recibosc_index'))->with('successMessage', 'El Recibo fue anulado correctamente.');
            } else {
                $recibo->estado = True;
                $recibo->save();
                return redirect(route('recibosc_index'))->with('successMessage', 'El Recibo fue des-anulado correctamente.');
            }
            
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(route('recibosc_index'))->with('errorMessage', 'El Recibo no se puede modificar.');
        }
    }

    public function cuotasc_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $cuotas = DB::table('cuotas as c')
        ->select(['c.forma_pago_id', 'p.razon_social', 'facturas_compras.factura_nro', 'facturas_compras.fecha_factura', 'cuotas_detalle.monto_cuota', 'c.cantidad_cuotas', 'cuotas_detalle.vencimiento', 'cuotas_detalle.esta_pagado', 'cuotas_detalle.cuota_detalle_id', 'monedas.sigla', 'monedas.nombre'])
        ->leftJoin('formas_pagos', 'c.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('facturas_compras', 'facturas_compras.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('monedas', 'monedas.moneda_id', '=', 'facturas_compras.moneda_id')
        ->leftJoin('proveedores as p', 'facturas_compras.proveedor_id', '=', 'p.proveedor_id')
        ->leftJoin('cuotas_detalle', 'cuotas_detalle.forma_pago_id', '=', 'c.forma_pago_id');

        if (!(blank($buscar))) {
            $cuotas = $cuotas->orWhere('p.razon_social', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('facturas_compras.factura_nro', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('cuotas_detalle.cuota_detalle_id', 'ILIKE', '%'. $buscar . '%');
        }

        if ($this->validarFecha($buscar, 'd/m/Y')) {
            $buscar_fecha = str_replace('/', '-', $buscar); // Truco para que haga bien la conversion a Y-m-d
            $buscar_fecha = date('Y-m-d', strtotime($buscar_fecha));
            $cuotas = $cuotas->orWhere('facturas_compras.fecha_factura', 'ILIKE', '%'. $buscar_fecha . '%')
                            ->orWhere('cuotas_detalle.vencimiento', 'ILIKE', '%'. $buscar_fecha . '%');
        }

        $cuotas = $cuotas->whereNotNull('p.razon_social')
        ->orderBy("c.forma_pago_id")->paginate($items);

        return view('compras', ['var' => 'cuotasc_index', 'cuotas' => $cuotas, 'items' => $items, 'buscar' => $buscar]);
    }

    public function cuotasc_show($f_pago_id, $c_detalle_id)
    {
        $cuota = DB::table('cuotas as c')
        ->select(['c.forma_pago_id', 'proveedores.razon_social', 'facturas_compras.factura_nro', 'facturas_compras.fecha_factura', 'cuotas_detalle.monto_cuota', 'c.cantidad_cuotas', 'cuotas_detalle.vencimiento', 'cuotas_detalle.esta_pagado', 'cuotas_detalle.cuota_detalle_id', 'monedas.sigla', 'monedas.nombre'])
        ->leftJoin('formas_pagos', 'c.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('facturas_compras', 'facturas_compras.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('monedas', 'monedas.moneda_id', '=', 'facturas_compras.moneda_id')
        ->leftJoin('proveedores', 'facturas_compras.proveedor_id', '=', 'proveedores.proveedor_id')
        ->leftJoin('cuotas_detalle', 'cuotas_detalle.forma_pago_id', '=', 'c.forma_pago_id')
        ->where('c.forma_pago_id', $f_pago_id)
        ->where('cuotas_detalle.cuota_detalle_id', $c_detalle_id)
        ->first();
        
        return view('compras', ['var' => 'cuotasc_show', 'cuota' => $cuota]);
    }

    public function cuotasc_edit($f_pago_id, $c_detalle_id)
    {
        $cuota = DB::table('cuotas as c')
        ->select(['c.forma_pago_id', 'proveedores.razon_social', 'facturas_compras.factura_nro', 'facturas_compras.fecha_factura', 'cuotas_detalle.monto_cuota', 'c.cantidad_cuotas', 'cuotas_detalle.vencimiento', 'cuotas_detalle.esta_pagado', 'cuotas_detalle.cuota_detalle_id', 'monedas.sigla', 'monedas.nombre'])
        ->leftJoin('formas_pagos', 'c.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('facturas_compras', 'facturas_compras.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('monedas', 'monedas.moneda_id', '=', 'facturas_compras.moneda_id')
        ->leftJoin('proveedores', 'facturas_compras.proveedor_id', '=', 'proveedores.proveedor_id')
        ->leftJoin('cuotas_detalle', 'cuotas_detalle.forma_pago_id', '=', 'c.forma_pago_id')
        ->where('c.forma_pago_id', $f_pago_id)
        ->where('cuotas_detalle.cuota_detalle_id', $c_detalle_id)
        ->first();

        return view('compras', ['var' => 'cuotasc_edit', 'cuota' => $cuota]);
    }

    public function cuotasc_update(Request $request, $f_pago_id, $c_detalle_id)
    {
        $this->validate($request, [
            'fecha_vencimiento' => 'required'
        ]);

        $v_fecha = str_replace('/', '-', $request->fecha_vencimiento); // Truco para que haga bien la conversion a Y-m-d
        $v_fecha = date('Y-m-d', strtotime($v_fecha));

        $viejo = CuotaDetalle::select(['cuotas_detalle.cuota_detalle_id', 'cuotas_detalle.vencimiento'])
        ->where('cuotas_detalle.forma_pago_id', $f_pago_id)
        ->where('cuotas_detalle.cuota_detalle_id', $c_detalle_id)->get();

        $cuota_update = CuotaDetalle::select(['cuotas_detalle.cuota_detalle_id', 'cuotas_detalle.vencimiento'])
        ->where('cuotas_detalle.forma_pago_id', $f_pago_id)
        ->where('cuotas_detalle.cuota_detalle_id', $c_detalle_id)
        ->update(['vencimiento' => $v_fecha]);
        
        $cuota = CuotaDetalle::select(['cuotas_detalle.cuota_detalle_id', 'cuotas_detalle.vencimiento'])
        ->where('cuotas_detalle.forma_pago_id', $f_pago_id)
        ->where('cuotas_detalle.cuota_detalle_id', $c_detalle_id)->get();

        $this->auditoria($cuota[0], 'U', $viejo[0]);
        return redirect(route('cuotasc_index'))->with('successMessage', 'Cuota actualizada correctamente.');
    }

    public function facturasc_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $facturasCompra = DB::table('facturas_compras as fc')->select(['fc.factura_compra_id','fc.factura_nro','proveedores.razon_social','fc.fecha_factura', 'formas_pagos.nombre','fc.precio_compra', 'monedas.sigla','fc.estado', 'marcas.nombre as marca', 'modelos.nombre as modelo', 'vehiculos.anho_fabricacion as anho'])
        ->leftJoin('proveedores', 'fc.proveedor_id', '=', 'proveedores.proveedor_id')
        ->leftJoin('monedas', 'fc.moneda_id', '=', 'monedas.moneda_id')
        ->leftJoin('formas_pagos', 'fc.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('vehiculos', 'fc.vehiculo_id', 'vehiculos.vehiculo_id')
        ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
        ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id');;

        if (!(blank($buscar))) {
            $facturasCompra = $facturasCompra->orWhere('fc.factura_nro', 'ILIKE', '%'. $buscar . '%')
                                            ->orWhere('proveedores.razon_social', 'ILIKE', '%'. $buscar . '%')
                                            ->orWhere('formas_pagos.nombre', 'ILIKE', '%'. $buscar . '%')
                                            ->orWhere('marcas.nombre', 'ILIKE', '%'. $buscar . '%')
                                            ->orWhere('modelos.nombre', 'ILIKE', '%'. $buscar . '%')
                                            ->orWhere('vehiculos.anho_fabricacion', 'ILIKE', '%'. $buscar . '%');
        }

        if ($this->validarFecha($buscar, 'd/m/Y')) {
            $buscar_fecha = str_replace('/', '-', $buscar); // Truco para que haga bien la conversion a Y-m-d
            $buscar_fecha = date('Y-m-d', strtotime($buscar_fecha));
            $facturasCompra = $facturasCompra->orWhere('fc.fecha_factura', 'ILIKE', '%'. $buscar_fecha . '%');
        }

        $facturasCompra = $facturasCompra->orderBy("fc.factura_compra_id")->paginate($items);

        return view('compras', ['var' => 'facturasc_index', 'facturasCompra' => $facturasCompra, 'items' => $items, 'buscar' => $buscar]);
    }

    public function facturasc_create()
    {
        $vehiculos = Vehiculo::select(['vehiculos.vehiculo_id as vehiculo_id', 'modelos.nombre as modelo', 'marcas.nombre as marca', 'colores.nombre as color', 'vehiculos.anho_fabricacion as anho', 'vehiculos.chasis as chasis', 'monedas.moneda_id as moneda_id', 'monedas.cotizacion as cotizacion', 'vehiculos.estado as estado'])
                     ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
                     ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
                     ->leftJoin('monedas', 'monedas.moneda_id', '=', 'vehiculos.moneda_id')
                     ->leftJoin('colores', 'colores.color_id', '=', 'vehiculos.color_id')
                     ->where('vehiculos.estado', '=', False)
                     ->where('vehiculos.costo', '=', Null)
                     ->where('vehiculos.moneda_id', '=', Null)
                     ->orderBy('marca')->get();
        $proveedores = Proveedor::all()->where('estado', '=', True)->sortBy('razon_social');
        $monedas = Moneda::all()->sortBy('nombre');
        $depositos = Deposito::all()->where('estado', '=', True)->sortBy('nombre');

        $facturaCompra = FacturaCompra::all();
        return view('compras', ['var' => 'facturasc_create', 'facturaCompra' => $facturaCompra, 'proveedores' => $proveedores, 'depositos' => $depositos, 'monedas' => $monedas, 'vehiculos' => $vehiculos]);
    }

     public function facturasc_store(Request $request)
    {
        $this->validate($request, [

            'factura_nro' => 'required',
            'proveedor_id' => 'required',
            'deposito_id' => 'required',
            'es_contado' => 'required',
            'fecha_factura' => 'required',
            'vehiculo_id' => 'required',
            'precio_compra' => 'required',
            'moneda_id' => 'required'
        ]);

        $monto_limpio = Str::of($request->monto)->replace(',', '-')->replace('.', '')->replace('-', '.');

        $facturaCompra = new FacturaCompra;
        $facturaCompra->factura_nro = $request->factura_nro;
        $facturaCompra->proveedor_id = $request->proveedor_id;
        $facturaCompra->fecha_factura = $request->fecha_factura;
        $facturaCompra->fecha_vencimiento = $request->fecha_vencimiento;
        $facturaCompra->es_contado = $request->boolean('es_contado');
        $facturaCompra->vehiculo_id = $request->vehiculo_id;
        $facturaCompra->precio_compra = $monto_limpio;
        $facturaCompra->moneda_id = $request->moneda_id;
        $facturaCompra->estado = True;

        $forma_pago_seleccionada = $request->forma_pago_nombre;
        
        $formaPago = new FormaPago;
        $formaPago->monto = $monto_limpio;

        switch($forma_pago_seleccionada) {
            case 'efectivo':
                $formaPago->nombre = 'Efectivo';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                break;

            case 'cheques':
                $formaPago->nombre = 'Cheque';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                $cheque = new Cheque;
                $cheque->forma_pago_id = $formaPago->forma_pago_id;
                $cheque->numero_cheque = $request->numero_cheque;
                $cheque->fecha_cheque = $request->fecha_cheque;
                $cheque->banco = $request->banco;
                $cheque->save();
                $this->auditoria($cheque, 'I');
                break;

            case 'depositos_bancarios':
                $formaPago->nombre = 'DepositoBancario';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                $depositoBancario = new DepositoBancario;
                $depositoBancario->forma_pago_id = $formaPago->forma_pago_id;
                $depositoBancario->numero_transaccion = $request->numero_transaccion;
                $depositoBancario->fecha_deposito = $request->fecha_deposito;
                $depositoBancario->banco_receptor = $request->banco_receptor;
                $depositoBancario->save();
                $this->auditoria($depositoBancario, 'I');
                break;

            case 'cuotas':
                $formaPago->nombre = 'Cuotas';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                $cuota = new Cuota;
                $cuota->forma_pago_id = $formaPago->forma_pago_id;
                $cuota->cantidad_cuotas = $request->cantidad_cuotas;
                $cuota->save();
                $this->auditoria($cuota, 'I');
                
                $v_total = (string)$monto_limpio;
                $v_cantidad_cuotas = $request->cantidad_cuotas; // Cantidad de Cuotas
                $v_cuota = intdiv($v_total, $v_cantidad_cuotas); // Division entera
                $v_diferencia = $v_total % $v_cantidad_cuotas;

                $v_fecha = str_replace('/', '-', $facturaCompra->fecha_factura); // Truco para que haga bien la conversion a Y-m-d
                $v_fecha = date('Y-m-d', strtotime($v_fecha));
                //echo $v_fecha;

                for ($i = 1; $i < $v_cantidad_cuotas; $i++) {
                    $cuotaDetalle = new CuotaDetalle;
                    $cuotaDetalle->cuota_detalle_id = $i;
                    $cuotaDetalle->forma_pago_id = $formaPago->forma_pago_id;
                    $cuotaDetalle->monto_cuota = $v_cuota;
                    $cuotaDetalle->vencimiento = date('Y-m-d', strtotime($v_fecha. ' + '. $i . 'months'));
                    $cuotaDetalle->esta_pagado = False;
                    $cuotaDetalle->save();
                    $this->auditoria($cuotaDetalle, 'I');
                }

                $cuotaDetalle = new CuotaDetalle;
                $cuotaDetalle->cuota_detalle_id = $v_cantidad_cuotas;
                $cuotaDetalle->forma_pago_id = $formaPago->forma_pago_id;
                $cuotaDetalle->monto_cuota = $v_cuota + $v_diferencia;
                $cuotaDetalle->vencimiento = date('Y-m-d', strtotime($v_fecha. ' + '. $v_cantidad_cuotas . 'months'));
                $cuotaDetalle->esta_pagado = False;
                $cuotaDetalle->save();
                $this->auditoria($cuotaDetalle, 'I');

                break;
            
            default:
                //default;
        };

        $facturaCompra->forma_pago_id = $formaPago->forma_pago_id;
        $facturaCompra->save();
        $this->auditoria($facturaCompra, 'I');

        $vehiculo = Vehiculo::find($request->vehiculo_id);
        $viejo = Vehiculo::find($request->vehiculo_id);
        $vehiculo->deposito_id = $request->deposito_id;
        $vehiculo->save();
        $this->auditoria($vehiculo, 'U', $viejo);

        return redirect(route('facturasc_index'))->with('successMessage', 'Factura de Compra añadida correctamente.');
    }

    public function facturasc_show($id)
    {
        $facturaCompra = FacturaCompra::findOrFail($id);
        $proveedor = Proveedor::find($facturaCompra->proveedor_id);
        $moneda = Moneda::find($facturaCompra->moneda_id);
        $vehiculo = Vehiculo::find($facturaCompra->vehiculo_id);
        $deposito = Deposito::find($vehiculo->deposito_id);
        $modelo = Modelo::find($vehiculo->modelo_id);
        $marca = Marca::find($modelo->marca_id);
        $color = Color::find($vehiculo->color_id);
        $moneda = Moneda::find($facturaCompra->moneda_id);
        $formaPago = FormaPago::find($facturaCompra->forma_pago_id);
        $cheque = Cheque::find($formaPago->forma_pago_id);        
        $depositoBancario = DepositoBancario::find($formaPago->forma_pago_id);
        $cuota = Cuota::find($formaPago->forma_pago_id);
        $cuotaDetalle = CuotaDetalle::firstWhere('forma_pago_id', $formaPago->forma_pago_id);

        return view('compras', ['var' => 'facturasc_show', 'facturaCompra' => $facturaCompra, 'proveedor' => $proveedor, 'moneda' => $moneda, 'vehiculo' => $vehiculo, 'deposito' => $deposito, 'marca' => $marca, 'modelo' => $modelo, 'color' => $color, 'moneda' => $moneda, 'formaPago' => $formaPago, 'cheque' => $cheque, 'depositoBancario' => $depositoBancario, 'cuota' => $cuota, 'cuotaDetalle' => $cuotaDetalle]);
    }

    public function facturasc_destroy($id)
    {
        try {
            $factura_compra = FacturaCompra::findOrFail($id); // Captura el valor de la forma_pago_id antes de borrar la Factura.
            $factura_compra->delete();
            $this->auditoria($factura_compra, 'D');
            $forma_pago = FormaPago::findOrFail($factura_compra->forma_pago_id);
            $forma_pago->delete();
            $this->auditoria($forma_pago, 'D');
            return redirect(URL::previous())->with('successMessage', 'La Factura de Compra fue borrada correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'La Factura de Compra no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }

    public function facturasc_disable($id)
    {
        try {
            $factura_compra = FacturaCompra::findOrFail($id); // Captura el valor de la forma_pago_id antes de borrar la Factura.
            $v_estado = $factura_compra->estado;

            if ($v_estado) {
                $factura_compra->estado = False;
                $factura_compra->save();
                return redirect(route('facturasc_index'))->with('successMessage', 'La Factura de Compra fue anulada correctamente.');
            } else {
                $factura_compra->estado = True;
                $factura_compra->save();
                return redirect(route('facturasc_index'))->with('successMessage', 'La Factura de Compra fue des-anulada correctamente.');
            }
            
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(route('facturasc_index'))->with('errorMessage', 'La Factura de Compra no se puede modificar.');
        }
    }

    public function auditoria($objeto, $tipo_transaccion, $objeto_viejo = null)
    {
        if ($tipo_transaccion == 'I') {
            $atributos = $objeto->attributesToArray();
            
            foreach ($atributos as $clave => $atributo) {
                if ($atributo !== $objeto->getKey()) {
                    $auditoria = new Auditoria;
                    $auditoria->tipo_transaccion = 'I';
                    $auditoria->tabla = $objeto->getTable();
                    $auditoria->tabla_pk = $objeto->getKey();
                    $auditoria->campo = $clave;
                    $auditoria->valor_nuevo = $atributo;
                    $auditoria->fecha = now();
                    $auditoria->usuario_id = Auth::user()->id;
                    $auditoria->save();
                }
            }
        }
        
        if ($tipo_transaccion == 'D') {
            $atributos = $objeto->attributesToArray();
            
            foreach ($atributos as $clave => $atributo) {
                if ($atributo !== $objeto->getKey()) {
                    $auditoria = new Auditoria;
                    $auditoria->tipo_transaccion = 'D';
                    $auditoria->tabla = $objeto->getTable();
                    $auditoria->tabla_pk = $objeto->getKey();
                    $auditoria->campo = $clave;
                    $auditoria->valor_viejo = $atributo;
                    $auditoria->fecha = now();
                    $auditoria->usuario_id = Auth::user()->id;
                    $auditoria->save();
                }
            }
        }

        if ($tipo_transaccion == 'U') {
            $atributos = $objeto->attributesToArray();
            
            foreach ($atributos as $clave => $atributo) {
                if ($atributo !== $objeto->getKey()) {
                    if ($objeto_viejo[$clave] != $atributo) {
                        $auditoria = new Auditoria;
                        $auditoria->tipo_transaccion = 'U';
                        $auditoria->tabla = $objeto->getTable();
                        $auditoria->tabla_pk = $objeto->getKey();
                        $auditoria->campo = $clave;
                        $auditoria->valor_viejo = $objeto_viejo[$clave];
                        $auditoria->valor_nuevo = $atributo;
                        $auditoria->fecha = now();
                        $auditoria->usuario_id = Auth::user()->id;
                        $auditoria->save();
                    }
                }
            }
        }

    }

    function validarFecha($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

}
