<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use PDF;
use Luecano\NumeroALetras\NumeroALetras;
use DateTime;


use App\Models\Auditoria;
use App\Models\Cliente;
use App\Models\Ciudad;
use App\Models\FacturaVenta;
use App\Models\FormaPago;
use App\Models\Cheque;
use App\Models\Cuota;
use App\Models\Color;
use App\Models\CuotaDetalle;
use App\Models\DepositoBancario;
use App\Models\Moneda;
use App\Models\Modelo;
use App\Models\Marca;
use App\Models\Vehiculo;
use App\Models\Recibo;

class VentasController extends Controller
{
    public function recibosv_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $recibos = DB::table('recibos as r')
        ->select(['r.recibo_id', 'r.numero', 'r.fecha', 'r.estado', 'fp.monto', 'cd.vencimiento', 'fp.nombre', 'c.razon_social', 'fv.factura_nro', 'm.sigla'])
        ->leftJoin('cuotas_detalle as cd', function($join){
            $join->on('cd.forma_pago_id', '=', 'r.forma_pago_id');
            $join->on('cd.cuota_detalle_id','=','r.cuota_detalle_id'); 
        })
        ->leftJoin('formas_pagos as fp', 'r.recibo_forma_pago_id', '=', 'fp.forma_pago_id')
        ->leftJoin('facturas_ventas as fv', 'fv.forma_pago_id', '=', 'r.forma_pago_id')
        ->leftJoin('clientes as c', 'c.cliente_id', '=', 'fv.cliente_id')
        ->leftJoin('monedas as m', 'm.moneda_id', '=', 'r.moneda_id');

        if (!(blank($buscar))) {
            $recibos = $recibos->orWhere('r.numero', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('c.razon_social', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('fv.factura_nro', 'ILIKE', '%'. $buscar . '%');
        }

        if ($this->validarFecha($buscar, 'd/m/Y')) {
            $buscar_fecha = str_replace('/', '-', $buscar); // Truco para que haga bien la conversion a Y-m-d
            $buscar_fecha = date('Y-m-d', strtotime($buscar_fecha));
            $recibos = $recibos->orWhere('r.fecha', 'ILIKE', '%'. $buscar_fecha . '%')
                            ->orWhere('cd.vencimiento', 'ILIKE', '%'. $buscar_fecha . '%');
        }

        $recibos = $recibos->whereNotNull('c.razon_social')
                            ->orderBy("r.recibo_id")->paginate($items);

        return view('ventas', ['var' => 'recibosv_index', 'recibos' => $recibos, 'items' => $items, 'buscar' => $buscar]);
    }

    public function recibosv_create()
    {
        $clientes = DB::table('clientes as c')
        ->select(['c.razon_social', 'fv.cliente_id'])
        ->leftJoin('facturas_ventas as fv', 'fv.cliente_id', '=', 'c.cliente_id')
        ->where('fv.es_contado', False)->get();

        $facturasVentas = DB::table('facturas_ventas as fv')
        ->select(['fv.cliente_id', 'fv.factura_nro', 'fv.fecha_factura', 'fv.moneda_id', 'cu.cantidad_cuotas', 'cd.*'])
        ->leftJoin('cuotas as cu', 'fv.forma_pago_id', '=', 'cu.forma_pago_id')
        ->leftJoin('cuotas_detalle as cd', 'cd.forma_pago_id', '=', 'cu.forma_pago_id')
        ->where('fv.es_contado', False)
        ->where('fv.estado', True)
        ->where('cd.esta_pagado', False)
        ->orderBy('fv.factura_venta_id')
        ->orderBy('cd.forma_pago_id')
        ->orderBy('cd.cuota_detalle_id')
        ->get();

        $monedas = Moneda::all()->sortBy('nombre');
        
        return view('ventas', ['var' => 'recibosv_create', 'facturasVentas' => $facturasVentas, 'clientes' => $clientes, 'monedas' => $monedas]);
    }

     public function recibosv_store(Request $request)
    {
        $this->validate($request, [

            'numero' => 'required',
            'fecha' => 'required',
            'moneda_id' => 'required'
        ]);

        $monto_limpio = Str::of($request->monto)->replace(',', '-')->replace('.', '')->replace('-', '.');

        $recibo = new Recibo;
        $recibo->numero = $request->numero;
        $recibo->fecha = $request->fecha;
        $recibo->moneda_id = $request->moneda_id;
        $recibo->forma_pago_id = $request->forma_pago_id;
        $recibo->cuota_detalle_id = $request->cuota_detalle_id;
        $recibo->estado = True;

        $forma_pago_seleccionada = $request->forma_pago_nombre;
        
        $formaPago = new FormaPago;
        $formaPago->monto = $monto_limpio;

        switch($forma_pago_seleccionada) {
            case 'efectivo':
                $formaPago->nombre = 'Efectivo';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                break;

            case 'cheques':
                $formaPago->nombre = 'Cheque';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                $cheque = new Cheque;
                $cheque->forma_pago_id = $formaPago->forma_pago_id;
                $cheque->numero_cheque = $request->numero_cheque;
                $cheque->fecha_cheque = $request->fecha_cheque;
                $cheque->banco = $request->banco;
                $cheque->save();
                $this->auditoria($cheque, 'I');
                break;

            case 'depositos_bancarios':
                $formaPago->nombre = 'DepositoBancario';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                $depositoBancario = new DepositoBancario;
                $depositoBancario->forma_pago_id = $formaPago->forma_pago_id;
                $depositoBancario->numero_transaccion = $request->numero_transaccion;
                $depositoBancario->fecha_deposito = $request->fecha_deposito;
                $depositoBancario->banco_receptor = $request->banco_receptor;
                $depositoBancario->save();
                $this->auditoria($depositoBancario, 'I');
                break;

            default:
                //default;
        };

        $recibo->recibo_forma_pago_id = $formaPago->forma_pago_id;
        $recibo->save();
        $this->auditoria($recibo, 'I');

        return redirect(route('recibosv_index'))->with('successMessage', 'Recibo añadido correctamente.');
    }

    public function recibosv_show($id)
    {
        $recibo = Recibo::findOrFail($id);
        //$cuotaDetalle = CuotaDetalle::firstWhere('forma_pago_id', $formaPago->forma_pago_id);
        $facturaVenta = DB::table('facturas_ventas as fv')
        ->select(['fv.factura_nro', 'fv.fecha_factura', 'fv.cliente_id', 'c.razon_social as razon_social', 'cu.cantidad_cuotas', 'cd.cuota_detalle_id', 'cd.monto_cuota', 'cd.vencimiento', 'm.sigla'])
        ->leftJoin('monedas as m', 'm.moneda_id', '=', 'fv.moneda_id')
        ->leftJoin('recibos as r', 'r.forma_pago_id', '=', 'fv.forma_pago_id')
        ->leftJoin('clientes as c', 'c.cliente_id', '=', 'fv.cliente_id')
        ->leftJoin('cuotas as cu', 'cu.forma_pago_id', '=', 'fv.forma_pago_id')
        ->leftJoin('cuotas_detalle as cd', 'cd.forma_pago_id', '=', 'cu.forma_pago_id')
        ->where('r.recibo_id', $id)
        ->first();;

        $moneda = Moneda::find($recibo->moneda_id);

        return view('ventas', ['var' => 'recibosv_show', 'facturaVenta' => $facturaVenta, 'recibo' => $recibo, 'moneda' => $moneda]);
    }

    public function recibosv_destroy($id)
    {
         try {
            $recibo = Recibo::findOrFail($id); // Captura el valor de la forma_pago_id antes de borrar la Factura.
            $recibo->delete();
            $this->auditoria($recibo, 'D');
            $forma_pago = FormaPago::findOrFail($recibo->recibo_forma_pago_id);
            $forma_pago->delete();
            $this->auditoria($forma_pago, 'D');
            return redirect(URL::previous())->with('successMessage', 'El Recibo fue borrado correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'El Recibo no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }

    public function recibosv_disable($id)
    {
        try {
            $recibo = Recibo::findOrFail($id); // Captura el valor de la forma_pago_id antes de borrar la Factura.
            $v_estado = $recibo->estado;

            if ($v_estado) {
                $recibo->estado = False;
                $recibo->save();
                return redirect(route('recibosv_index'))->with('successMessage', 'El Recibo fue anulado correctamente.');
            } else {
                $recibo->estado = True;
                $recibo->save();
                return redirect(route('recibosv_index'))->with('successMessage', 'El Recibo fue des-anulado correctamente.');
            }
            
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(route('recibosv_index'))->with('errorMessage', 'El Recibo no se puede modificar.');
        }
    }

    public function recibosv_pdf($id)
    {
        $recibo = DB::table('recibos as r')
        ->select(['r.recibo_id', 'r.numero as nro_recibo', 'r.fecha', 'cd.cuota_detalle_id as nro_cuota','cd.vencimiento', 'fp.nombre as forma_pago', 'c.razon_social as cliente', 'fv.factura_nro', 'm.sigla', 'cd.monto_cuota', 'fv.factura_nro', 'fv.fecha_factura', 'm.decimales', 'm.nombre as moneda', 'ch.numero_cheque as nro_cheque', 'ch.banco as banco_cheque', 'db.numero_transaccion as nro_deposito', 'db.banco_receptor as banco_deposito'])
        ->leftJoin('cuotas_detalle as cd', function($join){
            $join->on('cd.forma_pago_id', '=', 'r.forma_pago_id');
            $join->on('cd.cuota_detalle_id','=','r.cuota_detalle_id'); 
        })
        ->leftJoin('formas_pagos as fp', 'r.recibo_forma_pago_id', '=', 'fp.forma_pago_id')
        ->leftJoin('facturas_ventas as fv', 'fv.forma_pago_id', '=', 'r.forma_pago_id')
        ->leftJoin('clientes as c', 'c.cliente_id', '=', 'fv.cliente_id')
        ->leftJoin('monedas as m', 'm.moneda_id', '=', 'r.moneda_id')
        ->leftJoin('cheques as ch', 'ch.forma_pago_id', 'fp.forma_pago_id')
        ->leftJoin('depositos_bancarios as db', 'db.forma_pago_id', 'fp.forma_pago_id')
        ->where('r.recibo_id', $id)
        ->get();
        
        //dd($recibo);
        $monto_letras = [];
        
        $formatter = new NumeroALetras();
        $formatter->apocope = true;
        $decimales = $recibo[0]->decimales;
        $moneda = $recibo[0]->moneda;

        array_push($monto_letras, $formatter->toMoney($recibo[0]->monto_cuota, $decimales, $moneda, 'centavos'));

        $pdf = PDF::loadView('ventas.recibosv_pdf', ['recibo' => $recibo, 'monto_letras' => $monto_letras]);

        return $pdf->download('recibo_' . $recibo[0]->nro_recibo . '.pdf');
        //return view('ventas.recibosv_pdf', ['recibo' => $recibo, 'monto_letras' => $monto_letras]);
    }

    public function pagares_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $cd_max_cuotas = DB::table('cuotas as c1')
        ->select('c1.forma_pago_id', 'c1.cantidad_cuotas as cantidad_cuotas');

        $cd_max_vencimiento = DB::table('cuotas_detalle as cd2')
        ->select('cd2.forma_pago_id', DB::raw('MAX(cd2.vencimiento) as vencimiento'), DB::raw('BOOL_AND(cd2.esta_pagado) as esta_pagado'))
        ->groupBy('cd2.forma_pago_id');

        $cuotas_detalle = DB::table('cuotas_detalle as cd')
        ->select('cd.forma_pago_id', 'cuotas.cantidad_cuotas', 'cuota_vencimiento.vencimiento', 'cuota_vencimiento.esta_pagado')
        ->joinSub($cd_max_cuotas, 'cuotas', function ($join) {
            $join->on('cuotas.forma_pago_id', '=', 'cd.forma_pago_id');
        })
        ->joinSub($cd_max_vencimiento, 'cuota_vencimiento', function ($join) {
            $join->on('cuota_vencimiento.forma_pago_id', '=', 'cd.forma_pago_id');
        })
        ->distinct();

        $pagares = DB::table('facturas_ventas as fv')
        ->select(['c.razon_social', 'cuo.cantidad_cuotas', 'fv.factura_nro', 'fv.fecha_factura', 'fv.precio_venta', 'm.sigla', 'fv.forma_pago_id', 'cd.vencimiento', 'cd.esta_pagado'])
        ->leftJoin('cuotas as cuo', 'fv.forma_pago_id', 'cuo.forma_pago_id')
        ->leftJoin('clientes as c', 'fv.cliente_id', 'c.cliente_id')
        ->leftJoin('monedas as m', 'fv.moneda_id', 'm.moneda_id')
        ->joinSub($cuotas_detalle, 'cd', function ($join) {
            $join->on('fv.forma_pago_id', '=', 'cd.forma_pago_id');
        });

        if (!(blank($buscar))) {
            $pagares = $pagares->orWhere('c.razon_social', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('cuo.cantidad_cuotas', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('fv.factura_nro', 'ILIKE', '%'. $buscar . '%');
        }

        if ($this->validarFecha($buscar, 'd/m/Y')) {
            $buscar_fecha = str_replace('/', '-', $buscar); // Truco para que haga bien la conversion a Y-m-d
            $buscar_fecha = date('Y-m-d', strtotime($buscar_fecha));
            $pagares = $pagares->orWhere('fv.fecha_factura', 'ILIKE', '%'. $buscar_fecha . '%')
                                ->orWhere('cd.vencimiento', 'ILIKE', '%'. $buscar_fecha . '%');
        }

        $pagares = $pagares->where('fv.es_contado', False)
        ->orderBy('fv.fecha_factura')
        ->paginate($items);

        return view('ventas', ['var' => 'pagares_index', 'pagares' => $pagares, 'items' => $items, 'buscar' => $buscar]);
    }

    public function pagares_pdf($id)
    {
        $pagares = DB::table('cuotas_detalle as cd')
        ->select('cd.cuota_detalle_id as cuota_nro', 'cd.monto_cuota', 'm.sigla', 'cd.vencimiento', 'cuo.cantidad_cuotas', 'mar.nombre as marca', 'mod.nombre as modelo', 't.nombre as tipo', 'v.anho_fabricacion as anho', 'c.nombre as color', 'v.chasis', 'v.matricula', 'cli.razon_social as cliente', 'cli.ruc_ci as ci_cliente', 'cod.razon_social as codeudor', 'cod.ruc_ci as ci_codeudor', 'fv.fecha_factura', 'm.nombre as moneda', 'm.decimales')
        ->leftJoin('cuotas as cuo', 'cuo.forma_pago_id', 'cd.forma_pago_id')
        ->join('facturas_ventas as fv', 'fv.forma_pago_id', 'cd.forma_pago_id')
        ->leftJoin('monedas as m', 'm.moneda_id', 'fv.moneda_id')
        ->leftJoin('vehiculos as v', 'v.vehiculo_id', 'fv.vehiculo_id')
        ->leftJoin('modelos as mod', 'mod.modelo_id', 'v.modelo_id')
        ->leftJoin('marcas as mar', 'mar.marca_id', 'mod.marca_id')
        ->leftJoin('tipos as t', 't.tipo_id', 'v.tipo_id')
        ->leftJoin('colores as c', 'c.color_id', 'v.color_id')
        ->leftJoin('clientes as cli', 'cli.cliente_id', 'fv.cliente_id')
        ->leftJoin('clientes as cod', 'cod.cliente_id', 'fv.codeudor_id')
        ->where('fv.forma_pago_id', $id)
        ->orderBy('cd.cuota_detalle_id')
        ->get();
        
        $monto_letras = [];
        
        foreach ($pagares as $pagare) {
            $formatter = new NumeroALetras();
            $formatter->apocope = true;
            $decimales = $pagare->decimales;
            $moneda = $pagare->moneda;

            array_push($monto_letras, $formatter->toMoney($pagare->monto_cuota, $decimales, $moneda, 'centavos'));
        }
        

        $pdf = PDF::loadView('ventas.pagares_pdf', ['pagares' => $pagares, 'monto_letras' => $monto_letras]);

        return $pdf->download('pagares.pdf');
    }

    public function cuotasv_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $cuotas = DB::table('cuotas as c')
        ->select(['c.forma_pago_id', 'cl.razon_social', 'facturas_ventas.factura_nro', 'facturas_ventas.fecha_factura', 'cuotas_detalle.monto_cuota', 'c.cantidad_cuotas', 'cuotas_detalle.vencimiento', 'cuotas_detalle.esta_pagado', 'cuotas_detalle.cuota_detalle_id', 'monedas.sigla', 'monedas.nombre'])
        ->leftJoin('formas_pagos', 'c.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('facturas_ventas', 'facturas_ventas.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('monedas', 'monedas.moneda_id', '=', 'facturas_ventas.moneda_id')
        ->leftJoin('clientes as cl', 'facturas_ventas.cliente_id', '=', 'cl.cliente_id')
        ->leftJoin('cuotas_detalle', 'cuotas_detalle.forma_pago_id', '=', 'c.forma_pago_id');

        if (!(blank($buscar))) {
            $cuotas = $cuotas->orWhere('cl.razon_social', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('facturas_ventas.factura_nro', 'ILIKE', '%'. $buscar . '%');
        }

        if ($this->validarFecha($buscar, 'd/m/Y')) {
            $buscar_fecha = str_replace('/', '-', $buscar); // Truco para que haga bien la conversion a Y-m-d
            $buscar_fecha = date('Y-m-d', strtotime($buscar_fecha));
            $cuotas = $cuotas->orWhere('facturas_ventas.fecha_factura', 'ILIKE', '%'. $buscar_fecha . '%')
                            ->orWhere('cuotas_detalle.vencimiento', 'ILIKE', '%'. $buscar_fecha . '%');
        }

        $cuotas = $cuotas->whereNotNull('cl.razon_social')->orderBy("c.forma_pago_id")->paginate($items);

        return view('ventas', ['var' => 'cuotasv_index', 'cuotas' => $cuotas, 'items' => $items, 'buscar' => $buscar]);
    }

    public function cuotasv_show($f_pago_id, $c_detalle_id)
    {
        $cuota = DB::table('cuotas as c')
        ->select(['c.forma_pago_id', 'clientes.razon_social', 'facturas_ventas.factura_nro', 'facturas_ventas.fecha_factura', 'cuotas_detalle.monto_cuota', 'c.cantidad_cuotas', 'cuotas_detalle.vencimiento', 'cuotas_detalle.esta_pagado', 'cuotas_detalle.cuota_detalle_id', 'monedas.sigla', 'monedas.nombre'])
        ->leftJoin('formas_pagos', 'c.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('facturas_ventas', 'facturas_ventas.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('monedas', 'monedas.moneda_id', '=', 'facturas_ventas.moneda_id')
        ->leftJoin('clientes', 'facturas_ventas.cliente_id', '=', 'clientes.cliente_id')
        ->leftJoin('cuotas_detalle', 'cuotas_detalle.forma_pago_id', '=', 'c.forma_pago_id')
        ->where('c.forma_pago_id', $f_pago_id)
        ->where('cuotas_detalle.cuota_detalle_id', $c_detalle_id)
        ->first();
        
        return view('ventas', ['var' => 'cuotasv_show', 'cuota' => $cuota]);
    }

    public function cuotasv_edit($f_pago_id, $c_detalle_id)
    {
        $cuota = DB::table('cuotas as c')
        ->select(['c.forma_pago_id', 'clientes.razon_social', 'facturas_ventas.factura_nro', 'facturas_ventas.fecha_factura', 'cuotas_detalle.monto_cuota', 'c.cantidad_cuotas', 'cuotas_detalle.vencimiento', 'cuotas_detalle.esta_pagado', 'cuotas_detalle.cuota_detalle_id', 'monedas.sigla', 'monedas.nombre'])
        ->leftJoin('formas_pagos', 'c.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('facturas_ventas', 'facturas_ventas.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('monedas', 'monedas.moneda_id', '=', 'facturas_ventas.moneda_id')
        ->leftJoin('clientes', 'facturas_ventas.cliente_id', '=', 'clientes.cliente_id')
        ->leftJoin('cuotas_detalle', 'cuotas_detalle.forma_pago_id', '=', 'c.forma_pago_id')
        ->where('c.forma_pago_id', $f_pago_id)
        ->where('cuotas_detalle.cuota_detalle_id', $c_detalle_id)
        ->first();

        return view('ventas', ['var' => 'cuotasv_edit', 'cuota' => $cuota]);
    }

    public function cuotasv_update(Request $request, $f_pago_id, $c_detalle_id)
    {
        $this->validate($request, [
            'fecha_vencimiento' => 'required'
        ]);

        $v_fecha = str_replace('/', '-', $request->fecha_vencimiento); // Truco para que haga bien la conversion a Y-m-d
        $v_fecha = date('Y-m-d', strtotime($v_fecha));

        $viejo = CuotaDetalle::select(['cuotas_detalle.cuota_detalle_id', 'cuotas_detalle.vencimiento'])
        ->where('cuotas_detalle.forma_pago_id', $f_pago_id)
        ->where('cuotas_detalle.cuota_detalle_id', $c_detalle_id)->get();

        $cuota_update = CuotaDetalle::select(['cuotas_detalle.cuota_detalle_id', 'cuotas_detalle.vencimiento'])
        ->where('cuotas_detalle.forma_pago_id', $f_pago_id)
        ->where('cuotas_detalle.cuota_detalle_id', $c_detalle_id)
        ->update(['vencimiento' => $v_fecha]);
        
        $cuota = CuotaDetalle::select(['cuotas_detalle.cuota_detalle_id', 'cuotas_detalle.vencimiento'])
        ->where('cuotas_detalle.forma_pago_id', $f_pago_id)
        ->where('cuotas_detalle.cuota_detalle_id', $c_detalle_id)->get();

        $this->auditoria($cuota[0], 'U', $viejo[0]);
        return redirect(route('cuotasv_index'))->with('successMessage', 'Cuota actualizada correctamente.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function clientes_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $clientes = Cliente::select('*');

        if (!(blank($buscar))) {
            $clientes = $clientes->orWhere('clientes.razon_social', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('clientes.ruc_ci', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('clientes.telefono', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('clientes.correo', 'ILIKE', '%'. $buscar . '%');
        }

        $clientes = $clientes->orderBy('cliente_id')->paginate($items);
        return view('ventas', ['var' => 'clientes_index', 'clientes' => $clientes, 'items' => $items, 'buscar' => $buscar]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function clientes_create()
    {
        $ciudades = Ciudad::all()->sortBy('nombre');
        return view('ventas', ['var' => 'clientes_create', 'ciudades' => $ciudades]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function clientes_store(Request $request)
    {
        $this->validate($request, [
            'razon_social' => 'required',
            'ruc_ci' => 'required',
            'ciudad_id' => 'required',
            'estado_civil' => 'required'
        ]);

        $cliente = new Cliente;
        $cliente->razon_social = $request->razon_social;
        $cliente->ruc_ci = $request->ruc_ci;
        $cliente->direccion = $request->direccion;
        $cliente->ciudad_id = $request->ciudad_id;
        $cliente->telefono = $request->telefono;
        $cliente->correo = $request->correo;
        $cliente->estado_civil = $request->estado_civil;
        $cliente->observacion = $request->observacion;
        $cliente->estado = True;
        $cliente->save();

        $this->auditoria($cliente, 'I');
        return redirect(route('clientes_index'))->with('successMessage', 'Cliente añadido correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clientes_show($id)
    {
        $cliente = Cliente::findOrFail($id);
        $ciudad = Ciudad::findOrFail($cliente->ciudad_id);
        return view('ventas', ['var' => 'clientes_show', 'cliente' => $cliente, 'ciudad' => $ciudad]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clientes_edit($id)
    {
        $cliente = Cliente::findOrFail($id);
        $ciudades = Ciudad::all()->sortBy('nombre');
        return view('ventas', ['var' => 'clientes_edit', 'cliente' => $cliente, 'ciudades' => $ciudades]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clientes_update(Request $request, $id)
    {
        $this->validate($request, [
            'razon_social' => 'required',
            'ruc_ci' => 'required',
            'ciudad_id' => 'required',
            'estado_civil' => 'required'
        ]);

        $cliente = Cliente::findOrFail($id);
        $viejo = Cliente::findOrFail($id);
        $cliente->razon_social = $request->razon_social;
        $cliente->ruc_ci = $request->ruc_ci;
        $cliente->direccion = $request->direccion;
        $cliente->ciudad_id = $request->ciudad_id;
        $cliente->telefono = $request->telefono;
        $cliente->correo = $request->correo;
        $cliente->estado_civil = $request->estado_civil;
        $cliente->observacion = $request->observacion;
        $cliente->estado = $request->boolean('estado');
        $cliente->save();

        $this->auditoria($cliente, 'U', $viejo);
        return redirect(route('clientes_index'))->with('successMessage', 'Cliente actualizado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clientes_destroy($id)
    {
        try { 
            $cliente = Cliente::findOrFail($id);
            $cliente->delete();
            $this->auditoria($cliente, 'D');
            return redirect(URL::previous())->with('successMessage', 'Cliente borrado correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'El Cliente no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }



    public function facturasv_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $facturasVenta = DB::table('facturas_ventas as fv')->select(['fv.factura_venta_id','fv.factura_nro','clientes.razon_social','fv.fecha_factura', 'formas_pagos.nombre','fv.precio_venta', 'monedas.sigla','fv.estado', 'marcas.nombre as marca', 'modelos.nombre as modelo', 'vehiculos.anho_fabricacion as anho'])
        ->leftJoin('clientes', 'fv.cliente_id', '=', 'clientes.cliente_id')
        ->leftJoin('monedas', 'fv.moneda_id', '=', 'monedas.moneda_id')
        ->leftJoin('formas_pagos', 'fv.forma_pago_id', '=', 'formas_pagos.forma_pago_id')
        ->leftJoin('vehiculos', 'fv.vehiculo_id', 'vehiculos.vehiculo_id')
        ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
        ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id');

        if (!(blank($buscar))) {
            $facturasVenta = $facturasVenta->orWhere('fv.factura_nro', 'ILIKE', '%'. $buscar . '%')
                                            ->orWhere('clientes.razon_social', 'ILIKE', '%'. $buscar . '%')
                                            ->orWhere('formas_pagos.nombre', 'ILIKE', '%'. $buscar . '%')
                                            ->orWhere('modelos.nombre', 'ILIKE', '%'. $buscar . '%')
                                            ->orWhere('marcas.nombre', 'ILIKE', '%'. $buscar . '%')
                                            ->orWhere('vehiculos.anho_fabricacion', 'ILIKE', '%'. $buscar . '%');
        }
        
        if ($this->validarFecha($buscar, 'd/m/Y')) {
            $buscar_fecha = str_replace('/', '-', $buscar); // Truco para que haga bien la conversion a Y-m-d
            $buscar_fecha = date('Y-m-d', strtotime($buscar_fecha));
            $facturasVenta = $facturasVenta->orWhere('fv.fecha_factura', 'ILIKE', '%'. $buscar_fecha . '%');
        }

        $facturasVenta = $facturasVenta->orderBy("fv.factura_venta_id")->paginate($items);

        return view('ventas', ['var' => 'facturasv_index', 'facturasVenta' => $facturasVenta, 'items' => $items, 'buscar' => $buscar]);
    }

    public function facturasv_create()
    {
        $vehiculos = Vehiculo::select(['vehiculos.vehiculo_id as vehiculo_id', 'modelos.nombre as modelo', 'marcas.nombre as marca', 'colores.nombre as color', 'vehiculos.anho_fabricacion as anho', 'vehiculos.costo as costo', 'monedas.moneda_id as moneda_id', 'monedas.sigla as sigla', 'monedas.cotizacion as cotizacion', 'vehiculos.estado as estado'])
                     ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
                     ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
                     ->leftJoin('monedas', 'monedas.moneda_id', '=', 'vehiculos.moneda_id')
                     ->leftJoin('colores', 'colores.color_id', '=', 'vehiculos.color_id')
                     ->where('vehiculos.estado', '=', True)
                     ->orderBy('marca')->get();
        $clientes = Cliente::all()->where('estado', '=', True)->sortBy('razon_social');
        $monedas = Moneda::all()->sortBy('nombre');
        $facturasVentas = FacturaVenta::all();
        return view('ventas', ['var' => 'facturasv_create', 'facturasVentas' => $facturasVentas, 'clientes' => $clientes, 'monedas' => $monedas, 'vehiculos' => $vehiculos]);
    }

     public function facturasv_store(Request $request)
    {
        $this->validate($request, [

            'factura_nro' => 'required',
            'cliente_id' => 'required',
            'es_contado' => 'required',
            'fecha_factura' => 'required',
            'vehiculo_id' => 'required',
            'precio_venta' => 'required',
            'moneda_id' => 'required'
        ]);

        $monto_limpio = Str::of($request->monto)->replace(',', '-')->replace('.', '')->replace('-', '.');

        $facturaVenta = new FacturaVenta;
        $facturaVenta->factura_nro = $request->factura_nro;
        $facturaVenta->cliente_id = $request->cliente_id;
        $facturaVenta->codeudor_id = $request->codeudor_id;
        $facturaVenta->fecha_factura = $request->fecha_factura;
        $facturaVenta->fecha_vencimiento = $request->fecha_vencimiento;
        $facturaVenta->es_contado = $request->boolean('es_contado');
        $facturaVenta->vehiculo_id = $request->vehiculo_id;
        $facturaVenta->precio_venta = $monto_limpio;
        $facturaVenta->moneda_id = $request->moneda_id;
        $facturaVenta->estado = True;

        $forma_pago_seleccionada = $request->forma_pago_nombre;
        
        $formaPago = new FormaPago;
        $formaPago->monto = $monto_limpio;

        switch($forma_pago_seleccionada) {
            case 'efectivo':
                $formaPago->nombre = 'Efectivo';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                break;

            case 'cheques':
                $formaPago->nombre = 'Cheque';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                $cheque = new Cheque;
                $cheque->forma_pago_id = $formaPago->forma_pago_id;
                $cheque->numero_cheque = $request->numero_cheque;
                $cheque->fecha_cheque = $request->fecha_cheque;
                $cheque->banco = $request->banco;
                $cheque->save();
                $this->auditoria($cheque, 'I');
                break;

            case 'depositos_bancarios':
                $formaPago->nombre = 'DepositoBancario';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                $depositoBancario = new DepositoBancario;
                $depositoBancario->forma_pago_id = $formaPago->forma_pago_id;
                $depositoBancario->numero_transaccion = $request->numero_transaccion;
                $depositoBancario->fecha_deposito = $request->fecha_deposito;
                $depositoBancario->banco_receptor = $request->banco_receptor;
                $depositoBancario->save();
                $this->auditoria($depositoBancario, 'I');
                break;

            case 'cuotas':
                $formaPago->nombre = 'Cuotas';
                $formaPago->save();
                $this->auditoria($formaPago, 'I');
                $cuota = new Cuota;
                $cuota->forma_pago_id = $formaPago->forma_pago_id;
                $cuota->cantidad_cuotas = $request->cantidad_cuotas;
                $cuota->save();
                $this->auditoria($cuota, 'I');
                
                $v_total = (string)$monto_limpio;
                $v_cantidad_cuotas = $request->cantidad_cuotas; // Cantidad de Cuotas
                $v_cuota = intdiv($v_total, $v_cantidad_cuotas); // Division entera
                $v_diferencia = $v_total % $v_cantidad_cuotas;

                $v_fecha = str_replace('/', '-', $facturaVenta->fecha_factura); // Truco para que haga bien la conversion a Y-m-d
                $v_fecha = date('Y-m-d', strtotime($v_fecha));
                //echo $v_fecha;

                for ($i = 1; $i < $v_cantidad_cuotas; $i++) {
                    $cuotaDetalle = new CuotaDetalle;
                    $cuotaDetalle->cuota_detalle_id = $i;
                    $cuotaDetalle->forma_pago_id = $formaPago->forma_pago_id;
                    $cuotaDetalle->monto_cuota = $v_cuota;
                    $cuotaDetalle->vencimiento = date('Y-m-d', strtotime($v_fecha. ' + '. $i . 'months'));
                    $cuotaDetalle->esta_pagado = False;
                    $cuotaDetalle->save();
                    $this->auditoria($cuotaDetalle, 'I');
                }

                $cuotaDetalle = new CuotaDetalle;
                $cuotaDetalle->cuota_detalle_id = $v_cantidad_cuotas;
                $cuotaDetalle->forma_pago_id = $formaPago->forma_pago_id;
                $cuotaDetalle->monto_cuota = $v_cuota + $v_diferencia;
                $cuotaDetalle->vencimiento = date('Y-m-d', strtotime($v_fecha. ' + '. $v_cantidad_cuotas . 'months'));
                $cuotaDetalle->esta_pagado = False;
                $cuotaDetalle->save();
                $this->auditoria($cuotaDetalle, 'I');

                break;
            
            default:
                //default;
        };

        $facturaVenta->forma_pago_id = $formaPago->forma_pago_id;
        $facturaVenta->save();
        $this->auditoria($facturaVenta, 'I');

        return redirect(route('facturasv_index'))->with('successMessage', 'Factura de Venta añadida correctamente.');
    }

    public function facturasv_show($id)
    {
        $facturaVenta = FacturaVenta::findOrFail($id);
        $cliente = Cliente::find($facturaVenta->cliente_id);
        $codeudor = Cliente::find($facturaVenta->codeudor_id);
        $moneda = Moneda::find($facturaVenta->moneda_id);
        $vehiculo = Vehiculo::find($facturaVenta->vehiculo_id);
        $modelo = Modelo::find($vehiculo->modelo_id);
        $marca = Marca::find($modelo->marca_id);
        $color = Color::find($vehiculo->color_id);
        $moneda = Moneda::find($facturaVenta->moneda_id);
        $formaPago = FormaPago::find($facturaVenta->forma_pago_id);
        $cheque = Cheque::find($formaPago->forma_pago_id);        
        $depositoBancario = DepositoBancario::find($formaPago->forma_pago_id);
        $cuota = Cuota::find($formaPago->forma_pago_id);
        $cuotaDetalle = CuotaDetalle::firstWhere('forma_pago_id', $formaPago->forma_pago_id);

        return view('ventas', ['var' => 'facturasv_show', 'facturaVenta' => $facturaVenta, 'cliente' => $cliente, 'codeudor' => $codeudor, 'moneda' => $moneda, 'vehiculo' => $vehiculo, 'marca' => $marca, 'modelo' => $modelo, 'color' => $color, 'moneda' => $moneda, 'formaPago' => $formaPago, 'cheque' => $cheque, 'depositoBancario' => $depositoBancario, 'cuota' => $cuota, 'cuotaDetalle' => $cuotaDetalle]);
    }

    public function facturasv_destroy($id)
    {
        try {
            $factura_venta = FacturaVenta::findOrFail($id); // Captura el valor de la forma_pago_id antes de borrar la Factura.
            $factura_venta->delete();
            $this->auditoria($factura_venta, 'D');
            $forma_pago = FormaPago::findOrFail($factura_venta->forma_pago_id);
            $forma_pago->delete();
            $this->auditoria($forma_pago, 'D');
            return redirect(URL::previous())->with('successMessage', 'La Factura de Venta fue borrada correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'La Factura de Venta no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }

    public function facturasv_disable($id)
    {
        try {
            $factura_venta = FacturaVenta::findOrFail($id); // Captura el valor de la forma_pago_id antes de borrar la Factura.
            $v_estado = $factura_venta->estado;

            if ($v_estado) {
                $factura_venta->estado = False;
                $factura_venta->save();
                return redirect(route('facturasv_index'))->with('successMessage', 'La Factura de Venta fue anulada correctamente.');
            } else {
                $factura_venta->estado = True;
                $factura_venta->save();
                return redirect(route('facturasv_index'))->with('successMessage', 'La Factura de Venta fue des-anulada correctamente.');
            }
            
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(route('facturasv_index'))->with('errorMessage', 'La Factura de Venta no se puede modificar.');
        }
    }

    public function auditoria($objeto, $tipo_transaccion, $objeto_viejo = null)
    {
        if ($tipo_transaccion == 'I') {
            $atributos = $objeto->attributesToArray();
            
            foreach ($atributos as $clave => $atributo) {
                //dd($atributo);
                if ($atributo !== $objeto->getKey()) {
                    $auditoria = new Auditoria;
                    $auditoria->tipo_transaccion = 'I';
                    $auditoria->tabla = $objeto->getTable();
                    $auditoria->tabla_pk = $objeto->getKey();
                    $auditoria->campo = $clave;
                    $auditoria->valor_nuevo = $atributo;
                    $auditoria->fecha = now();
                    $auditoria->usuario_id = Auth::user()->id;
                    $auditoria->save();
                }
            }
        }
        
        if ($tipo_transaccion == 'D') {
            $atributos = $objeto->attributesToArray();
            
            foreach ($atributos as $clave => $atributo) {
                if ($atributo !== $objeto->getKey()) {
                    $auditoria = new Auditoria;
                    $auditoria->tipo_transaccion = 'D';
                    $auditoria->tabla = $objeto->getTable();
                    $auditoria->tabla_pk = $objeto->getKey();
                    $auditoria->campo = $clave;
                    $auditoria->valor_viejo = $atributo;
                    $auditoria->fecha = now();
                    $auditoria->usuario_id = Auth::user()->id;
                    $auditoria->save();
                }
            }
        }

        if ($tipo_transaccion == 'U') {
            $atributos = $objeto->attributesToArray();
            
            foreach ($atributos as $clave => $atributo) {
                if ($atributo !== $objeto->getKey()) {
                    if ($objeto_viejo[$clave] != $atributo) {
                        $auditoria = new Auditoria;
                        $auditoria->tipo_transaccion = 'U';
                        $auditoria->tabla = $objeto->getTable();
                        $auditoria->tabla_pk = $objeto->getKey();
                        $auditoria->campo = $clave;
                        $auditoria->valor_viejo = $objeto_viejo[$clave];
                        $auditoria->valor_nuevo = $atributo;
                        $auditoria->fecha = now();
                        $auditoria->usuario_id = Auth::user()->id;
                        $auditoria->save();
                    }
                }
            }
        }

    }
    
    function validarFecha($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }
}