<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Auditoria;
use App\Models\Vehiculo;
use App\Models\Marca;
use App\Models\Moneda;
use App\Models\Color;
use App\Models\Tipo;
use App\Models\Deposito;
use App\Models\FacturaVenta;
use App\Models\FormaPago;
use App\Models\Cliente;
use App\Models\FacturaCompra;
use App\Models\Proveedor;


use App\Models\User;

use Knp\Snappy\Pdf; 
use PdfReport;
use ExcelReport;



class ReportesController extends Controller
{

    public function vehiculos_index(Request $request)
    {
        $items = $request->items ?? 10;
        $marca_id = $request->marca_id;
        $tipo_id = $request->tipo_id;
        $color_id = $request->color_id;

        $anho_id = $request->anho_id;
        $deposito_id = $request->deposito_id;
        $estado_id = $request->estado_id;
        $disponibilidad_id = $request->disponibilidad_id;

        $sort_by = $request->sort_by;

        $marcas = Marca::all()->sortBy('nombre');
        $tipos = Tipo::all()->sortBy('nombre');
        $colores = Color::all()->sortBy('nombre');
        $depositos = Deposito::all()->sortBy('nombre');
        $anhos = DB::table('vehiculos')->select('anho_fabricacion as anho_nro')->distinct()->orderBy('anho_nro')->get();
        
        $disponibilidad = DB::table('vehiculos')
        ->select('vehiculos.vehiculo_id', DB::raw("CASE WHEN vehiculos.estado THEN 'Disponible' ELSE CASE WHEN vehiculos.estado = false AND vehiculos.costo IS NULL THEN 'Inactivo' ELSE 'Vendido' END END AS nombre"));

        $vehiculos = Vehiculo::select('vehiculos.vehiculo_id as vehiculo_id', 'modelos.nombre as modelo', 'marcas.nombre as marca', 'colores.nombre as color', 'vehiculos.anho_fabricacion as anho', 'vehiculos.costo as costo', 'vehiculos.transmision as transmision', 'tipos.nombre as tipo', 'monedas.sigla as sigla', 'depositos.nombre as deposito', 'vehiculos.estado as estado', 'disponibilidad.nombre as disponibilidad') 
                
        ->joinSub($disponibilidad, 'disponibilidad', function ($join) use ($disponibilidad_id) {
            $join->on('vehiculos.vehiculo_id', '=', 'disponibilidad.vehiculo_id');
            
            if (isset($disponibilidad_id)) {
                $join->where('nombre', $disponibilidad_id); }
        })

        ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
        ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
        ->leftJoin('monedas', 'monedas.moneda_id', '=', 'vehiculos.moneda_id')
        ->leftJoin('colores', 'colores.color_id', '=', 'vehiculos.color_id')
        ->leftJoin('depositos', 'depositos.deposito_id', '=', 'vehiculos.deposito_id')
        ->leftJoin('tipos', 'tipos.tipo_id', '=', 'vehiculos.tipo_id');

        // FILTRADO
        if (isset($marca_id)) {
            $vehiculos = $vehiculos->where('marcas.marca_id', $marca_id); }
        if (isset($tipo_id)) {
            $vehiculos = $vehiculos->where('tipos.tipo_id', $tipo_id); }
        if (isset($color_id)) {
            $vehiculos = $vehiculos->where('colores.color_id', $color_id); }
        if (isset($anho_id)) {
            $vehiculos = $vehiculos->where('vehiculos.anho_fabricacion', $anho_id); }
        if (isset($deposito_id)) {
            $vehiculos = $vehiculos->where('depositos.deposito_id', $deposito_id); }

        // ORDENAMIENTO
        if (isset($sort_by)) {
            switch ($sort_by) { 
                case 'marca':
                    $vehiculos = $vehiculos->orderBy('marca');
                    break;
                
                case 'modelo':
                    $vehiculos = $vehiculos->orderBy('modelo');
                    break;

                case 'tipo':
                    $vehiculos = $vehiculos->orderBy('tipo');
                    break;

                case 'color':
                    $vehiculos = $vehiculos->orderBy('color');
                    break;

                case 'transmision':
                    $vehiculos = $vehiculos->orderBy('transmision');
                    break;
                case 'anho':
                    $vehiculos = $vehiculos->orderBy('anho');
                    break;
                case 'deposito':
                    $vehiculos = $vehiculos->orderBy('deposito');
                    break;
            }
        }

        // EJECUTAR CONSULTA
        $vehiculos = $vehiculos->paginate($items);

        return view('reportes', 
            ['var' => 'r_vehiculos_index', 'vehiculos' => $vehiculos, 'marcas' => $marcas, 'colores' => $colores, 'tipos' => $tipos, 'anhos' => $anhos, 'depositos' => $depositos, 'items' => $items, 'marca_sel'=> $marca_id, 'tipo_sel'=> $tipo_id, 'color_sel'=> $color_id, 'anho_sel' => $anho_id,  'deposito_sel'=> $deposito_id, 'sort_sel'=> $sort_by, 'disponibilidad_sel'=> $disponibilidad_id]);
    }

    public function r_vehiculos (Request $request)
    {

        $items = $request->items ?? 10;
        $marca_id = $request->marca_id;
        $tipo_id = $request->tipo_id;
        $color_id = $request->color_id;
        $anho_id = $request->anho_id;
        $deposito_id = $request->deposito_id;
        $estado_id = $request->estado_id;
        $disponibilidad_id = $request->disponibilidad_id;

        $sort_by = $request->sort_by;

        $marcas = Marca::all()->sortBy('nombre');
        $tipos = Tipo::all()->sortBy('nombre');
        $colores = Color::all()->sortBy('nombre');
        $depositos = Deposito::all()->sortBy('nombre');
        $anhos = DB::table('vehiculos')->select('anho_fabricacion as anho_nro')->distinct()->orderBy('anho_nro')->get();

        $meta = array();
        
        $disponibilidad = DB::table('vehiculos')
        ->select('vehiculos.vehiculo_id', DB::raw("CASE WHEN vehiculos.estado THEN 'Disponible' ELSE CASE WHEN vehiculos.estado = false AND vehiculos.costo IS NULL THEN 'Inactivo' ELSE 'Vendido' END END AS nombre"));

        $vehiculos = Vehiculo::select('vehiculos.vehiculo_id as vehiculo_id', 'modelos.nombre as modelo', 'marcas.nombre as marca', 'colores.nombre as color', 'vehiculos.anho_fabricacion as anho', 'vehiculos.costo as costo', 'vehiculos.transmision as transmision', 'tipos.nombre as tipo', 'monedas.sigla as sigla', 'depositos.nombre as deposito', 'vehiculos.estado as estado', 'disponibilidad.nombre as disponibilidad') 
                
        ->joinSub($disponibilidad, 'disponibilidad', function ($join) use ($disponibilidad_id) {
            $join->on('vehiculos.vehiculo_id', '=', 'disponibilidad.vehiculo_id');
            
            if (isset($disponibilidad_id)) {
                $join->where('nombre', $disponibilidad_id); }
        })

        ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
        ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
        ->leftJoin('monedas', 'monedas.moneda_id', '=', 'vehiculos.moneda_id')
        ->leftJoin('colores', 'colores.color_id', '=', 'vehiculos.color_id')
        ->leftJoin('depositos', 'depositos.deposito_id', '=', 'vehiculos.deposito_id')
        ->leftJoin('tipos', 'tipos.tipo_id', '=', 'vehiculos.tipo_id');

        // FILTRADO
        if (isset($marca_id)) {
            $vehiculos = $vehiculos->where('marcas.marca_id', $marca_id); 
            $marca_nombre = $vehiculos->get();
            $meta += ['Marca' => $marca_nombre[0]->marca]; 
        }
        if (isset($tipo_id)) {
            $vehiculos = $vehiculos->where('tipos.tipo_id', $tipo_id); 
            $tipo_nombre = $vehiculos->get();
            $meta += ['Tipo' => $tipo_nombre[0]->tipo]; 
        }
        if (isset($color_id)) {
            $vehiculos = $vehiculos->where('colores.color_id', $color_id);
            $color_nombre = $vehiculos->get();
            $meta += ['Color' => $color_nombre[0]->color]; 
        }
        if (isset($anho_id)) {
            $vehiculos = $vehiculos->where('vehiculos.anho_fabricacion', $anho_id); 
            $anho_nombre = $vehiculos->get();
            $meta += ['Año' => $anho_nombre[0]->anho]; 
        }
        if (isset($deposito_id)) {
            $vehiculos = $vehiculos->where('depositos.deposito_id', $deposito_id); 
            $deposito_nombre = $vehiculos->get();
            $meta += ['Depósito' => $deposito_nombre[0]->deposito]; 
        }

        // ORDENAMIENTO
        if (isset($sort_by)) {
            switch ($sort_by) { 
                case 'marca':
                    $vehiculos = $vehiculos->orderBy('marca');
                    $meta += ['Ordenado por' => 'Marca'];
                    break;
                
                case 'modelo':
                    $vehiculos = $vehiculos->orderBy('modelo');
                    $meta += ['Ordenado por' => 'Modelo'];
                    break;

                case 'tipo':
                    $vehiculos = $vehiculos->orderBy('tipo');
                    $meta += ['Ordenado por' => 'Tipo'];
                    break;

                case 'color':
                    $vehiculos = $vehiculos->orderBy('color');
                    $meta += ['Ordenado por' => 'Color'];
                    break;

                case 'transmision':
                    $vehiculos = $vehiculos->orderBy('transmision');
                    $meta += ['Ordenado por' => 'Transmisión'];
                    break;
                case 'anho':
                    $vehiculos = $vehiculos->orderBy('anho');
                    $meta += ['Ordenado por' => 'Año'];
                    break;
                case 'deposito':
                    $vehiculos = $vehiculos->orderBy('deposito');
                    $meta += ['Ordenado por' => 'Depósito'];
                    break;
                default:
                    $vehiculos = $vehiculos->orderBy('marca');
                    $meta += ['Ordenado por' => 'Marca'];
                    break;
            }
        }

        $titulo = 'Reporte de Vehículos'; // Report titulo

        $columnas = [ // Set Column to be displayed
            'Marca' => 'marca',
            'Modelo' => 'modelo',
            'Tipo' => 'tipo',
            'Color' => 'color',
            'Transmision' => function($vehiculos) { // You can do data manipulation, if statement or any action do you want inside this closure
                                switch ($vehiculos->transmision) { 
                                    case 'A':
                                        return 'Automático';
                                        break;
                                    case 'M':
                                        return 'Manual';
                                        break;
                                    case 'S':
                                        return 'Secuencial';
                                        break;
                                    }
                            },
            'Año' => 'anho',
            'Depósito' => 'deposito',
            'Costo' => function($vehiculos) {
                            switch ($vehiculos->sigla) { 
                                case 'Gs':
                                    return number_format($vehiculos->costo, $decimals = 0 , $dec_point = "," , $thousands_sep = "." ) . ' ' . $vehiculos->sigla;
                                    break;
                                case '':
                                    return '';
                                default:
                                    return number_format($vehiculos->costo, $decimals = 2 , $dec_point = "," , $thousands_sep = "." ) . ' ' . $vehiculos->sigla;
                                    break;
                            }
                        },
            'Disponibilidad' => 'disponibilidad'
        ];

        return (['titulo' => $titulo, 'meta' => $meta, 'vehiculos' => $vehiculos, 'columnas' => $columnas]);
    }

    public function r_vehiculos_xls(Request $request)
    {

        $resultado = $this->r_vehiculos($request);

        return ExcelReport::of($resultado['titulo'], $resultado['meta'], $resultado['vehiculos'], $resultado['columnas'])
                    ->editColumns(['Disponibilidad'], [ // Mass edit column
                        'class' => 'bold'
                    ])
                    ->editColumns(['año'], [ // Mass edit column
                        'class' => 'left'
                    ])
                    ->download('ReporteVehiculos');
    }

    public function r_vehiculos_pdf(Request $request)
    {

        $resultado = $this->r_vehiculos($request);

        return PdfReport::of($resultado['titulo'], $resultado['meta'], $resultado['vehiculos'], $resultado['columnas'])
                    ->editColumns(['Disponibilidad'], [ // Mass edit column
                        'class' => 'bold'
                    ])
                    ->editColumns(['año'], [ // Mass edit column
                        'class' => 'left'
                    ])
                    ->stream();
    }

    public function ventas_index(Request $request)
    {

        $items = $request->items ?? 10;

        $cliente_id = $request->cliente_id;
        $vehiculo_id = $request->vehiculo_id;
        $forma_pago_id = $request->forma_pago_id;
        $estado_id = $request->estado_id;
        $v_fecha_desde = str_replace('/', '-', $request->fecha_desde); // Truco para que haga bien la conversion a Y-m-d
        $v_fecha_hasta = str_replace('/', '-', $request->fecha_hasta); // Truco para que haga bien la conversion a Y-m-d

        $fecha_desde = date('Y-m-d', strtotime($v_fecha_desde));
        $fecha_hasta = date('Y-m-d', strtotime($v_fecha_hasta));

        $sort_by = $request->sort_by;

        $clientes = Cliente::all()->sortBy('razon_social');

        $vehiculos = DB::table('vehiculos')
        ->select(DB::raw("CONCAT(marcas.nombre, ' ', modelos.nombre) as nombre"))->distinct()
        ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
        ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
        ->orderBy('nombre')->get();

        $vehiculos_f = DB::table('vehiculos')
        ->select('vehiculos.vehiculo_id', DB::raw("CONCAT(marcas.nombre, ' ', modelos.nombre) as nombre"))
        ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
        ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
        ->orderBy('nombre');
        
        $formas_pagos = DB::table('formas_pagos')->select('nombre')->distinct()->orderBy('nombre')->get();

        $ventas = FacturaVenta::select('facturas_ventas.factura_nro as nro', 'facturas_ventas.fecha_factura as fecha', 'clientes.razon_social as cliente', 'vehiculos.nombre as vehiculo',  'facturas_ventas.precio_venta as precio', 'monedas.sigla as sigla', 'formas_pagos.nombre as forma_pago', 'facturas_ventas.estado as estado')

        ->joinSub($vehiculos_f, 'vehiculos', function ($join) use ($vehiculo_id) {
            $join->on('facturas_ventas.vehiculo_id', '=', 'vehiculos.vehiculo_id');
                if (isset($vehiculo_id)) {
                    $join->where('nombre', 'ILIKE',  $vehiculo_id); }
        })

        ->leftJoin('clientes', 'clientes.cliente_id', '=','facturas_ventas.cliente_id')
        ->leftJoin('monedas', 'monedas.moneda_id', '=', 'facturas_ventas.moneda_id')
        ->leftJoin('formas_pagos', 'formas_pagos.forma_pago_id', '=', 'facturas_ventas.forma_pago_id');
        

        // FILTRADO
        if (isset($cliente_id)) {
            $ventas = $ventas->where('facturas_ventas.cliente_id', $cliente_id); }
        
        if (isset($request->fecha_desde)) {
            if (isset($request->fecha_hasta)) {
                $ventas = $ventas->whereBetween('facturas_ventas.fecha_factura', [$fecha_desde, $fecha_hasta]);        
            } else {
                $ventas = $ventas->where('facturas_ventas.fecha_factura', '>=', $fecha_desde);
            }
        } else {
            if (isset($request->fecha_hasta)) {
                $ventas = $ventas->where('facturas_ventas.fecha_factura', '<=', $fecha_hasta);   
            }
        }

        if (isset($forma_pago_id)) {
            $ventas = $ventas->where('formas_pagos.nombre', $forma_pago_id); }
        if (isset($estado_id)) {
            if ($estado_id == "Activo") {
                $ventas = $ventas->where('facturas_ventas.estado', True);
            } else {
                $ventas = $ventas->where('facturas_ventas.estado', False);
            }
        }

        // ORDENAMIENTO
        if (isset($sort_by)) {
            switch ($sort_by) { 
                case 'factura_nro':
                    $ventas = $ventas->orderBy('nro');
                    break;
                
                case 'cliente':
                    $ventas = $ventas->orderBy('cliente');
                    break;
                
                case 'vehiculo':
                    $ventas = $ventas->orderBy('vehiculo');
                    break;

                case 'forma_pago':
                    $ventas = $ventas->orderBy('forma_pago');
                    break;

                case 'estado':
                    $ventas = $ventas->orderBy('estado', 'desc');
                    break;
            }
        }

        // EJECUTAR CONSULTA
        $ventas = $ventas->paginate($items);

        return view('reportes', 
            ['var' => 'r_ventas_index', 'ventas' => $ventas, 'clientes' => $clientes, 'vehiculos' => $vehiculos, 'formas_pagos' => $formas_pagos, 'items' => $items, 'cliente_sel' => $cliente_id, 'vehiculo_sel' => $vehiculo_id, 'forma_pago_sel' => $forma_pago_id, 'fecha_desde_sel' => $request->fecha_desde, 'fecha_hasta_sel' => $request->fecha_hasta, 'estado_sel' => $estado_id, 'sort_sel'=> $sort_by]);
    }

    public function r_ventas(Request $request)
    {

        $items = $request->items ?? 10;

        $cliente_id = $request->cliente_id;
        $vehiculo_id = $request->vehiculo_id;
        $forma_pago_id = $request->forma_pago_id;
        $estado_id = $request->estado_id;
        $v_fecha_desde = str_replace('/', '-', $request->fecha_desde); // Truco para que haga bien la conversion a Y-m-d
        $v_fecha_hasta = str_replace('/', '-', $request->fecha_hasta); // Truco para que haga bien la conversion a Y-m-d

        $fecha_desde = date('Y-m-d', strtotime($v_fecha_desde));
        $fecha_hasta = date('Y-m-d', strtotime($v_fecha_hasta));

        $sort_by = $request->sort_by;
        $meta = array();

        $clientes = Cliente::all()->sortBy('razon_social');

        $vehiculos = DB::table('vehiculos')
        ->select(DB::raw("CONCAT(marcas.nombre, ' ', modelos.nombre) as nombre"))->distinct()
        ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
        ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
        ->orderBy('nombre')->get();

        $vehiculos_f = DB::table('vehiculos')
        ->select('vehiculos.vehiculo_id', DB::raw("CONCAT(marcas.nombre, ' ', modelos.nombre) as nombre"))
        ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
        ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
        ->orderBy('nombre');
        
        $formas_pagos = DB::table('formas_pagos')->select('nombre')->distinct()->orderBy('nombre')->get();

        $ventas = FacturaVenta::select('facturas_ventas.factura_nro as nro', 'facturas_ventas.fecha_factura as fecha', 'clientes.razon_social as cliente', 'vehiculos.nombre as vehiculo',  'facturas_ventas.precio_venta as precio', 'monedas.sigla as sigla', 'formas_pagos.nombre as forma_pago', 'facturas_ventas.estado as estado')

        ->joinSub($vehiculos_f, 'vehiculos', function ($join) use ($vehiculo_id) {
            $join->on('facturas_ventas.vehiculo_id', '=', 'vehiculos.vehiculo_id');
                if (isset($vehiculo_id)) {
                    $join->where('nombre', 'ILIKE',  $vehiculo_id); }
        })

        ->leftJoin('clientes', 'clientes.cliente_id', '=','facturas_ventas.cliente_id')
        ->leftJoin('monedas', 'monedas.moneda_id', '=', 'facturas_ventas.moneda_id')
        ->leftJoin('formas_pagos', 'formas_pagos.forma_pago_id', '=', 'facturas_ventas.forma_pago_id');
        

        // FILTRADO
        if (isset($cliente_id)) {
            $ventas = $ventas->where('facturas_ventas.cliente_id', $cliente_id); 
            $cliente_nombre = $ventas->get();
            $meta += ['Cliente' => $cliente_nombre[0]->cliente]; 
        }
        
        if (isset($request->fecha_desde)) {
            if (isset($request->fecha_hasta)) {
                $ventas = $ventas->whereBetween('facturas_ventas.fecha_factura', [$fecha_desde, $fecha_hasta]);
                $meta += ['Entre Fechas' => $request->fecha_desde . ' - ' . $request->fecha_hasta]; 
            } else {
                $ventas = $ventas->where('facturas_ventas.fecha_factura', '>=', $fecha_desde);
                $meta += ['Desde la Fecha' => $request->fecha_desde]; 
            }
        } else {
            if (isset($request->fecha_hasta)) {
                $ventas = $ventas->where('facturas_ventas.fecha_factura', '<=', $fecha_hasta);
                $meta += ['Hasta la Fecha' => $request->fecha_hasta]; 
            }
        }

        if (isset($forma_pago_id)) {
            $ventas = $ventas->where('formas_pagos.nombre', $forma_pago_id); 
            $forma_pago_nombre = $ventas->get();
            $meta += ['Forma de Pago' => $forma_pago_nombre[0]->forma_pago]; 
        }

        if (isset($vehiculo_id)) {
            $ventas = $ventas->where('vehiculos.nombre', $vehiculo_id); 
            $vehiculo_nombre = $ventas->get();
            $meta += ['Vehículo' => $vehiculo_nombre[0]->vehiculo]; 
        }

        if (isset($estado_id)) {
            if ($estado_id == "Activo") {
                $ventas = $ventas->where('facturas_ventas.estado', True);
                $meta += ['Estado' => 'Activo'];
            } else {
                $ventas = $ventas->where('facturas_ventas.estado', False);
                $meta += ['Estado' => 'Anulado'];
            }
        }

        // ORDENAMIENTO
        if (isset($sort_by)) {
            switch ($sort_by) { 
                case 'factura_nro':
                    $ventas = $ventas->orderBy('nro');
                    $meta += ['Ordenado por' => 'Número de Factura'];
                    break;
                
                case 'cliente':
                    $ventas = $ventas->orderBy('cliente');
                    $meta += ['Ordenado por' => 'Cliente'];
                    break;
                
                case 'vehiculo':
                    $ventas = $ventas->orderBy('vehiculo');
                    $meta += ['Ordenado por' => 'Vehículo'];
                    break;

                case 'forma_pago':
                    $ventas = $ventas->orderBy('forma_pago');
                    $meta += ['Ordenado por' => 'Forma de Pago'];
                    break;

                case 'estado':
                    $ventas = $ventas->orderBy('estado', 'desc');
                    $meta += ['Ordenado por' => 'Estado'];
                    break;
            }
        }

        $titulo = 'Reporte de Ventas'; // Report titulo

        $columnas = [ // Set Column to be displayed
            'Fac. Nro' => 'nro',
            'Fecha' => function($ventas) { 
                return date("d/m/Y", strtotime($ventas->fecha)); },
            'Cliente' => 'cliente',
            'Vehículo' => 'vehiculo',
            'Precio' => function($ventas) {
                            switch ($ventas->sigla) { 
                                case 'Gs':
                                    return number_format($ventas->precio, $decimals = 0 , $dec_point = "," , $thousands_sep = "." ) . ' ' . $ventas->sigla;
                                    break;
                                default:
                                    return number_format($ventas->precio, $decimals = 2 , $dec_point = "," , $thousands_sep = "." ) . ' ' . $ventas->sigla;
                                    break;
                            }
                        },
            'Forma Pago' => 'forma_pago',
            'Estado' => function($ventas) { 
                return ($ventas->estado == 1) ? 'Activo' : 'Anulado'; }
        ];

        return (['titulo' => $titulo, 'meta' => $meta, 'ventas' => $ventas, 'columnas' => $columnas]);
    }

    public function r_ventas_xls(Request $request)
    {

        $resultado = $this->r_ventas($request);

        return ExcelReport::of($resultado['titulo'], $resultado['meta'], $resultado['ventas'], $resultado['columnas'])
                    ->download('ReporteVentas');
    }

    public function r_ventas_pdf(Request $request)
    {

        $resultado = $this->r_ventas($request);

        return PdfReport::of($resultado['titulo'], $resultado['meta'], $resultado['ventas'], $resultado['columnas'])
                    ->stream();
    }

    public function compras_index(Request $request)
    {

        $items = $request->items ?? 10;

        $proveedor_id = $request->proveedor_id;
        $vehiculo_id = $request->vehiculo_id;
        $forma_pago_id = $request->forma_pago_id;
        $estado_id = $request->estado_id;
        $v_fecha_desde = str_replace('/', '-', $request->fecha_desde); // Truco para que haga bien la conversion a Y-m-d
        $v_fecha_hasta = str_replace('/', '-', $request->fecha_hasta); // Truco para que haga bien la conversion a Y-m-d

        $fecha_desde = date('Y-m-d', strtotime($v_fecha_desde));
        $fecha_hasta = date('Y-m-d', strtotime($v_fecha_hasta));

        $sort_by = $request->sort_by;

        $proveedores = Proveedor::all()->sortBy('razon_social');

        $vehiculos = DB::table('vehiculos')
        ->select(DB::raw("CONCAT(marcas.nombre, ' ', modelos.nombre) as nombre"))->distinct()
        ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
        ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
        ->orderBy('nombre')->get();

        $vehiculos_f = DB::table('vehiculos')
        ->select('vehiculos.vehiculo_id', DB::raw("CONCAT(marcas.nombre, ' ', modelos.nombre) as nombre"))
        ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
        ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
        ->orderBy('nombre');
        
        $formas_pagos = DB::table('formas_pagos')->select('nombre')->distinct()->orderBy('nombre')->get();

        $compras = FacturaCompra::select('facturas_compras.factura_nro as nro', 'facturas_compras.fecha_factura as fecha', 'proveedores.razon_social as proveedor', 'vehiculos.nombre as vehiculo',  'facturas_compras.precio_compra as precio', 'monedas.sigla as sigla', 'formas_pagos.nombre as forma_pago', 'facturas_compras.estado as estado')

        ->joinSub($vehiculos_f, 'vehiculos', function ($join) use ($vehiculo_id) {
            $join->on('facturas_compras.vehiculo_id', '=', 'vehiculos.vehiculo_id');
                if (isset($vehiculo_id)) {
                    $join->where('nombre', 'ILIKE',  $vehiculo_id); }
        })

        ->leftJoin('proveedores', 'proveedores.proveedor_id', '=','facturas_compras.proveedor_id')
        ->leftJoin('monedas', 'monedas.moneda_id', '=', 'facturas_compras.moneda_id')
        ->leftJoin('formas_pagos', 'formas_pagos.forma_pago_id', '=', 'facturas_compras.forma_pago_id');
        

        // FILTRADO
        if (isset($proveedor_id)) {
            $compras = $compras->where('facturas_compras.proveedor_id', $proveedor_id); }
        
        if (isset($request->fecha_desde)) {
            if (isset($request->fecha_hasta)) {
                $compras = $compras->whereBetween('facturas_compras.fecha_factura', [$fecha_desde, $fecha_hasta]);        
            } else {
                $compras = $compras->where('facturas_compras.fecha_factura', '>=', $fecha_desde);
            }
        } else {
            if (isset($request->fecha_hasta)) {
                $compras = $compras->where('facturas_compras.fecha_factura', '<=', $fecha_hasta);   
            }
        }

        if (isset($forma_pago_id)) {
            $compras = $compras->where('formas_pagos.nombre', $forma_pago_id); }
        if (isset($estado_id)) {
            if ($estado_id == "Activo") {
                $compras = $compras->where('facturas_compras.estado', True);
            } else {
                $compras = $compras->where('facturas_compras.estado', False);
            }
        }

        // ORDENAMIENTO
        if (isset($sort_by)) {
            switch ($sort_by) { 
                case 'factura_nro':
                    $compras = $compras->orderBy('nro');
                    break;
                
                case 'proveedor':
                    $compras = $compras->orderBy('proveedor');
                    break;
                
                case 'vehiculo':
                    $compras = $compras->orderBy('vehiculo');
                    break;

                case 'forma_pago':
                    $compras = $compras->orderBy('forma_pago');
                    break;

                case 'estado':
                    $compras = $compras->orderBy('estado', 'desc');
                    break;
            }
        }

        // EJECUTAR CONSULTA
        $compras = $compras->paginate($items);

        return view('reportes', 
            ['var' => 'r_compras_index', 'compras' => $compras, 'proveedores' => $proveedores, 'vehiculos' => $vehiculos, 'formas_pagos' => $formas_pagos, 'items' => $items, 'proveedor_sel' => $proveedor_id, 'vehiculo_sel' => $vehiculo_id, 'forma_pago_sel' => $forma_pago_id, 'fecha_desde_sel' => $request->fecha_desde, 'fecha_hasta_sel' => $request->fecha_hasta, 'estado_sel' => $estado_id, 'sort_sel'=> $sort_by]);
    }

    public function r_compras(Request $request)
    {

        $items = $request->items ?? 10;

        $proveedor_id = $request->proveedor_id;
        $vehiculo_id = $request->vehiculo_id;
        $forma_pago_id = $request->forma_pago_id;
        $estado_id = $request->estado_id;
        $v_fecha_desde = str_replace('/', '-', $request->fecha_desde); // Truco para que haga bien la conversion a Y-m-d
        $v_fecha_hasta = str_replace('/', '-', $request->fecha_hasta); // Truco para que haga bien la conversion a Y-m-d

        $fecha_desde = date('Y-m-d', strtotime($v_fecha_desde));
        $fecha_hasta = date('Y-m-d', strtotime($v_fecha_hasta));

        $sort_by = $request->sort_by;
        $meta = array();

        $proveedores = Proveedor::all()->sortBy('razon_social');

        $vehiculos = DB::table('vehiculos')
        ->select(DB::raw("CONCAT(marcas.nombre, ' ', modelos.nombre) as nombre"))->distinct()
        ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
        ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
        ->orderBy('nombre')->get();

        $vehiculos_f = DB::table('vehiculos')
        ->select('vehiculos.vehiculo_id', DB::raw("CONCAT(marcas.nombre, ' ', modelos.nombre) as nombre"))
        ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
        ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
        ->orderBy('nombre');
        
        $formas_pagos = DB::table('formas_pagos')->select('nombre')->distinct()->orderBy('nombre')->get();

        $compras = FacturaCompra::select('facturas_compras.factura_nro as nro', 'facturas_compras.fecha_factura as fecha', 'proveedores.razon_social as proveedor', 'vehiculos.nombre as vehiculo',  'facturas_compras.precio_compra as precio', 'monedas.sigla as sigla', 'formas_pagos.nombre as forma_pago', 'facturas_compras.estado as estado')

        ->joinSub($vehiculos_f, 'vehiculos', function ($join) use ($vehiculo_id) {
            $join->on('facturas_compras.vehiculo_id', '=', 'vehiculos.vehiculo_id');
                if (isset($vehiculo_id)) {
                    $join->where('nombre', 'ILIKE',  $vehiculo_id); }
        })

        ->leftJoin('proveedores', 'proveedores.proveedor_id', '=','facturas_compras.proveedor_id')
        ->leftJoin('monedas', 'monedas.moneda_id', '=', 'facturas_compras.moneda_id')
        ->leftJoin('formas_pagos', 'formas_pagos.forma_pago_id', '=', 'facturas_compras.forma_pago_id');
        

        // FILTRADO
        if (isset($proveedor_id)) {
            $compras = $compras->where('facturas_compras.proveedor_id', $proveedor_id); 
            $proveedor_nombre = $compras->get();
            $meta += ['Proveedor' => $proveedor_nombre[0]->proveedor]; 
        }
        
        if (isset($request->fecha_desde)) {
            if (isset($request->fecha_hasta)) {
                $compras = $compras->whereBetween('facturas_compras.fecha_factura', [$fecha_desde, $fecha_hasta]);
                $meta += ['Entre Fechas' => $request->fecha_desde . ' - ' . $request->fecha_hasta]; 
            } else {
                $compras = $compras->where('facturas_compras.fecha_factura', '>=', $fecha_desde);
                $meta += ['Desde la Fecha' => $request->fecha_desde]; 
            }
        } else {
            if (isset($request->fecha_hasta)) {
                $compras = $compras->where('facturas_compras.fecha_factura', '<=', $fecha_hasta);
                $meta += ['Hasta la Fecha' => $request->fecha_hasta]; 
            }
        }

        if (isset($forma_pago_id)) {
            $compras = $compras->where('formas_pagos.nombre', $forma_pago_id); 
            $forma_pago_nombre = $compras->get();
            $meta += ['Forma de Pago' => $forma_pago_nombre[0]->forma_pago]; 
        }

        if (isset($vehiculo_id)) {
            $compras = $compras->where('vehiculos.nombre', $vehiculo_id); 
            $vehiculo_nombre = $compras->get();
            $meta += ['Vehículo' => $vehiculo_nombre[0]->vehiculo]; 
        }

        if (isset($estado_id)) {
            if ($estado_id == "Activo") {
                $compras = $compras->where('facturas_compras.estado', True);
                $meta += ['Estado' => 'Activo'];
            } else {
                $compras = $compras->where('facturas_compras.estado', False);
                $meta += ['Estado' => 'Anulado'];
            }
        }

        // ORDENAMIENTO
        if (isset($sort_by)) {
            switch ($sort_by) { 
                case 'factura_nro':
                    $compras = $compras->orderBy('nro');
                    $meta += ['Ordenado por' => 'Número de Factura'];
                    break;
                
                case 'proveedor':
                    $compras = $compras->orderBy('proveedor');
                    $meta += ['Ordenado por' => 'Proveedor'];
                    break;
                
                case 'vehiculo':
                    $compras = $compras->orderBy('vehiculo');
                    $meta += ['Ordenado por' => 'Vehículo'];
                    break;

                case 'forma_pago':
                    $compras = $compras->orderBy('forma_pago');
                    $meta += ['Ordenado por' => 'Forma de Pago'];
                    break;

                case 'estado':
                    $compras = $compras->orderBy('estado', 'desc');
                    $meta += ['Ordenado por' => 'Estado'];
                    break;
            }
        }

        $titulo = 'Reporte de Compras'; // Report titulo

        $columnas = [ // Set Column to be displayed
            'Fac. Nro' => 'nro',
            'Fecha' => function($compras) { 
                return date("d/m/Y", strtotime($compras->fecha)); },
            'Proveedor' => 'proveedor',
            'Vehículo' => 'vehiculo',
            'Costo' => function($compras) {
                            switch ($compras->sigla) { 
                                case 'Gs':
                                    return number_format($compras->precio, $decimals = 0 , $dec_point = "," , $thousands_sep = "." ) . ' ' . $compras->sigla;
                                    break;
                                default:
                                    return number_format($compras->precio, $decimals = 2 , $dec_point = "," , $thousands_sep = "." ) . ' ' . $compras->sigla;
                                    break;
                            }
                        },
            'Forma Pago' => 'forma_pago',
            'Estado' => function($compras) { 
                return ($compras->estado == 1) ? 'Activo' : 'Anulado'; }
        ];

        return (['titulo' => $titulo, 'meta' => $meta, 'compras' => $compras, 'columnas' => $columnas]);
    }

    public function r_compras_xls(Request $request)
    {

        $resultado = $this->r_compras($request);

        return ExcelReport::of($resultado['titulo'], $resultado['meta'], $resultado['compras'], $resultado['columnas'])
                    ->download('ReporteCompras');
    }

    public function r_compras_pdf(Request $request)
    {

        $resultado = $this->r_compras($request);

        return PdfReport::of($resultado['titulo'], $resultado['meta'], $resultado['compras'], $resultado['columnas'])
                    ->stream();
    }

    public function auditorias_index(Request $request)
    {
        $items = $request->items ?? 10;
        $usuario_id = $request->usuario_id;
        $tipo_id = $request->tipo_id;
        $tabla_id = $request->tabla_id;

        $v_fecha_desde = str_replace('/', '-', $request->fecha_desde); // Truco para que haga bien la conversion a Y-m-d
        $v_fecha_hasta = str_replace('/', '-', $request->fecha_hasta); // Truco para que haga bien la conversion a Y-m-d

        $fecha_desde = date('Y-m-d', strtotime($v_fecha_desde));
        $fecha_hasta = date('Y-m-d', strtotime($v_fecha_hasta));

        $sort_by = $request->sort_by;

        $usuarios = User::all()->sortBy('nombre');
        
        $tablas = DB::table('auditorias')->select('tabla as nombre')->distinct()->orderBy('tabla')->get();

        $tipos = DB::table('auditorias as tipos')
        ->select('tipos.auditoria_id', DB::raw("CASE WHEN tipos.tipo_transaccion = 'I' THEN 'Insertar' ELSE CASE WHEN tipos.tipo_transaccion = 'U' THEN 'Actualizar' ELSE 'Borrar' END END AS tipo_nombre"));
        
        $auditorias = Auditoria::select('usuarios.nombre as usuario', 'tipos.tipo_nombre as tipo', 'auditorias.tabla as tabla', 'auditorias.tabla_pk as pk', 'auditorias.campo as campo', 'auditorias.valor_viejo as valor_viejo', 'auditorias.valor_nuevo as valor_nuevo', 'auditorias.fecha as fecha') 
        ->leftJoin('usuarios', 'usuarios.id', '=', 'auditorias.usuario_id')
        ->joinSub($tipos, 'tipos', function ($join) use ($tipo_id) {
            $join->on('tipos.auditoria_id', '=', 'auditorias.auditoria_id');
                if (isset($tipo_id)) {
                    $join->where('tipo_nombre', 'ILIKE',  $tipo_id); }
        });

        // FILTRADO
        if (isset($usuario_id)) {
            $auditorias = $auditorias->where('auditorias.usuario_id', $usuario_id); }
        if (isset($tipo_id)) {
            $auditorias = $auditorias->where('tipos.tipo_nombre', $tipo_id); }
        if (isset($tabla_id)) {
            $auditorias = $auditorias->where('auditorias.tabla', $tabla_id); }
        if (isset($request->fecha_desde)) {
            if (isset($request->fecha_hasta)) {
                $auditorias = $auditorias->whereBetween('auditorias.fecha', [$fecha_desde, $fecha_hasta]);        
            } else {
                $auditorias = $auditorias->where('auditorias.fecha', '>=', $fecha_desde);
            }
        } else {
            if (isset($request->fecha_hasta)) {
                $auditorias = $auditorias->where('auditorias.fecha', '<=', $fecha_hasta);   
            }
        }
        

        // ORDENAMIENTO
        if (isset($sort_by)) {
            switch ($sort_by) { 
                case 'usuario':
                    $auditorias = $auditorias->orderBy('usuario');
                    break;
                
                case 'tipo':
                    $auditorias = $auditorias->orderBy('tipo');
                    break;

                case 'tabla':
                    $auditorias = $auditorias->orderBy('tabla');
                    break;

                case 'fecha':
                    $auditorias = $auditorias->orderBy('fecha');
                    break;

            }
        }

        // EJECUTAR CONSULTA
        $auditorias = $auditorias->paginate($items);

        return view('reportes', 
            ['var' => 'r_auditorias_index', 'auditorias' => $auditorias, 'usuarios' => $usuarios, 'tablas' => $tablas, 'items' => $items, 'usuario_sel'=> $usuario_id, 'tipo_sel'=> $tipo_id, 'tabla_sel'=> $tabla_id, 'fecha_desde_sel' => $request->fecha_desde, 'fecha_hasta_sel' => $request->fecha_hasta, 'sort_sel'=> $sort_by]);
    }
       
    public function r_auditorias(Request $request)
    {
        $items = $request->items ?? 10;
        $usuario_id = $request->usuario_id;
        $tipo_id = $request->tipo_id;
        $tabla_id = $request->tabla_id;

        $v_fecha_desde = str_replace('/', '-', $request->fecha_desde); // Truco para que haga bien la conversion a Y-m-d
        $v_fecha_hasta = str_replace('/', '-', $request->fecha_hasta); // Truco para que haga bien la conversion a Y-m-d

        $fecha_desde = date('Y-m-d', strtotime($v_fecha_desde));
        $fecha_hasta = date('Y-m-d', strtotime($v_fecha_hasta));

        $sort_by = $request->sort_by;
        $meta = array();

        $usuarios = User::all()->sortBy('nombre');
        
        $tablas = DB::table('auditorias')->select('tabla as nombre')->distinct()->orderBy('tabla')->get();
        
        $auditorias = Auditoria::select('usuarios.nombre as usuario', 'auditorias.tipo_transaccion as tipo', 'auditorias.tabla as tabla', 'auditorias.tabla_pk as pk', 'auditorias.campo as campo', 'auditorias.valor_viejo as valor_viejo', 'auditorias.valor_nuevo as valor_nuevo', 'auditorias.fecha as fecha') 
        ->leftJoin('usuarios', 'usuarios.id', '=', 'auditorias.usuario_id');

        // FILTRADO
        if (isset($usuario_id)) {
            $auditorias = $auditorias->where('auditorias.usuario_id', $usuario_id); 
            $usuario_nombre = $auditorias->get();
            $meta += ['Usuario' => $usuario_nombre[0]->usuario]; 
        }
        if (isset($tipo_id)) {
            $auditorias = $auditorias->where('auditorias.tipo_transaccion', $tipo_id); 
            switch ($tipo_id) { 
                case 'U':
                $meta += ['Tipo' => 'Actualizar'];
                break;

                case 'I':
                $meta += ['Tipo' => 'Insertar'];
                break;

                case 'D':
                $meta += ['Tipo' => 'Borrar'];
                break;
            }
        }
        if (isset($tabla_id)) {
            $auditorias = $auditorias->where('auditorias.tabla', $tabla_id); 
            $tabla_nombre = $auditorias->get();
            $meta += ['Tabla' => $tabla_nombre[0]->tabla];
        }
        if (isset($request->fecha_desde)) {
            if (isset($request->fecha_hasta)) {
                $auditorias = $auditorias->whereBetween('auditorias.fecha', [$fecha_desde, $fecha_hasta]);
                $meta += ['Entre Fechas' => $request->fecha_desde . ' - ' . $request->fecha_hasta];         
            } else {
                $auditorias = $auditorias->where('auditorias.fecha', '>=', $fecha_desde);
                $meta += ['Desde la Fecha' => $request->fecha_desde]; 
            }
        } else {
            if (isset($request->fecha_hasta)) {
                $auditorias = $auditorias->where('auditorias.fecha', '<=', $fecha_hasta);
                $meta += ['Hasta la Fecha' => $request->fecha_hasta];  
            }
        }
        

        // ORDENAMIENTO
        if (isset($sort_by)) {
            switch ($sort_by) { 
                case 'usuario':
                    $auditorias = $auditorias->orderBy('usuario');
                    $meta += ['Ordenado por' => 'Usuario'];
                    break;
                
                case 'tipo':
                    $auditorias = $auditorias->orderBy('tipo');
                    $meta += ['Ordenado por' => 'Tipo de Transacción'];
                    break;

                case 'tabla':
                    $auditorias = $auditorias->orderBy('tabla');
                    $meta += ['Ordenado por' => 'Tabla'];
                    break;

                case 'fecha':
                    $auditorias = $auditorias->orderBy('fecha');
                    $meta += ['Ordenado por' => 'Fecha'];
                    break;

            }
        }

        $titulo = 'Reporte de Auditorias'; // Report titulo

        $columnas = [ // Set Column to be displayed
            'Usuario' => 'usuario',
            'Tipo' => function($auditorias) {
                            switch ($auditorias->tipo) { 
                                case 'I':
                                    return 'Insertar';
                                    break;
                                case 'U':
                                    return 'Actualizar';
                                    break;
                                case 'D':
                                    return 'Borrar';
                                    break;
                            }
                        },
            'Tabla' => 'tabla',
            'PK de la Tabla' => 'pk',
            'Campo' => 'campo',
            'Valor Viejo' => 'valor_viejo',
            'Valor Nuevo' => 'valor_nuevo',
            'Fecha' => function($compras) { 
                return date("d/m/Y", strtotime($compras->fecha)); }
        ];

        return (['titulo' => $titulo, 'meta' => $meta, 'auditorias' => $auditorias, 'columnas' => $columnas]);
    }

    public function r_auditorias_xls(Request $request)
    {

        $resultado = $this->r_auditorias($request);

        return ExcelReport::of($resultado['titulo'], $resultado['meta'], $resultado['auditorias'], $resultado['columnas'])
                    /*->editColumns(['Disponibilidad'], [ // Mass edit column
                        'class' => 'bold'
                    ])
                    ->editColumns(['año'], [ // Mass edit column
                        'class' => 'left'
                    ])*/
                    // ->limit(20) // Limit record to be showed
                    ->download('ReporteAuditorias');
    }

    public function r_auditorias_pdf(Request $request)
    {

        $resultado = $this->r_auditorias($request);

        return PdfReport::of($resultado['titulo'], $resultado['meta'], $resultado['auditorias'], $resultado['columnas'])
                    /*->editColumns(['Disponibilidad'], [ // Mass edit column
                        'class' => 'bold'
                    ])
                    ->editColumns(['año'], [ // Mass edit column
                        'class' => 'left'
                    ])*/
                    // ->limit(20) // Limit record to be showed
                    ->stream();
    }


}