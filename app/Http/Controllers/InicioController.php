<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;


class InicioController extends Controller
{
    public function main_autocomplete(Request $request)
    {
        $apartados = ['Vehiculos -> Vehiculos', 'Vehiculos -> Marcas', 'Vehiculos -> Modelos', 'Vehiculos -> Tipos', 'Vehiculos -> Colores',
                        'Ventas -> Facturas', 'Ventas -> Clientes', 'Ventas -> Cuotas', 'Ventas -> Pagares', 'Ventas -> Recibos',
                        'Compras -> Facturas', 'Compras -> Proveedores', 'Compras -> Cuotas', 'Compras -> Recibos',
                        'Reportes -> Vehiculos', 'Reportes -> Ventas', 'Reportes -> Compras', 'Reportes -> Auditorias',
                        'Sistema -> Usuarios', 'Sistema -> Moneda', 'Sistema -> Depositos', 'Sistema -> Ciudades', 'Sistema -> Paises'];

        return response()->json($apartados);
    }

    public function main_autocomplete_filtrar(Request $request)
    {
        if (!(blank($request->main_buscar))) {
            $apartados = ['Vehiculos -> Vehiculos', 'Vehiculos -> Marcas', 'Vehiculos -> Modelos', 'Vehiculos -> Tipos', 'Vehiculos -> Colores',
                        'Ventas -> Facturas', 'Ventas -> Clientes', 'Ventas -> Cuotas', 'Ventas -> Pagares', 'Ventas -> Recibos',
                        'Compras -> Facturas', 'Compras -> Proveedores', 'Compras -> Cuotas', 'Compras -> Recibos',
                        'Reportes -> Vehiculos', 'Reportes -> Ventas', 'Reportes -> Compras', 'Reportes -> Auditorias',
                        'Sistema -> Usuarios', 'Sistema -> Moneda', 'Sistema -> Depositos', 'Sistema -> Ciudades', 'Sistema -> Paises'];

            $rutas = ['vehiculos_index', 'marcas_index', 'modelos_index', 'tipos_index', 'colores_index',
                        'facturasv_index', 'clientes_index', 'cuotasv_index', 'pagares_index', 'recibosv_index',
                        'facturasc_index', 'proveedores_index', 'cuotasc_index', 'recibosc_index',
                        'r_vehiculos_index', 'r_ventas_index', 'r_compras_index', 'r_auditorias_index', 
                        'usuarios_index', 'monedas_index', 'depositos_index', 'ciudades_index', 'paises_index'];

            foreach ($apartados as $clave => $valor) {
                if ($valor == $request->main_buscar) {
                    $retorno = $rutas[$clave];
                    return redirect(route($retorno));
                }
            }
        }
        
        //return back();
        return view('site_map', ['var' => 'site_map', 'apartados' => $apartados, 'rutas' => $rutas]);
    }

    public function inicio_index(Request $request)
    {
        /* Código para extraer los datos para los recuadros superiores */

        $ventas_mensuales = DB::table('facturas_ventas as fv')
        ->select(DB::raw("to_char(SUM(fv.precio_venta * m.cotizacion), 'FM999G999G999G999')"))
        ->leftJoin('monedas as m', 'm.moneda_id', 'fv.moneda_id')
        ->whereBetween('fv.fecha_factura', [DB::raw("now() - '1 months'::interval"), DB::raw("now()")])
        ->get();
        
        $ventas_anuales = DB::table('facturas_ventas as fv')
        ->select(DB::raw("to_char(SUM(fv.precio_venta * m.cotizacion), 'FM999G999G999G999')"))
        ->leftJoin('monedas as m', 'm.moneda_id', 'fv.moneda_id')
        ->whereBetween('fv.fecha_factura', [DB::raw("now() - '1 year'::interval"), DB::raw("now()")])
        ->get();

        $vehiculos_facturados = $this->objectToArray(DB::table('facturas_ventas')->select('vehiculo_id')->get());

        $cantidad_vendido = DB::table('vehiculos as v')
        ->select('v.vehiculo_id')
        ->whereIn('v.vehiculo_id', $vehiculos_facturados)
        ->count();

        $cantidad_no_vendido = DB::table('vehiculos as v')
        ->select('v.vehiculo_id')
        ->whereNotIn('v.vehiculo_id', $vehiculos_facturados)
        ->whereNotNull('deposito_id')
        ->count();

        $porcentaje_vendido = $cantidad_vendido / ($cantidad_vendido + $cantidad_no_vendido) * 100;
        $porcentaje_vendido = number_format($porcentaje_vendido, $decimals = 0 , $dec_point = "," , $thousands_sep = "." );

        $clientes_activos = DB::table('facturas_ventas as fv')
        ->select('fv.cliente_id')
        ->where('fv.estado', True)
        ->whereBetween('fv.fecha_factura', [DB::raw("now() - '1 year'::interval"), DB::raw("now()")])
        ->distinct()->get()->count();

        /* Código para extraer el resumen de ganancias de los últimos 8 meses */
        
        $mes_1 = DB::table('facturas_ventas as fv')
        ->select(DB::raw("COALESCE(SUM(fv.precio_venta * m.cotizacion),0)"))
        ->leftJoin('monedas as m', 'm.moneda_id', 'fv.moneda_id')
        ->where('fv.estado', True)
        ->whereBetween('fv.fecha_factura', [DB::raw("(date_trunc('month', current_date - '7 month'::interval))::date"), DB::raw("(date_trunc('month', current_date - '7 month'::interval) + '1 month'::interval - '1 day'::interval)::date")])
        ->get();

        $mes_2 = DB::table('facturas_ventas as fv')
        ->select(DB::raw("COALESCE(SUM(fv.precio_venta * m.cotizacion),0)"))
        ->leftJoin('monedas as m', 'm.moneda_id', 'fv.moneda_id')
        ->where('fv.estado', True)
        ->whereBetween('fv.fecha_factura', [DB::raw("(date_trunc('month', current_date - '6 month'::interval))::date"), DB::raw("(date_trunc('month', current_date - '6 month'::interval) + '1 month'::interval - '1 day'::interval)::date")])
        ->get();

        $mes_3 = DB::table('facturas_ventas as fv')
        ->select(DB::raw("COALESCE(SUM(fv.precio_venta * m.cotizacion),0)"))
        ->leftJoin('monedas as m', 'm.moneda_id', 'fv.moneda_id')
        ->where('fv.estado', True)
        ->whereBetween('fv.fecha_factura', [DB::raw("(date_trunc('month', current_date - '5 month'::interval))::date"), DB::raw("(date_trunc('month', current_date - '5 month'::interval) + '1 month'::interval - '1 day'::interval)::date")])
        ->get();

        $mes_4 = DB::table('facturas_ventas as fv')
        ->select(DB::raw("COALESCE(SUM(fv.precio_venta * m.cotizacion),0)"))
        ->leftJoin('monedas as m', 'm.moneda_id', 'fv.moneda_id')
        ->where('fv.estado', True)
        ->whereBetween('fv.fecha_factura', [DB::raw("(date_trunc('month', current_date - '4 month'::interval))::date"), DB::raw("(date_trunc('month', current_date - '4 month'::interval) + '1 month'::interval - '1 day'::interval)::date")])
        ->get();

        $mes_5 = DB::table('facturas_ventas as fv')
        ->select(DB::raw("COALESCE(SUM(fv.precio_venta * m.cotizacion),0)"))
        ->leftJoin('monedas as m', 'm.moneda_id', 'fv.moneda_id')
        ->where('fv.estado', True)
        ->whereBetween('fv.fecha_factura', [DB::raw("(date_trunc('month', current_date - '3 month'::interval))::date"), DB::raw("(date_trunc('month', current_date - '3 month'::interval) + '1 month'::interval - '1 day'::interval)::date")])
        ->get();

        $mes_6 = DB::table('facturas_ventas as fv')
        ->select(DB::raw("COALESCE(SUM(fv.precio_venta * m.cotizacion),0)"))
        ->leftJoin('monedas as m', 'm.moneda_id', 'fv.moneda_id')
        ->where('fv.estado', True)
        ->whereBetween('fv.fecha_factura', [DB::raw("(date_trunc('month', current_date - '2 month'::interval))::date"), DB::raw("(date_trunc('month', current_date - '2 month'::interval) + '1 month'::interval - '1 day'::interval)::date")])
        ->get();

        $mes_7 = DB::table('facturas_ventas as fv')
        ->select(DB::raw("COALESCE(SUM(fv.precio_venta * m.cotizacion),0)"))
        ->leftJoin('monedas as m', 'm.moneda_id', 'fv.moneda_id')
        ->where('fv.estado', True)
        ->whereBetween('fv.fecha_factura', [DB::raw("(date_trunc('month', current_date - '1 month'::interval))::date"), DB::raw("(date_trunc('month', current_date - '1 month'::interval) + '1 month'::interval - '1 day'::interval)::date")])
        ->get();

        $mes_8 = DB::table('facturas_ventas as fv')
        ->select(DB::raw("COALESCE(SUM(fv.precio_venta * m.cotizacion),0)"))
        ->leftJoin('monedas as m', 'm.moneda_id', 'fv.moneda_id')
        ->where('fv.estado', True)
        ->whereBetween('fv.fecha_factura', [DB::raw("(date_trunc('month', current_date))::date"), DB::raw("now()")])
        ->get();

        $meses = ['', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Set', 'Oct', 'Nov', 'Dic'];

        $resumen_eje_x = [$meses[date('n', strtotime('now - 7 month'))], $meses[date('n', strtotime('now - 6 month'))], $meses[date('n', strtotime('now - 5 month'))], $meses[date('n', strtotime('now - 4 month'))], $meses[date('n', strtotime('now - 3 month'))], $meses[date('n', strtotime('now - 2 month'))], $meses[date('n', strtotime('now - 1 month'))], $meses[date('n', strtotime('now'))]];

        $resumen_puntos = [$mes_1[0]->coalesce, $mes_2[0]->coalesce, $mes_3[0]->coalesce, $mes_4[0]->coalesce, $mes_5[0]->coalesce, $mes_6[0]->coalesce, $mes_7[0]->coalesce, $mes_8[0]->coalesce];

        /* Código para dibujar el gráfico torta con las Fuentes de Ingreso (Efectivo, Cheque, Depósito Bancario y Cuotas) */
        $forma_pago_efectivo = $this->objectToArray(DB::table('formas_pagos')->select('forma_pago_id')->where('nombre', 'Efectivo')->get());
        $forma_pago_cheque = $this->objectToArray(DB::table('formas_pagos')->select('forma_pago_id')->where('nombre', 'Cheque')->get());
        $forma_pago_cuotas = $this->objectToArray(DB::table('formas_pagos')->select('forma_pago_id')->where('nombre', 'Cuotas')->get());
        $forma_pago_deposito = $this->objectToArray(DB::table('formas_pagos')->select('forma_pago_id')->where('nombre', 'DepositoBancario')->get());
        
        $facturas_ventas_efectivo = DB::table('facturas_ventas as fv')
        ->where('fv.estado', True)
        ->whereIn('fv.forma_pago_id', $forma_pago_efectivo)
        ->count();

        $facturas_ventas_cheques = DB::table('facturas_ventas as fv')
        ->where('fv.estado', True)
        ->whereIn('fv.forma_pago_id', $forma_pago_cheque)
        ->count();

        $facturas_ventas_cuotas = DB::table('facturas_ventas as fv')
        ->where('fv.estado', True)
        ->whereIn('fv.forma_pago_id', $forma_pago_cuotas)
        ->count();

        $facturas_ventas_depositos = DB::table('facturas_ventas as fv')
        ->where('fv.estado', True)
        ->whereIn('fv.forma_pago_id', $forma_pago_deposito)
        ->count();

        return view('welcome', ['ventas_mensuales' => $ventas_mensuales[0]->to_char, 'ventas_anuales' => $ventas_anuales[0]->to_char, 'porcentaje_vendido' => $porcentaje_vendido, 'clientes_activos' => $clientes_activos, 'facturas_ventas_efectivo' => $facturas_ventas_efectivo, 'facturas_ventas_cheques' => $facturas_ventas_cheques, 'facturas_ventas_cuotas' => $facturas_ventas_cuotas, 'facturas_ventas_depositos' => $facturas_ventas_depositos, 'resumen_eje_x' => $resumen_eje_x, 'resumen_puntos' => $resumen_puntos]);
    }

    function objectToArray($object)
    {
        return @json_decode(json_encode($object), true);
    }
}
