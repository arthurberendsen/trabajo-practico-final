<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;

use App\Models\Auditoria;
use App\Models\Color;
use App\Models\Marca;
use App\Models\Modelo;
use App\Models\Moneda;
use App\Models\Tipo;
use App\Models\Vehiculo;
use App\Models\Deposito;

class VehiculosController extends Controller
{

    public function colores_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;
        $colores = Color::select('*');

        if (!(blank($buscar))) {
            $colores = $colores->orWhere('colores.nombre', 'ILIKE', '%'. $buscar . '%');
        }

        $colores = $colores->orderBy('color_id')->paginate($items);
        return view('vehiculos', ['var' => 'colores_index', 'colores' => $colores, 'items' => $items, 'buscar' => $buscar]);
    }

    public function colores_create()
    {
        return view('vehiculos', ['var' => 'colores_create']);
    }

    public function colores_store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $color = new Color;
        $color->nombre = $request->nombre;
        $color->save();
        
        $this->auditoria($color, 'I');
        return redirect(route('colores_index'))->with('successMessage', 'Color añadido correctamente.');
    }

    public function colores_show($id)
    {
        $color = Color::findOrFail($id);
        return view('vehiculos', ['var' => 'colores_show', 'color' => $color]);
    }

    public function colores_edit($id)
    {
        $color = Color::findOrFail($id);
        return view('vehiculos', ['var' => 'colores_edit', 'color' => $color]);
    }

    public function colores_update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $color = Color::findOrFail($id);
        $viejo = Color::findOrFail($id);
        $color->nombre = $request->nombre;
        $color->save();
        $this->auditoria($color, 'U', $viejo);
        return redirect(route('colores_index'))->with('successMessage', 'Color actualizado correctamente.');
    }

    public function colores_destroy($id)
    {
        try { 
            $color = Color::findOrFail($id);
            $color->delete();
            $this->auditoria($color, 'D');
            return redirect(URL::previous())->with('successMessage', 'Color borrado correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'El color no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }



    public function tipos_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;
        $tipos = Tipo::select('*');

        if (!(blank($buscar))) {
            $tipos = $tipos->orWhere('tipos.nombre', 'ILIKE', '%'. $buscar . '%');
        }

        $tipos = $tipos->orderBy('tipo_id')->paginate($items);

        return view('vehiculos', ['var' => 'tipos_index', 'tipos' => $tipos, 'items' => $items, 'buscar' => $buscar]);
    }

    public function tipos_create()
    {
        return view('vehiculos', ['var' => 'tipos_create']);
    }

    public function tipos_store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $tipo = new Tipo;
        $tipo->nombre = $request->nombre;
        $tipo->save();

        $this->auditoria($tipo, 'I');
        return redirect(route('tipos_index'))->with('successMessage', 'Tipo añadido correctamente.');
    }

    public function tipos_show($id)
    {
        $tipo = Tipo::findOrFail($id);
        return view('vehiculos', ['var' => 'tipos_show', 'tipo' => $tipo]);
    }

    public function tipos_edit($id)
    {
        $tipo = Tipo::findOrFail($id);
        return view('vehiculos', ['var' => 'tipos_edit', 'tipo' => $tipo]);
    }

    public function tipos_update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $tipo = Tipo::findOrFail($id);
        $viejo = Tipo::findOrFail($id);
        $tipo->nombre = $request->nombre;
        $tipo->save();

        $this->auditoria($tipo, 'U', $viejo);
        return redirect(route('tipos_index'))->with('successMessage', 'Tipo actualizado correctamente.');
    }

    public function tipos_destroy($id)
    {
        try { 
            $tipo = Tipo::findOrFail($id);
            $tipo->delete();
            $this->auditoria($tipo, 'D');
            return redirect(URL::previous())->with('successMessage', 'Tipo borrado correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'El Tipo no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }




    public function modelos_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $marcas = Marca::all();
        $modelos = Modelo::select(['modelos.modelo_id as modelo_id', 'modelos.nombre as nombre', 'marcas.nombre as marca'])
                   ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id');

        if (!(blank($buscar))) {
            $modelos = $modelos->orWhere('marcas.nombre', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('modelos.nombre', 'ILIKE', '%'. $buscar . '%');
        }

        $modelos = $modelos->orderBy('modelo_id')->paginate($items);

        return view('vehiculos', ['var' => 'modelos_index', 'modelos' => $modelos, 'items' => $items, 'marcas' => $marcas, 'buscar' => $buscar]);
    }

    public function modelos_create()
    {
        $marcas = Marca::all()->sortBy('nombre');
        return view('vehiculos', ['var' => 'modelos_create', 'marcas' => $marcas]);
    }

    public function modelos_store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'marca_id' => 'required'
        ]);

        $modelo = new Modelo;
        $modelo->nombre = $request->nombre;
        $modelo->marca_id = $request->marca_id;
        $modelo->save();

        $this->auditoria($modelo, 'I');
        return redirect(route('modelos_index'))->with('successMessage', 'Modelo añadido correctamente.');
    }

    public function modelos_show($id)
    {
        $modelo = Modelo::findOrFail($id);
        $marca = Marca::findOrFail($modelo->marca_id);
        return view('vehiculos', ['var' => 'modelos_show', 'modelo' => $modelo, 'marca' => $marca]);
    }

    public function modelos_edit($id)
    {
        $modelo = Modelo::findOrFail($id);
        $marcas = Marca::all()->sortBy('nombre');
        return view('vehiculos', ['var' => 'modelos_edit', 'modelo' => $modelo, 'marcas' => $marcas]);
    }

    public function modelos_update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'marca_id' => 'required'
        ]);

        $modelo = Modelo::findOrFail($id);
        $viejo = Modelo::findOrFail($id);
        $modelo->nombre = $request->nombre;
        $modelo->marca_id = $request->marca_id;
        $modelo->save();

        $this->auditoria($modelo, 'U', $viejo);
        return redirect(route('modelos_index'))->with('successMessage', 'Modelo actualizado correctamente.');
    }

    public function modelos_destroy($id)
    {
        try { 
            $modelo = Modelo::findOrFail($id);
            $modelo->delete();
            $this->auditoria($modelo, 'D');
            return redirect(URL::previous())->with('successMessage', 'Modelo borrado correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'El Modelo no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }



    public function marcas_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $marcas = Marca::select('*');
        if (!(blank($buscar))) {
            $marcas = $marcas->orWhere('marcas.nombre', 'ILIKE', '%'. $buscar . '%');
        }

        $marcas = $marcas->orderBy('marca_id')->paginate($items);

        return view('vehiculos', ['var' => 'marcas_index', 'marcas' => $marcas, 'items' => $items, 'buscar' => $buscar]);
    }

    public function marcas_create()
    {
        return view('vehiculos', ['var' => 'marcas_create']);
    }

    public function marcas_store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $marca = new Marca;
        $marca->nombre = $request->nombre;
        $marca->save();

        $this->auditoria($marca, 'I');
        return redirect(route('marcas_index'))->with('successMessage', 'Marca añadido correctamente.');
    }

    public function marcas_show($id)
    {
        $marca = Marca::findOrFail($id);
        return view('vehiculos', ['var' => 'marcas_show', 'marca' => $marca]);
    }

    public function marcas_edit($id)
    {
        $marca = Marca::findOrFail($id);
        return view('vehiculos', ['var' => 'marcas_edit', 'marca' => $marca]);
    }

    public function marcas_update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $marca = Marca::findOrFail($id);
        $viejo = Marca::findOrFail($id);
        $marca->nombre = $request->nombre;
        $marca->save();

        $this->auditoria($marca, 'U', $viejo);
        return redirect(route('marcas_index'))->with('successMessage', 'Marca actualizada correctamente.');
    }

    public function marcas_destroy($id)
    {
        try { 
            $marca = Marca::findOrFail($id);
            $marca->delete();
            $this->auditoria($marca, 'D');
            return redirect(URL::previous())->with('successMessage', 'Marca borrada correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'La marca no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }


    public function vehiculos_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $vehiculos = Vehiculo::select(['vehiculos.vehiculo_id as vehiculo_id', 'modelos.nombre as modelo', 'marcas.nombre as marca', 'colores.nombre as color', 'vehiculos.anho_fabricacion as anho', 'vehiculos.costo as costo', 'monedas.sigla as sigla', 'depositos.nombre as deposito', 'vehiculos.estado as estado'])
                     ->leftJoin('modelos', 'modelos.modelo_id', '=', 'vehiculos.modelo_id')
                     ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
                     ->leftJoin('monedas', 'monedas.moneda_id', '=', 'vehiculos.moneda_id')
                     ->leftJoin('colores', 'colores.color_id', '=', 'vehiculos.color_id')
                     ->leftJoin('depositos', 'depositos.deposito_id', '=', 'vehiculos.deposito_id');
        
        if (!(blank($buscar))) {
            $vehiculos = $vehiculos->OrWhere('marcas.nombre', 'ILIKE', '%'. $buscar . '%')
                                    ->OrWhere('modelos.nombre', 'ILIKE', '%'. $buscar . '%')
                                    ->OrWhere('vehiculos.anho_fabricacion', 'ILIKE', '%'. $buscar . '%')
                                    ->OrWhere('depositos.nombre', 'ILIKE', '%'. $buscar . '%')
                                    ->OrWhere('colores.nombre', 'ILIKE', '%'. $buscar . '%');
        }
        
        $vehiculos = $vehiculos->orderBy('marca')->paginate($items);

        return view('vehiculos', ['var' => 'vehiculos_index', 'vehiculos' => $vehiculos, 'items' => $items, 'buscar' => $buscar]);
    }

    public function vehiculos_create()
    {
        $modelos = Modelo::select(['modelos.modelo_id as modelo_id', 'modelos.nombre as modelo', 'marcas.nombre as marca'])
                     ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
                     ->orderBy('marca')->get();
        $tipos = Tipo::all()->sortBy('nombre');
        $colores = Color::all()->sortBy('nombre');
        return view('vehiculos', ['var' => 'vehiculos_create', 'modelos' => $modelos, 'tipos' => $tipos, 'colores' => $colores]);
    }

    public function vehiculos_store(Request $request)
    {
        $this->validate($request, [
            'modelo_id' => 'required',
            'anho_fabricacion' => 'required',
            'tipo_id' => 'required',
            'color_id' => 'required',  
            'chasis' => 'required',
            'transmision' => 'required'
        ]);

        $vehiculo = new Vehiculo;
        $vehiculo->modelo_id = $request->modelo_id;
        $vehiculo->anho_fabricacion = $request->anho_fabricacion;
        $vehiculo->tipo_id = $request->tipo_id;
        $vehiculo->matricula = $request->matricula;
        $vehiculo->color_id = $request->color_id;
        $vehiculo->deposito_id = Null;
        $vehiculo->chasis = $request->chasis;
        $vehiculo->transmision = $request->transmision;
        $vehiculo->costo = Null;
        $vehiculo->moneda_id = Null;
        $vehiculo->estado = False;
        $vehiculo->save();

        $this->auditoria($vehiculo, 'I');
        return redirect(route('vehiculos_index'))->with('successMessage', 'Vehiculo añadido correctamente.');
    }

    public function vehiculos_show($id)
    {
        $vehiculo = Vehiculo::findOrFail($id);
        $modelo = Modelo::findOrFail($vehiculo->modelo_id);
        $marca = Marca::findOrFail($modelo->marca_id);
        $tipo = Tipo::findOrFail($vehiculo->tipo_id);
        $color = Color::findOrFail($vehiculo->color_id);
        $deposito = Deposito::findOrFail($vehiculo->deposito_id);
        $moneda = Moneda::findOrFail($vehiculo->moneda_id);
        return view('vehiculos', ['var' => 'vehiculos_show', 'vehiculo' => $vehiculo, 'marca' => $marca, 'modelo' => $modelo, 'tipo' => $tipo, 'color' => $color, 'deposito' => $deposito, 'moneda' => $moneda]);
    }

    public function vehiculos_edit($id)
    {
        $vehiculo = Vehiculo::findOrFail($id);
        $modelos = Modelo::select(['modelos.modelo_id as modelo_id', 'modelos.nombre as modelo', 'marcas.nombre as marca'])
                     ->leftJoin('marcas', 'marcas.marca_id', '=', 'modelos.marca_id')
                     ->orderBy('marca')->get();
        $tipos = Tipo::all()->sortBy('nombre');
        $colores = Color::all()->sortBy('nombre');
        return view('vehiculos', ['var' => 'vehiculos_edit', 'vehiculo' => $vehiculo, 'modelos' => $modelos, 'tipos' => $tipos, 'colores' => $colores]);
    }

    public function vehiculos_update(Request $request, $id)
    {
        $this->validate($request, [
            'modelo_id' => 'required',
            'anho_fabricacion' => 'required',
            'tipo_id' => 'required',
            'color_id' => 'required',  
            'chasis' => 'required',
            'transmision' => 'required'
        ]);

        $vehiculo = Vehiculo::findOrFail($id);
        $viejo = Vehiculo::findOrFail($id);
        $vehiculo->modelo_id = $request->modelo_id;
        $vehiculo->anho_fabricacion = $request->anho_fabricacion;
        $vehiculo->tipo_id = $request->tipo_id;
        $vehiculo->matricula = $request->matricula;
        $vehiculo->color_id = $request->color_id;
        $vehiculo->chasis = $request->chasis;
        $vehiculo->transmision = $request->transmision;
        $vehiculo->save();

        $this->auditoria($vehiculo, 'U', $viejo);
        return redirect(route('vehiculos_index'))->with('successMessage', 'Vehículo actualizado correctamente.');
    }

    public function vehiculos_destroy($id)
    {
        try { 
            $vehiculo = Vehiculo::findOrFail($id);
            $vehiculo->delete();
            $this->auditoria($vehiculo, 'D');
            return redirect(URL::previous())->with('successMessage', 'Vehículo borrado correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'El Vehículo no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }

    public function auditoria($objeto, $tipo_transaccion, $objeto_viejo = null)
    {
        if ($tipo_transaccion == 'I') {
            $atributos = $objeto->attributesToArray();
            
            foreach ($atributos as $clave => $atributo) {
                if ($atributo !== $objeto->getKey()) {
                    $auditoria = new Auditoria;
                    $auditoria->tipo_transaccion = 'I';
                    $auditoria->tabla = $objeto->getTable();
                    $auditoria->tabla_pk = $objeto->getKey();
                    $auditoria->campo = $clave;
                    $auditoria->valor_nuevo = $atributo;
                    $auditoria->fecha = now();
                    $auditoria->usuario_id = Auth::user()->id;
                    $auditoria->save();
                }
            }
        }
        
        if ($tipo_transaccion == 'D') {
            $atributos = $objeto->attributesToArray();
            
            foreach ($atributos as $clave => $atributo) {
                if ($atributo !== $objeto->getKey()) {
                    $auditoria = new Auditoria;
                    $auditoria->tipo_transaccion = 'D';
                    $auditoria->tabla = $objeto->getTable();
                    $auditoria->tabla_pk = $objeto->getKey();
                    $auditoria->campo = $clave;
                    $auditoria->valor_viejo = $atributo;
                    $auditoria->fecha = now();
                    $auditoria->usuario_id = Auth::user()->id;
                    $auditoria->save();
                }
            }
        }

        if ($tipo_transaccion == 'U') {
            $atributos = $objeto->attributesToArray();
            
            foreach ($atributos as $clave => $atributo) {
                if ($atributo !== $objeto->getKey()) {
                    if ($objeto_viejo[$clave] != $atributo) {
                        $auditoria = new Auditoria;
                        $auditoria->tipo_transaccion = 'U';
                        $auditoria->tabla = $objeto->getTable();
                        $auditoria->tabla_pk = $objeto->getKey();
                        $auditoria->campo = $clave;
                        $auditoria->valor_viejo = $objeto_viejo[$clave];
                        $auditoria->valor_nuevo = $atributo;
                        $auditoria->fecha = now();
                        $auditoria->usuario_id = Auth::user()->id;
                        $auditoria->save();
                    }
                }
            }
        }

    }

}