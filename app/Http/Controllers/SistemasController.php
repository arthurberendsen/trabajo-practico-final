<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use DateTime;

use App\Models\Auditoria;
use App\Models\Ciudad;
use App\Models\Pais;
use App\Models\Moneda;
use App\Models\Deposito;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class SistemasController extends Controller
{

    public function usuarios_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $usuarios = User::select('*');

        if (!(blank($buscar))) {
            $usuarios = $usuarios->orWhere('usuarios.nombre', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('usuarios.email', 'ILIKE', '%'. $buscar . '%');
        }

        $usuarios = $usuarios->orderBy('id')->paginate($items);
        return view('sistemas', ['var' => 'usuarios_index', 'usuarios' => $usuarios, 'items' => $items, 'buscar' => $buscar]);
    }

    public function usuarios_create()
    {
        return view('sistemas', ['var' => 'usuarios_create']);
    }

    public function usuarios_store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'email' => 'required',
            'password' => 'required',
            'repassword' => 'required'
        ]);

        $usuario = new User;
        $usuario->nombre = $request->nombre;
        $usuario->email = $request->email;
        $usuario->avatar = 'public/avatar/no_avatar.png';

        if (isset($request->password) && isset($request->repassword)) {
            if ($request->password === $request->repassword) {
                $usuario->password = Hash::make($request->password);
            } else {
                return redirect(route('usuarios_create'))->with(['errorMessage' => 'Los campos de Contraseña no coinciden. Favor verificar', 'nombre' => $request->nombre, 'email' => $request->email]);
            }
        } else {
            return redirect(route('usuarios_create'))->with(['errorMessage' => 'Uno de los campos de Contraseña no coincide. Favor volver a escribir', 'nombre' => $request->nombre, 'email' => $request->email]);
        }
        
        $usuario->estado = True;
        $usuario->save();

        $this->auditoria($usuario, 'I');
        return redirect(route('usuarios_index'))->with('successMessage', 'Usuario añadido correctamente.');
    }

    public function usuarios_show($id)
    {
        $usuario = User::findOrFail($id);
        return view('sistemas', ['var' => 'usuarios_show', 'usuario' => $usuario]);
    }

    public function usuarios_edit($id)
    {
        $usuario = User::findOrFail($id);
        return view('sistemas', ['var' => 'usuarios_edit', 'usuario' => $usuario]);
    }

    public function usuarios_update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'email' => 'required'
        ]);

        $usuario = User::findOrFail($id);
        $viejo = User::findOrFail($id);
        $usuario->nombre = $request->nombre;
        $usuario->email = $request->email;

        if (isset($request->password) && isset($request->repassword)) {
            if ($request->password === $request->repassword) {
                $usuario->password = Hash::make($request->password);
            } else {
                return redirect(route('usuarios_edit', $usuario->id))->with('errorMessage', 'Los campos de Contraseña no coinciden. Favor verificar');
            }
        } elseif (empty($request->password) xor empty($request->repassword)) {
            return redirect(route('usuarios_edit', $usuario->id))->with('errorMessage', 'Uno de los campos de Contraseña no coincide. Favor volver a escribir');
        }
        
        $usuario->estado = $request->boolean('estado');
        $usuario->save();

        $this->auditoria($usuario, 'U', $viejo);
        return redirect(route('usuarios_index'))->with('successMessage', 'Usuario actualizado correctamente.');
    }

    public function usuarios_destroy($id)
    {
        try { 
            $usuario = User::findOrFail($id);
            $usuario->delete();
            $this->auditoria($usuario, 'D');
            return redirect(URL::previous())->with('successMessage', 'Usuario borrado correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'El Usuario no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }



    public function monedas_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $monedas = Moneda::select('*');

        if (!(blank($buscar))) {
            $monedas = $monedas->orWhere('monedas.nombre', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('monedas.sigla', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('monedas.decimales', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('monedas.cotizacion', 'ILIKE', '%'. $buscar . '%');
        }

        $monedas = $monedas->orderBy('moneda_id')->paginate($items);

        return view('sistemas', ['var' => 'monedas_index', 'monedas' => $monedas, 'items' => $items, 'buscar' => $buscar]);
    }

    public function monedas_create()
    {
        return view('sistemas', ['var' => 'monedas_create']);
    }

    public function monedas_store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'sigla' => 'required',
            'decimales' => 'required',
            'cotizacion' => 'required'
        ]);

        $moneda = new Moneda;
        $moneda->nombre = $request->nombre;
        $moneda->sigla = $request->sigla;
        $moneda->decimales = $request->decimales;
        $moneda->cotizacion = $request->cotizacion;
        $moneda->estado = True;
        $moneda->save();

        $this->auditoria($moneda, 'I');
        return redirect(route('monedas_index'))->with('successMessage', 'Moneda añadida correctamente.');
    }

    public function monedas_show($id)
    {
        $moneda = Moneda::findOrFail($id);
        return view('sistemas', ['var' => 'monedas_show', 'moneda' => $moneda]);
    }

    public function monedas_edit($id)
    {
        $moneda = Moneda::findOrFail($id);
        return view('sistemas', ['var' => 'monedas_edit', 'moneda' => $moneda]);
    }

    public function monedas_update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'sigla' => 'required',
            'decimales' => 'required',
            'cotizacion' => 'required'
        ]);

        $moneda = Moneda::findOrFail($id);
        $viejo =  Moneda::findOrFail($id);
        $moneda->nombre = $request->nombre;
        $moneda->sigla = $request->sigla;
        $moneda->decimales = $request->decimales;
        $moneda->cotizacion = $request->cotizacion;
        $moneda->estado = $request->boolean('estado');
        $moneda->save();

        $this->auditoria($moneda, 'U', $viejo);
        return redirect(route('monedas_index'))->with('successMessage', 'Moneda actualizada correctamente.');
    }

    public function monedas_destroy($id)
    {
        try { 
            $moneda = Moneda::findOrFail($id);
            $moneda->delete();
            $this->auditoria($moneda, 'D');
            return redirect(URL::previous())->with('successMessage', 'Moneda borrada correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'La Moneda no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }




    public function depositos_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $depositos = Deposito::select('*');

        if (!(blank($buscar))) {
            $depositos = $depositos->orWhere('depositos.nombre', 'ILIKE', '%'. $buscar . '%');
        }

        $depositos = $depositos->orderBy('deposito_id')->paginate($items);


        return view('sistemas', ['var' => 'depositos_index', 'depositos' => $depositos, 'items' => $items, 'buscar' => $buscar]);
    }

    public function depositos_create()
    {
        return view('sistemas', ['var' => 'depositos_create']);
    }

    public function depositos_store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $deposito = new Deposito;
        $deposito->nombre = $request->nombre;
        $deposito->estado = True;
        $deposito->save();

        $this->auditoria($deposito, 'I');
        return redirect(route('depositos_index'))->with('successMessage', 'Depósito añadido correctamente.');
    }

    public function depositos_show($id)
    {
        $deposito = Deposito::findOrFail($id);
        return view('sistemas', ['var' => 'depositos_show', 'deposito' => $deposito]);
    }

    public function depositos_edit($id)
    {
        $deposito = Deposito::findOrFail($id);
        return view('sistemas', ['var' => 'depositos_edit', 'deposito' => $deposito]);
    }

    public function depositos_update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $deposito = Deposito::findOrFail($id);
        $viejo = Deposito::findOrFail($id);
        $deposito->nombre = $request->nombre;
        $deposito->estado = $request->boolean('estado');
        $deposito->save();

        $this->auditoria($deposito, 'U', $viejo);
        return redirect(route('depositos_index'))->with('successMessage', 'Depósito actualizado correctamente.');
    }

    public function depositos_destroy($id)
    {
        try { 
            $deposito = Deposito::findOrFail($id);
            $deposito->delete();
            $this->auditoria($deposito, 'D');
            return redirect(URL::previous())->with('successMessage', 'Depósito borrado correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'El Depósito no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }



    public function ciudades_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $ciudades = Ciudad::select(['ciudades.ciudad_id as ciudad_id', 'ciudades.nombre as nombre', 'paises.nombre as pais'])
                   ->leftJoin('paises', 'paises.pais_id', '=', 'ciudades.pais_id');

        if (!(blank($buscar))) {
            $ciudades = $ciudades->orWhere('ciudades.nombre', 'ILIKE', '%'. $buscar . '%')
                                ->orWhere('paises.nombre', 'ILIKE', '%'. $buscar . '%');
        }

        $ciudades = $ciudades->orderBy('ciudad_id')->paginate($items);

        return view('sistemas', ['var' => 'ciudades_index', 'ciudades' => $ciudades, 'items' => $items, 'buscar' => $buscar]);
    }

    public function ciudades_create()
    {
        $paises = Pais::all()->sortBy('nombre');
        return view('sistemas', ['var' => 'ciudades_create', 'paises' => $paises]);
    }

    public function ciudades_store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'pais_id' => 'required'
        ]);

        $ciudad = new Ciudad;
        $ciudad->nombre = $request->nombre;
        $ciudad->pais_id = $request->pais_id;
        $ciudad->save();

        $this->auditoria($ciudad, 'I');
        return redirect(route('ciudades_index'))->with('successMessage', 'Ciudad añadida correctamente.');
    }

    public function ciudades_show($id)
    {
        $ciudad = Ciudad::findOrFail($id);
        $pais = Pais::findOrFail($ciudad->pais_id);
        return view('sistemas', ['var' => 'ciudades_show', 'ciudad' => $ciudad, 'pais' => $pais]);
    }

    public function ciudades_edit($id)
    {
        $ciudad = Ciudad::findOrFail($id);
        $paises = Pais::all()->sortBy('nombre');
        return view('sistemas', ['var' => 'ciudades_edit', 'ciudad' => $ciudad, 'paises' => $paises]);
    }

    public function ciudades_update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'pais_id' => 'required'
        ]);

        $ciudad = Ciudad::findOrFail($id);
        $viejo = Ciudad::findOrFail($id);
        $ciudad->nombre = $request->nombre;
        $ciudad->pais_id = $request->pais_id;
        $ciudad->save();

        $this->auditoria($ciudad, 'U', $viejo);
        return redirect(route('ciudades_index'))->with('successMessage', 'Ciudad actualizada correctamente.');
    }

    public function ciudades_destroy($id)
    {
        try { 
            $ciudad = Ciudad::findOrFail($id);
            $ciudad->delete();
            $this->auditoria($ciudad, 'D');
            return redirect(URL::previous())->with('successMessage', 'Ciudad borrada correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'La Ciudad no se puede borrar. Esta siendo utilizada por algún registro dentro del sistema.');
        }
    }




    public function paises_index(Request $request)
    {
        $items = $request->items ?? 5;
        $buscar = $request->buscar;

        $paises = Pais::select('*');

        if (!(blank($buscar))) {
            $paises = $paises->orWhere('paises.nombre', 'ILIKE', '%'. $buscar . '%');
        }

        $paises = $paises->orderBy('pais_id')->paginate($items);
        
        return view('sistemas', ['var' => 'paises_index', 'paises' => $paises, 'items' => $items, 'buscar' => $buscar]);
    }

    public function paises_create()
    {
        return view('sistemas', ['var' => 'paises_create']);
    }

    public function paises_store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $pais = new Pais;
        $pais->nombre = $request->nombre;
        $pais->save();

        $this->auditoria($pais, 'I');
        return redirect(route('paises_index'))->with('successMessage', 'Pais añadido correctamente.');
    }

    public function paises_show($id)
    {
        $pais = Pais::findOrFail($id);
        return view('sistemas', ['var' => 'paises_show', 'pais' => $pais]);
    }

    public function paises_edit($id)
    {
        $pais = Pais::findOrFail($id);
        return view('sistemas', ['var' => 'paises_edit', 'pais' => $pais]);
    }

    public function paises_update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required'
        ]);

        $pais = Pais::findOrFail($id);
        $viejo = Pais::findOrFail($id);
        $pais->nombre = $request->nombre;
        $pais->save();

        $this->auditoria($pais, 'U', $viejo);
        return redirect(route('paises_index'))->with('successMessage', 'Pais actualizado correctamente.');
    }

    public function paises_destroy($id)
    {
        try { 
            $pais = Pais::findOrFail($id);
            $pais->delete();
            $this->auditoria($pais, 'D');
            return redirect(URL::previous())->with('successMessage', 'Pais borrado correctamente.');
        } catch (\Illuminate\Database\QueryException $ex) {
            return redirect(URL::previous())->with('errorMessage', 'El pais no se puede borrar. Esta siendo utilizado por algún registro dentro del sistema.');
        }
    }

    public function auditoria($objeto, $tipo_transaccion, $objeto_viejo = null)
    {
        if ($tipo_transaccion == 'I') {
            $atributos = $objeto->attributesToArray();
            
            foreach ($atributos as $clave => $atributo) {
                if ($atributo !== $objeto->getKey()) {
                    $auditoria = new Auditoria;
                    $auditoria->tipo_transaccion = 'I';
                    $auditoria->tabla = $objeto->getTable();
                    $auditoria->tabla_pk = $objeto->getKey();
                    $auditoria->campo = $clave;
                    $auditoria->valor_nuevo = $atributo;
                    $auditoria->fecha = now();
                    $auditoria->usuario_id = Auth::user()->id;
                    $auditoria->save();
                }
            }
        }
        
        if ($tipo_transaccion == 'D') {
            $atributos = $objeto->attributesToArray();
            
            foreach ($atributos as $clave => $atributo) {
                if ($atributo !== $objeto->getKey()) {
                    $auditoria = new Auditoria;
                    $auditoria->tipo_transaccion = 'D';
                    $auditoria->tabla = $objeto->getTable();
                    $auditoria->tabla_pk = $objeto->getKey();
                    $auditoria->campo = $clave;
                    $auditoria->valor_viejo = $atributo;
                    $auditoria->fecha = now();
                    $auditoria->usuario_id = Auth::user()->id;
                    $auditoria->save();
                }
            }
        }

        if ($tipo_transaccion == 'U') {
            $atributos = $objeto->attributesToArray();
            
            foreach ($atributos as $clave => $atributo) {
                if ($atributo !== $objeto->getKey()) {
                    if ($objeto_viejo[$clave] != $atributo) {
                        $auditoria = new Auditoria;
                        $auditoria->tipo_transaccion = 'U';
                        $auditoria->tabla = $objeto->getTable();
                        $auditoria->tabla_pk = $objeto->getKey();
                        $auditoria->campo = $clave;
                        $auditoria->valor_viejo = $objeto_viejo[$clave];
                        $auditoria->valor_nuevo = $atributo;
                        $auditoria->fecha = now();
                        $auditoria->usuario_id = Auth::user()->id;
                        $auditoria->save();
                    }
                }
            }
        }

    }

    function validarFecha($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }
}