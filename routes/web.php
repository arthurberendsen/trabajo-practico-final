<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\VentasController;
use App\Http\Controllers\ComprasController;
use App\Http\Controllers\VehiculosController;
use App\Http\Controllers\SistemasController;
use App\Http\Controllers\PerfilesController;
use App\Http\Controllers\ReportesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::view('home', 'home')->middleware('auth');

Route::middleware(['auth'])->group(function () {

	Route::get('/', [InicioController::class, 'inicio_index'])->name('inicio');
	Route::get('main_autocomplete', [InicioController::class, 'main_autocomplete'])->name('main_autocomplete');
	Route::post('main_autocomplete', [InicioController::class, 'main_autocomplete_filtrar'])->name('main_autocomplete_filtrar');
	//Route::get('autocomplete', [InicioController::class, 'autocomplete'])->name('autocomplete');

	Route::get('/ventas/clientes', [VentasController::class, 'clientes_index'])->name('clientes_index');
	Route::post('/ventas/clientes', [VentasController::class, 'clientes_index'])->name('clientes_index');
	Route::get('/ventas/clientes/crear', [VentasController::class, 'clientes_create'])->name('clientes_create');
	Route::post('/ventas/clientes/crear', [VentasController::class, 'clientes_store'])->name('clientes_store');
	Route::get('/ventas/clientes/editar/{id}', [VentasController::class, 'clientes_edit'])->name('clientes_edit');
	Route::get('/ventas/clientes/mostrar/{id}', [VentasController::class, 'clientes_show'])->name('clientes_show');
	Route::post('/ventas/clientes/actualizar/{id}', [VentasController::class, 'clientes_update'])->name('clientes_update');
	Route::delete('/ventas/clientes/borrar/{id}', [VentasController::class, 'clientes_destroy'])->name('clientes_destroy');

	Route::get('/ventas/facturasv', [VentasController::class, 'facturasv_index'])->name('facturasv_index');
	Route::post('/ventas/facturasv', [VentasController::class, 'facturasv_index'])->name('facturasv_index');
	Route::get('/ventas/facturasv/crear', [VentasController::class, 'facturasv_create'])->name('facturasv_create');
	Route::post('/ventas/facturasv/crear', [VentasController::class, 'facturasv_store'])->name('facturasv_store');
	//Route::get('/ventas/facturasv/editar/{id}', [VentasController::class, 'facturasv_edit'])->name('facturasv_edit');
	Route::get('/ventas/facturasv/mostrar/{id}', [VentasController::class, 'facturasv_show'])->name('facturasv_show');
	//Route::post('/ventas/facturasv/actualizar/{id}', [VentasController::class, 'facturasv_update'])->name('facturasv_update');
	Route::delete('/ventas/facturasv/borrar/{id}', [VentasController::class, 'facturasv_destroy'])->name('facturasv_destroy');
	Route::post('/ventas/facturasv/anular/{id}', [VentasController::class, 'facturasv_disable'])->name('facturasv_disable');

	Route::get('/ventas/pagares', [VentasController::class, 'pagares_index'])->name('pagares_index');
	Route::post('/ventas/pagares', [VentasController::class, 'pagares_index'])->name('pagares_index');
	//Route::get('/ventas/pagares/mostrar/{id}', [VentasController::class, 'pagares_show'])->name('pagares_show');
	Route::get('/ventas/pagares_pdf/{id}', [VentasController::class, 'pagares_pdf'])->name('pagares_pdf');

	Route::get('/ventas/cuotasv', [VentasController::class, 'cuotasv_index'])->name('cuotasv_index');
	Route::post('/ventas/cuotasv', [VentasController::class, 'cuotasv_index'])->name('cuotasv_index');
	//Route::get('/ventas/cuotasv/crear', [VentasController::class, 'cuotasv_create'])->name('cuotasv_create');
	//Route::post('/ventas/cuotasv/crear', [VentasController::class, 'cuotasv_store'])->name('cuotasv_store');
	Route::get('/ventas/cuotasv/editar/{f_pago_id}/{c_detalle_id}', [VentasController::class, 'cuotasv_edit'])->name('cuotasv_edit');
	Route::get('/ventas/cuotasv/mostrar/{f_pago_id}/{c_detalle_id}', [VentasController::class, 'cuotasv_show'])->name('cuotasv_show');
	Route::post('/ventas/cuotasv/actualizar/{f_pago_id}/{c_detalle_id}', [VentasController::class, 'cuotasv_update'])->name('cuotasv_update');
	//Route::delete('/ventas/cuotasv/borrar/{id}', [VentasController::class, 'cuotasv_destroy'])->name('cuotasv_destroy');

	Route::get('/ventas/recibosv', [VentasController::class, 'recibosv_index'])->name('recibosv_index');
	Route::post('/ventas/recibosv', [VentasController::class, 'recibosv_index'])->name('recibosv_index');
	Route::get('/ventas/recibosv/crear', [VentasController::class, 'recibosv_create'])->name('recibosv_create');
	Route::post('/ventas/recibosv/crear', [VentasController::class, 'recibosv_store'])->name('recibosv_store');
	//Route::get('/ventas/recibosv/editar/{id}', [VentasController::class, 'recibosv_edit'])->name('recibosv_edit');
	Route::get('/ventas/recibosv/mostrar/{id}', [VentasController::class, 'recibosv_show'])->name('recibosv_show');
	//Route::post('/ventas/recibosv/actualizar/{id}', [VentasController::class, 'recibosv_update'])->name('recibosv_update');
	Route::delete('/ventas/recibosv/borrar/{id}', [VentasController::class, 'recibosv_destroy'])->name('recibosv_destroy');
	Route::post('/ventas/recibosv/anular/{id}', [VentasController::class, 'recibosv_disable'])->name('recibosv_disable');
	Route::get('/ventas/recibosv_pdf/{id}', [VentasController::class, 'recibosv_pdf'])->name('recibosv_pdf');

	Route::get('clientes_autocomplete', [VentasController::class, 'clientes_autocomplete'])->name('clientes_autocomplete');
});

Route::middleware(['auth'])->group(function () {

	Route::get('/compras/proveedores', [ComprasController::class, 'proveedores_index'])->name('proveedores_index');
	Route::post('/compras/proveedores', [ComprasController::class, 'proveedores_index'])->name('proveedores_index');
	Route::get('/compras/proveedores/crear', [ComprasController::class, 'proveedores_create'])->name('proveedores_create');
	Route::post('/compras/proveedores/crear', [ComprasController::class, 'proveedores_store'])->name('proveedores_store');
	Route::get('/compras/proveedores/editar/{id}', [ComprasController::class, 'proveedores_edit'])->name('proveedores_edit');
	Route::get('/compras/proveedores/mostrar/{id}', [ComprasController::class, 'proveedores_show'])->name('proveedores_show');
	Route::post('/compras/proveedores/actualizar/{id}', [ComprasController::class, 'proveedores_update'])->name('proveedores_update');
	Route::delete('/compras/proveedores/borrar/{id}', [ComprasController::class, 'proveedores_destroy'])->name('proveedores_destroy');

	Route::get('/compras/facturasc', [ComprasController::class, 'facturasc_index'])->name('facturasc_index');
	Route::post('/compras/facturasc', [ComprasController::class, 'facturasc_index'])->name('facturasc_index');
	Route::get('/compras/facturasc/crear', [ComprasController::class, 'facturasc_create'])->name('facturasc_create');
	Route::post('/compras/facturasc/crear', [ComprasController::class, 'facturasc_store'])->name('facturasc_store');
	//Route::get('/compras/facturasc/editar/{id}', [ComprasController::class, 'facturasc_edit'])->name('facturasc_edit');
	Route::get('/compras/facturasc/mostrar/{id}', [ComprasController::class, 'facturasc_show'])->name('facturasc_show');
	//Route::post('/compras/facturasc/actualizar/{id}', [ComprasController::class, 'facturasc_update'])->name('facturasc_update');
	Route::delete('/compras/facturasc/borrar/{id}', [ComprasController::class, 'facturasc_destroy'])->name('facturasc_destroy');
	Route::post('/compras/facturasc/anular/{id}', [ComprasController::class, 'facturasc_disable'])->name('facturasc_disable');

	Route::get('/compras/cuotasc', [ComprasController::class, 'cuotasc_index'])->name('cuotasc_index');
	Route::post('/compras/cuotasc', [ComprasController::class, 'cuotasc_index'])->name('cuotasc_index');
	//Route::get('/compras/cuotasc/crear', [ComprasController::class, 'cuotasc_create'])->name('cuotasc_create');
	//Route::post('/compras/cuotasc/crear', [ComprasController::class, 'cuotasc_store'])->name('cuotasc_store');
	Route::get('/compras/cuotasc/editar/{f_pago_id}/{c_detalle_id}', [ComprasController::class, 'cuotasc_edit'])->name('cuotasc_edit');
	Route::get('/compras/cuotasc/mostrar/{f_pago_id}/{c_detalle_id}', [ComprasController::class, 'cuotasc_show'])->name('cuotasc_show');
	Route::post('/compras/cuotasc/actualizar/{f_pago_id}/{c_detalle_id}', [ComprasController::class, 'cuotasc_update'])->name('cuotasc_update');
	//Route::delete('/compras/cuotasc/borrar/{id}', [ComprasController::class, 'cuotasc_destroy'])->name('cuotasc_destroy');

	Route::get('/compras/recibosc', [ComprasController::class, 'recibosc_index'])->name('recibosc_index');
	Route::post('/compras/recibosc', [ComprasController::class, 'recibosc_index'])->name('recibosc_index');
	Route::get('/compras/recibosc/crear', [ComprasController::class, 'recibosc_create'])->name('recibosc_create');
	Route::post('/compras/recibosc/crear', [ComprasController::class, 'recibosc_store'])->name('recibosc_store');
	//Route::get('/compras/recibosc/editar/{id}', [ComprasController::class, 'recibosc_edit'])->name('recibosc_edit');
	Route::get('/compras/recibosc/mostrar/{id}', [ComprasController::class, 'recibosc_show'])->name('recibosc_show');
	//Route::post('/compras/recibosc/actualizar/{id}', [ComprasController::class, 'recibosc_update'])->name('recibosc_update');
	Route::delete('/compras/recibosc/borrar/{id}', [ComprasController::class, 'recibosc_destroy'])->name('recibosc_destroy');
	Route::post('/compras/recibosc/anular/{id}', [ComprasController::class, 'recibosc_disable'])->name('recibosc_disable');
	
});

Route::middleware(['auth'])->group(function () {

	Route::get('/vehiculos/colores', [VehiculosController::class, 'colores_index'])->name('colores_index');
	Route::post('/vehiculos/colores', [VehiculosController::class, 'colores_index'])->name('colores_index');
	Route::get('/vehiculos/colores/crear', [VehiculosController::class, 'colores_create'])->name('colores_create');
	Route::post('/vehiculos/colores/crear', [VehiculosController::class, 'colores_store'])->name('colores_store');
	Route::get('/vehiculos/colores/editar/{id}', [VehiculosController::class, 'colores_edit'])->name('colores_edit');
	Route::get('/vehiculos/colores/mostrar/{id}', [VehiculosController::class, 'colores_show'])->name('colores_show');
	Route::post('/vehiculos/colores/actualizar/{id}', [VehiculosController::class, 'colores_update'])->name('colores_update');
	Route::delete('/vehiculos/colores/borrar/{id}', [VehiculosController::class, 'colores_destroy'])->name('colores_destroy');

	Route::get('/vehiculos/tipos', [VehiculosController::class, 'tipos_index'])->name('tipos_index');
	Route::post('/vehiculos/tipos', [VehiculosController::class, 'tipos_index'])->name('tipos_index');
	Route::get('/vehiculos/tipos/crear', [VehiculosController::class, 'tipos_create'])->name('tipos_create');
	Route::post('/vehiculos/tipos/crear', [VehiculosController::class, 'tipos_store'])->name('tipos_store');
	Route::get('/vehiculos/tipos/editar/{id}', [VehiculosController::class, 'tipos_edit'])->name('tipos_edit');
	Route::get('/vehiculos/tipos/mostrar/{id}', [VehiculosController::class, 'tipos_show'])->name('tipos_show');
	Route::post('/vehiculos/tipos/actualizar/{id}', [VehiculosController::class, 'tipos_update'])->name('tipos_update');
	Route::delete('/vehiculos/tipos/borrar/{id}', [VehiculosController::class, 'tipos_destroy'])->name('tipos_destroy');

	Route::get('/vehiculos/modelos', [VehiculosController::class, 'modelos_index'])->name('modelos_index');
	Route::post('/vehiculos/modelos', [VehiculosController::class, 'modelos_index'])->name('modelos_index');
	Route::get('/vehiculos/modelos/crear', [VehiculosController::class, 'modelos_create'])->name('modelos_create');
	Route::post('/vehiculos/modelos/crear', [VehiculosController::class, 'modelos_store'])->name('modelos_store');
	Route::get('/vehiculos/modelos/editar/{id}', [VehiculosController::class, 'modelos_edit'])->name('modelos_edit');
	Route::get('/vehiculos/modelos/mostrar/{id}', [VehiculosController::class, 'modelos_show'])->name('modelos_show');
	Route::post('/vehiculos/modelos/actualizar/{id}', [VehiculosController::class, 'modelos_update'])->name('modelos_update');
	Route::delete('/vehiculos/modelos/borrar/{id}', [VehiculosController::class, 'modelos_destroy'])->name('modelos_destroy');

	Route::get('/vehiculos/marcas', [VehiculosController::class, 'marcas_index'])->name('marcas_index');
	Route::post('/vehiculos/marcas', [VehiculosController::class, 'marcas_index'])->name('marcas_index');
	Route::get('/vehiculos/marcas/crear', [VehiculosController::class, 'marcas_create'])->name('marcas_create');
	Route::post('/vehiculos/marcas/crear', [VehiculosController::class, 'marcas_store'])->name('marcas_store');
	Route::get('/vehiculos/marcas/editar/{id}', [VehiculosController::class, 'marcas_edit'])->name('marcas_edit');
	Route::get('/vehiculos/marcas/mostrar/{id}', [VehiculosController::class, 'marcas_show'])->name('marcas_show');
	Route::post('/vehiculos/marcas/actualizar/{id}', [VehiculosController::class, 'marcas_update'])->name('marcas_update');
	Route::delete('/vehiculos/marcas/borrar/{id}', [VehiculosController::class, 'marcas_destroy'])->name('marcas_destroy');

	Route::get('/vehiculos/vehiculos', [VehiculosController::class, 'vehiculos_index'])->name('vehiculos_index');
	Route::post('/vehiculos/vehiculos', [VehiculosController::class, 'vehiculos_index'])->name('vehiculos_index');
	Route::get('/vehiculos/vehiculos/crear', [VehiculosController::class, 'vehiculos_create'])->name('vehiculos_create');
	Route::post('/vehiculos/vehiculos/crear', [VehiculosController::class, 'vehiculos_store'])->name('vehiculos_store');
	Route::get('/vehiculos/vehiculos/editar/{id}', [VehiculosController::class, 'vehiculos_edit'])->name('vehiculos_edit');
	Route::get('/vehiculos/vehiculos/mostrar/{id}', [VehiculosController::class, 'vehiculos_show'])->name('vehiculos_show');
	Route::post('/vehiculos/vehiculos/actualizar/{id}', [VehiculosController::class, 'vehiculos_update'])->name('vehiculos_update');
	Route::delete('/vehiculos/vehiculos/borrar/{id}', [VehiculosController::class, 'vehiculos_destroy'])->name('vehiculos_destroy');
});

Route::middleware(['auth'])->group(function () {

	Route::get('/reportes/vehiculos', [ReportesController::class, 'vehiculos_index'])->name('r_vehiculos_index');
	Route::get('/reportes/r_vehiculos_pdf', [ReportesController::class, 'r_vehiculos_pdf'])->name('r_vehiculos_pdf');
	Route::get('/reportes/r_vehiculos_xls', [ReportesController::class, 'r_vehiculos_xls'])->name('r_vehiculos_xls');

	Route::get('/reportes/ventas', [ReportesController::class, 'ventas_index'])->name('r_ventas_index');
	Route::get('/reportes/r_ventas_pdf', [ReportesController::class, 'r_ventas_pdf'])->name('r_ventas_pdf');
	Route::get('/reportes/r_ventas_xls', [ReportesController::class, 'r_ventas_xls'])->name('r_ventas_xls');

	Route::get('/reportes/compras', [ReportesController::class, 'compras_index'])->name('r_compras_index');
	Route::get('/reportes/r_compras_pdf', [ReportesController::class, 'r_compras_pdf'])->name('r_compras_pdf');
	Route::get('/reportes/r_compras_xls', [ReportesController::class, 'r_compras_xls'])->name('r_compras_xls');

	Route::get('/reportes/auditorias', [ReportesController::class, 'auditorias_index'])->name('r_auditorias_index');
	Route::get('/reportes/r_auditorias_pdf', [ReportesController::class, 'r_auditorias_pdf'])->name('r_auditorias_pdf');
	Route::get('/reportes/r_auditorias_xls', [ReportesController::class, 'r_auditorias_xls'])->name('r_auditorias_xls');
	
});

Route::middleware(['auth'])->group(function () {

	Route::get('/sistemas/usuarios', [SistemasController::class, 'usuarios_index'])->name('usuarios_index');
	Route::post('/sistemas/usuarios', [SistemasController::class, 'usuarios_index'])->name('usuarios_index');
	Route::get('/sistemas/usuarios/crear', [SistemasController::class, 'usuarios_create'])->name('usuarios_create');
	Route::post('/sistemas/usuarios/crear', [SistemasController::class, 'usuarios_store'])->name('usuarios_store');
	Route::get('/sistemas/usuarios/editar/{id}', [SistemasController::class, 'usuarios_edit'])->name('usuarios_edit');
	Route::get('/sistemas/usuarios/mostrar/{id}', [SistemasController::class, 'usuarios_show'])->name('usuarios_show');
	Route::post('/sistemas/usuarios/actualizar/{id}', [SistemasController::class, 'usuarios_update'])->name('usuarios_update');
	Route::delete('/sistemas/usuarios/borrar/{id}', [SistemasController::class, 'usuarios_destroy'])->name('usuarios_destroy');

	Route::get('/sistemas/monedas', [SistemasController::class, 'monedas_index'])->name('monedas_index');
	Route::post('/sistemas/monedas', [SistemasController::class, 'monedas_index'])->name('monedas_index');
	Route::get('/sistemas/monedas/crear', [SistemasController::class, 'monedas_create'])->name('monedas_create');
	Route::post('/sistemas/monedas/crear', [SistemasController::class, 'monedas_store'])->name('monedas_store');
	Route::get('/sistemas/monedas/editar/{id}', [SistemasController::class, 'monedas_edit'])->name('monedas_edit');
	Route::get('/sistemas/monedas/mostrar/{id}', [SistemasController::class, 'monedas_show'])->name('monedas_show');
	Route::post('/sistemas/monedas/actualizar/{id}', [SistemasController::class, 'monedas_update'])->name('monedas_update');
	Route::delete('/sistemas/monedas/borrar/{id}', [SistemasController::class, 'monedas_destroy'])->name('monedas_destroy');

	Route::get('/sistemas/depositos', [SistemasController::class, 'depositos_index'])->name('depositos_index');
	Route::post('/sistemas/depositos', [SistemasController::class, 'depositos_index'])->name('depositos_index');
	Route::get('/sistemas/depositos/crear', [SistemasController::class, 'depositos_create'])->name('depositos_create');
	Route::post('/sistemas/depositos/crear', [SistemasController::class, 'depositos_store'])->name('depositos_store');
	Route::get('/sistemas/depositos/editar/{id}', [SistemasController::class, 'depositos_edit'])->name('depositos_edit');
	Route::get('/sistemas/depositos/mostrar/{id}', [SistemasController::class, 'depositos_show'])->name('depositos_show');
	Route::post('/sistemas/depositos/actualizar/{id}', [SistemasController::class, 'depositos_update'])->name('depositos_update');
	Route::delete('/sistemas/depositos/borrar/{id}', [SistemasController::class, 'depositos_destroy'])->name('depositos_destroy');

	Route::get('/sistemas/ciudades', [SistemasController::class, 'ciudades_index'])->name('ciudades_index');
	Route::post('/sistemas/ciudades', [SistemasController::class, 'ciudades_index'])->name('ciudades_index');
	Route::get('/sistemas/ciudades/crear', [SistemasController::class, 'ciudades_create'])->name('ciudades_create');
	Route::post('/sistemas/ciudades/crear', [SistemasController::class, 'ciudades_store'])->name('ciudades_store');
	Route::get('/sistemas/ciudades/editar/{id}', [SistemasController::class, 'ciudades_edit'])->name('ciudades_edit');
	Route::get('/sistemas/ciudades/mostrar/{id}', [SistemasController::class, 'ciudades_show'])->name('ciudades_show');
	Route::post('/sistemas/ciudades/actualizar/{id}', [SistemasController::class, 'ciudades_update'])->name('ciudades_update');
	Route::delete('/sistemas/ciudades/borrar/{id}', [SistemasController::class, 'ciudades_destroy'])->name('ciudades_destroy');

	Route::get('/sistemas/paises', [SistemasController::class, 'paises_index'])->name('paises_index');
	Route::post('/sistemas/paises', [SistemasController::class, 'paises_index'])->name('paises_index');
	Route::get('/sistemas/paises/crear', [SistemasController::class, 'paises_create'])->name('paises_create');
	Route::post('/sistemas/paises/crear', [SistemasController::class, 'paises_store'])->name('paises_store');
	Route::get('/sistemas/paises/editar/{id}', [SistemasController::class, 'paises_edit'])->name('paises_edit');
	Route::get('/sistemas/paises/mostrar/{id}', [SistemasController::class, 'paises_show'])->name('paises_show');
	Route::post('/sistemas/paises/actualizar/{id}', [SistemasController::class, 'paises_update'])->name('paises_update');
	Route::delete('/sistemas/paises/borrar/{id}', [SistemasController::class, 'paises_destroy'])->name('paises_destroy');
});

Route::middleware(['auth'])->group(function () {
	
	Route::get('/perfiles/perfil/{id}', [PerfilesController::class, 'perfil_index'])->name('perfil_index');
	Route::post('/perfiles/perfil/actualizar/{id}', [PerfilesController::class, 'perfil_update'])->name('perfil_update');
	
 	Route::post('/perfiles/perfil/actualizar_avatar/{id}', [PerfilesController::class, 'avatar_update'])->name('avatar_update');
 	Route::delete('/perfiles/perfil/borrar_avatar/{id}', [PerfilesController::class, 'avatar_destroy'])->name('avatar_destroy');
});