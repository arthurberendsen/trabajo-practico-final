<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            'nombre' => 'Arturo Berendsen',
            'email' => 'arturo@gmail.com',
            'password' => Hash::make('arturo123'),
            'avatar' => 'public/avatar/no_avatar.png',
            'estado' => True,
        ]);

        DB::table('usuarios')->insert([
            'nombre' => 'Joaquin Biedermann',
            'email' => 'joaquin@gmail.com',
            'password' => Hash::make('joaquin123'),
            'avatar' => 'public/avatar/no_avatar.png',
            'estado' => True,
        ]);

        DB::table('usuarios')->insert([
            'nombre' => 'Gonzalo Propp',
            'email' => 'gonzalo@gmail.com',
            'password' => Hash::make('gonzalo123'),
            'avatar' => 'public/avatar/no_avatar.png',
            'estado' => True,
        ]);

        DB::table('usuarios')->insert([
            'nombre' => 'Mauricio Merin',
            'email' => 'mauricio@gmail.com',
            'password' => Hash::make('mauricio123'),
            'avatar' => 'public/avatar/no_avatar.png',
            'estado' => True,
        ]);
    }
}
