@extends ('layouts.main')

@section ('contenido')

    <div class="d-sm-flex justify-content-between align-items-center mb-4" style="box-shadow: 0px 0px;">
        <h3 class="text-dark mb-0">Inicio</h3>
        <!-- <a class="btn btn-primary btn-sm d-none d-sm-inline-block" role="button" href="#"><i class="fas fa-download fa-sm text-white-50"></i>&nbsp;Generar Reportes</a> -->
    </div>
    <div class="row">
        <div class="col-md-6 col-xl-3 mb-4">
            <div class="card shadow border-left-primary py-2">
                <div class="card-body">
                    <div class="row align-items-center no-gutters">
                        <div class="col mr-2">
                            <div class="text-uppercase text-primary font-weight-bold text-xs mb-1"><span>Ventas (ult. mes)</span></div>
                            <div class="text-dark font-weight-bold h5 mb-0"><span>{{ $ventas_mensuales }}</span></div>
                        </div>
                        <div class="col-auto"><i class="fas fa-calendar fa-2x text-gray-300"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3 mb-4">
            <div class="card shadow border-left-success py-2">
                <div class="card-body">
                    <div class="row align-items-center no-gutters">
                        <div class="col mr-2">
                            <div class="text-uppercase text-success font-weight-bold text-xs mb-1"><span>Ventas (ult. año)</span></div>
                            <div class="text-dark font-weight-bold h5 mb-0"><span>{{ $ventas_anuales }}</span></div>
                        </div>
                        <div class="col-auto"><i class="fas fa-dollar-sign fa-2x text-gray-300"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3 mb-4">
            <div class="card shadow border-left-info py-2">
                <div class="card-body">
                    <div class="row align-items-center no-gutters">
                        <div class="col mr-2">
                            <div class="text-uppercase text-info font-weight-bold text-xs mb-1"><span>Porcentaje Vendido</span></div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="text-dark font-weight-bold h5 mb-0 mr-3"><span>{{ $porcentaje_vendido }}%</span></div>
                                </div>
                                <div class="col">
                                    <div class="progress progress-sm">
                                        <div class="progress-bar bg-info" aria-valuenow="{{ $porcentaje_vendido }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $porcentaje_vendido }}%;"><span class="sr-only">{{ $porcentaje_vendido }}%</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto"><i class="fas fa-clipboard-list fa-2x text-gray-300"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-xl-3 mb-4">
            <div class="card shadow border-left-warning py-2">
                <div class="card-body">
                    <div class="row align-items-center no-gutters">
                        <div class="col mr-2">
                            <div class="text-uppercase text-warning font-weight-bold text-xs mb-1"><span>Clientes Activos</span></div>
                            <div class="text-dark font-weight-bold h5 mb-0"><span>{{ $clientes_activos }}</span></div>
                        </div>
                        <div class="col-auto"><i class="fas fa-users fa-2x text-gray-300"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-7 col-xl-8">
            <div class="card shadow mb-4">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h6 class="text-primary font-weight-bold m-0">Resumen de Ventas</h6>
                    <!-- <div class="dropdown no-arrow"><button class="btn btn-link btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button"><i class="fas fa-ellipsis-v text-gray-400"></i></button>
                        <div class="dropdown-menu shadow dropdown-menu-right animated--fade-in">
                            <p class="text-center dropdown-header">dropdown header:</p><a class="dropdown-item" href="#">&nbsp;Action</a><a class="dropdown-item" href="#">&nbsp;Another action</a>
                            <div class="dropdown-divider"></div><a class="dropdown-item" href="#">&nbsp;Something else here</a></div>
                    </div> -->
                </div>
                <div class="card-body">
                    <div class="chart-area"><canvas data-bs-chart="{&quot;type&quot;:&quot;line&quot;,
                        &quot;data&quot;:{&quot;labels&quot;:
                        [&quot;{{ $resumen_eje_x[0] }}&quot;,&quot;{{ $resumen_eje_x[1] }}&quot;,&quot;{{ $resumen_eje_x[2] }}&quot;,&quot;{{ $resumen_eje_x[3] }}&quot;,&quot;{{ $resumen_eje_x[4] }}&quot;,&quot;{{ $resumen_eje_x[5] }}&quot;,&quot;{{ $resumen_eje_x[6] }}&quot;,&quot;{{ $resumen_eje_x[7] }}&quot;],
                        &quot;datasets&quot;:[{&quot;label&quot;:&quot;Ventas&quot;,&quot;fill&quot;:true,&quot;data&quot;:
                        [&quot;{{ $resumen_puntos[0] }}&quot;,&quot;{{ $resumen_puntos[1] }}&quot;,&quot;{{ $resumen_puntos[2] }}&quot;,&quot;{{ $resumen_puntos[3] }}&quot;,&quot;{{ $resumen_puntos[4] }}&quot;,&quot;{{ $resumen_puntos[5] }}&quot;,&quot;{{ $resumen_puntos[6] }}&quot;,&quot;{{ $resumen_puntos[7] }}&quot;],
                        &quot;backgroundColor&quot;:&quot;#f2f7fb&quot;,&quot;borderColor&quot;:&quot;rgba(78, 115, 223, 1)&quot;}]},&quot;options&quot;:{&quot;maintainAspectRatio&quot;:false,&quot;legend&quot;:{&quot;display&quot;:false},&quot;title&quot;:{},&quot;scales&quot;:{&quot;xAxes&quot;:[{&quot;gridLines&quot;:{&quot;color&quot;:&quot;rgb(234, 236, 244)&quot;,&quot;zeroLineColor&quot;:&quot;rgb(234, 236, 244)&quot;,&quot;drawBorder&quot;:false,&quot;drawTicks&quot;:false,&quot;borderDash&quot;:[&quot;2&quot;],&quot;zeroLineBorderDash&quot;:[&quot;2&quot;],&quot;drawOnChartArea&quot;:false},&quot;ticks&quot;:{&quot;fontColor&quot;:&quot;#858796&quot;,&quot;padding&quot;:20}}],&quot;yAxes&quot;:[{&quot;gridLines&quot;:{&quot;color&quot;:&quot;rgb(234, 236, 244)&quot;,&quot;zeroLineColor&quot;:&quot;rgb(234, 236, 244)&quot;,&quot;drawBorder&quot;:false,&quot;drawTicks&quot;:false,&quot;borderDash&quot;:[&quot;2&quot;],&quot;zeroLineBorderDash&quot;:[&quot;2&quot;]},&quot;ticks&quot;:{&quot;fontColor&quot;:&quot;#858796&quot;,&quot;padding&quot;:20}}]}}}"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-xl-4">
            <div class="card shadow mb-4">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h6 class="text-primary font-weight-bold m-0">Fuentes de Ingreso</h6>
                    <!-- <div class="dropdown no-arrow"><button class="btn btn-link btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false" type="button"><i class="fas fa-ellipsis-v text-gray-400"></i></button>
                        <div class="dropdown-menu shadow dropdown-menu-right animated--fade-in">
                            <p class="text-center dropdown-header">dropdown header:</p><a class="dropdown-item" href="#">&nbsp;Action</a><a class="dropdown-item" href="#">&nbsp;Another action</a>
                            <div class="dropdown-divider"></div><a class="dropdown-item" href="#">&nbsp;Something else here</a></div>
                    </div> -->
                </div>
                <div class="card-body">
                    <div class="chart-area">
                        <canvas data-bs-chart="{
                        &quot;type&quot;:&quot;doughnut&quot;,
                        &quot;data&quot;:{&quot;labels&quot;:[&quot;Cuotas&quot;,&quot;Cheques&quot;,&quot;Efectivo&quot;,&quot;Depósitos Bancarios&quot;],
                        &quot;datasets&quot;:[{&quot;label&quot;:&quot;&quot;,
                        &quot;backgroundColor&quot;:[&quot;#4e73df&quot;,&quot;#1cc88a&quot;,&quot;#36b9cc&quot;,&quot;#fc758e&quot;],
                        &quot;borderColor&quot;:[&quot;#ffffff&quot;,&quot;#ffffff&quot;,&quot;#ffffff&quot;,&quot;#ffffff&quot;],
                        &quot;data&quot;:[&quot;{{ $facturas_ventas_cuotas }}&quot;,&quot;{{ $facturas_ventas_cheques }}&quot;,&quot;{{ $facturas_ventas_efectivo }}&quot;,&quot;{{ $facturas_ventas_depositos }}&quot;]
                    }]},&quot;options&quot;:{&quot;maintainAspectRatio&quot;:false,&quot;legend&quot;:{&quot;display&quot;:false},&quot;title&quot;:{}}}">
                        </canvas>
                    </div>
                    <div class="text-center small mt-4">
                        <span class="mr-2"><i class="fas fa-circle" style="color:#4e73df;"></i>&nbsp;Cuotas</span>
                        <span class="mr-2"><i class="fas fa-circle" style="color:#1cc88a;"></i>&nbsp;Cheques</span>
                        <span class="mr-2"><i class="fas fa-circle" style="color:#36b9cc;"></i>&nbsp;Efectivo</span>
                        <span class="mr-2"><i class="fas fa-circle" style="color:#fc758e;"></i>&nbsp;Depósitos</span>
                    </div>
                </div>
        </div>
    </div>
</div>

@endsection