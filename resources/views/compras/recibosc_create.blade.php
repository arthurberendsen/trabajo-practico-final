@section ('contenido')

<form action="{{ route('recibosc_store') }}" id="crear_recibo" method="POST">
{{ csrf_field() }}

<!-- INICIO MODALES -->

<div role="dialog" tabindex="-1" class="modal fade" id="modal_descartar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Descartar Cambios</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <p>¿Desea realmente descartar los cambios realizados?</p>
            </div>
            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">No</button><a class="btn btn-primary" role="button" href="{{ route('recibosc_index') }}">Si</a></div>
        </div>
    </div>
</div>


<div role="dialog" tabindex="-1" class="modal fade" id="modal_formapago">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Definir Forma de Pago</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"><label for="forma_pago_nombre"><strong>Forma de Pago</strong></label>
                                <select class="form-control" id="forma_pago_nombre" name="forma_pago_nombre">
                                    <option value="efectivo" selected>Efectivo</option>
                                    <option value="cheques">Cheques</option>
                                    <option value="depositos_bancarios">Depósito Bancario</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-md-12">
                                <div class="form-group"><label for="monto"><strong>Monto</strong></label><input class="form-control" type="text" placeholder="" id="monto" name="monto" readonly>
                                </div>
                            </div>
                        </div>  
                    <div class="formulario_forma_pago">
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <div class="form-group"><label for="monto"><strong>Monto</strong></label><input class="form-control" type="text" placeholder="" id="monto" name="Monto" readonly>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-warning" type="button" data-dismiss="modal">Volver atrás</button>
                <a class="btn btn-primary" role="button" href="{{ route('recibosc_index') }}" onclick="event.preventDefault(); document.getElementById('crear_recibo').submit();">Guardar Cambios</a>
            </div>
        </div>
    </div>
</div>

<!-- FIN MODALES -->

<div class="container-fluid">
    
    <!-- <div class="alert alert-danger" role="alert">
        <p id=error_container></p>
    </div> -->

<!-- 	@if($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger" role="alert">
		  		ERROR: {{ $error }}
			</div>
		@endforeach
	@endif -->

    <h3 class="text-dark mb-4">Recibos</h3>
    <div class="row mb-3">
        <div class="col-lg-10 col-xl-10">
            <div class="row">
                <div class="col offset-xl-0">

                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Nuevo Recibo</p>
                        </div>
                        <div class="card-body">
                                <div class="form-row">
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="numero"><strong>Número</strong></label>
                                            <input class="form-control @error('numero') is-invalid @enderror " type="text" placeholder="" id="numero" name="numero" value="{{ old('numero') }}">
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Provea un Nro. de Recibo</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="form-group"><label for="proveedor_id"><strong>Proveedor</strong></label>
                                            <select class="form-control @error('proveedor_id') is-invalid @enderror" id="proveedor_id" name="proveedor_id">
                                                <option value="" @if (old('proveedor_id') == '') selected @endif>Elegir un Proveedor...</option>
                                                @foreach($proveedores as $proveedor)
                                                    <option value="{{ $proveedor->proveedor_id }}" @if (old('proveedor_id') == $proveedor->proveedor_id) selected @endif>{{ $proveedor->razon_social }}</option>
                                                @endforeach
                                            </select>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Seleccione el Proveedor</strong>
                                            </span>
                                        </div>
                                        
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="form-group"><label for="moneda_id"><strong>Moneda</strong>
                                        </label><select class="form-control @error('moneda_id') is-invalid @enderror" id="moneda_id" name="moneda_id">
                                                <option value="" @if (old('moneda_id') == '') selected @endif>Elegir una moneda...</option>
                                            @foreach($monedas as $moneda)
                                                <option value="{{ $moneda->moneda_id }}" @if (old('moneda_id') == $moneda->moneda_id) selected @endif>{{ $moneda->nombre }}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback" role="alert">
                                                <strong>Seleccione una Moneda</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="fecha"><strong>Fecha</strong></label>
                                            <input type="text" class="form-control datepicker @error('fecha') is-invalid @enderror" name="fecha" id="fecha" value="@if (old('fecha')) {{ old('fecha') }} @else {{ date('d/m/Y') }} @endif">
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Seleccione una Fecha</strong>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>


                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Cuota a Cancelar</p>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-xl-6">
                                    <div class="form-group"><label for="cuota_id"><strong>Cuotas Pendientes</strong></label>
                                        <select class="form-control @error('cuota_id') is-invalid @enderror" id="cuota_id" name="cuota_id">
                                            <option value="" selected>Elegir una Cuota...</option>
                                        <!-- foreach($vehiculos as $vehiculo)
                                            <option value=""></option>
                                        endforeach -->
                                        </select>
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Seleccione la Cuota a cancelar</strong>
                                        </span>
                                    </div>
                                    
                                </div>
                                <div class="col-xl-3">
                                    <div class="form-group"><label for="monto_cuota"><strong>Monto Cuota</strong></label><input class="form-control" type="text" placeholder="" name="monto_cuota" value="" id="monto_cuota" readonly>
                                    </div>
                                </div>
                                <div class="col-xl-1">
                                    <div class="form-group"><label for="sigla"><strong>Sigla</strong></label><input class="form-control" type="text" placeholder="" name="sigla" id="sigla" readonly>
                                    </div>
                                </div>
                                <div class="col-xl-2">
                                    <div class="form-group"><label for="vencimiento"><strong>Vencimiento</strong></label><input class="form-control" type="text" placeholder="" name="vencimiento" id="vencimiento" readonly>
                                    </div>
                                </div>
                            </div>
                            
                            <input type="hidden" id="forma_pago_id" name="forma_pago_id" value="">
                            <input type="hidden" id="cuota_detalle_id" name="cuota_detalle_id" value="">

                            <div class="form-group">
                                <button class="btn btn-primary" type="button" id="seleccionarFormaDePago">Seleccionar Forma de Pago</button>
                                <a class="btn btn-danger ml-3" href="#" data-toggle="modal" data-target="#modal_descartar">Descartar</a>
                                <button class="btn btn-secondary ml-3" type="button" id="limpiar" name="limpiar">Limpiar campos</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>

<script type="text/javascript">

    //$('#sigla').val("");
    //$('#vencimiento').val("");

    /* Codigo para inicializar el DatePicker */
    $('.datepicker').datepicker({
        clearBtn: true,
        language: "es",
        autoclose: true,
        todayHighlight: true
    });
    
    var monto_cuota_decimales = 0;

    // Codigo para inicializar el AutoNumeric, y formatear el monto_cuota automaticamente
    var monto_cuota = new AutoNumeric('#monto_cuota', {
        //allowDecimalPadding: false,
        decimalCharacter: ",",
        decimalCharacterAlternative: ",",
        digitGroupSeparator: ".",
        formulaMode: false,
        minimumValue: "0",
        modifyValueOnWheel: false,
        outputFormat: "string"
    });

    /* Limpiar campo monto_cuota al darle click al boton Limpiar Campos */
    $('button[id=limpiar]').click(function() {
        monto_cuota.set(0);
    });

    /* Abrir forma de pago si y solo si se seleccionó un producto primero */
    $('button[id=seleccionarFormaDePago]').click(function() {
        
        if ($('#monto_cuota').val() !== "0,00") {
            if ($('#monto_cuota').val().length !== 0) {
                $('#modal_formapago').modal('toggle'); //Abrir Modal
                $('#forma_pago_nombre').val('efectivo'); //Setear en valor "efectivo"
                $('#monto').val($('#monto_cuota').val()); //Setea el Precio de Venta al Monto Total
                $('.formulario_forma_pago').empty(); // Vacia elementos insertados con anterioridad
                $('.cantidad_cuotas').empty();
            };
        };
    });

    /* Codigo para autocompletar sugerencia de precio en base al costo y la moneda seleccionada.*/

    $('select[name=proveedor_id], select[name=moneda_id]').change(function() {
        
        var ProveedorId = $('select[name=proveedor_id]').val();
        var MonedaId = $('select[name=moneda_id]').val();
        
        $('#cuota_id').empty().append('<option value="" selected>Elegir una Cuota...</option>');
        $('#monto_cuota').val("");
        $('#sigla').val("");
        $('#vencimiento').val("");
        $('#forma_pago_id').val("");
        $('#cuota_detalle_id').val("");
        
        if (ProveedorId.length !== 0 && MonedaId.length !== 0) {
            generarCuotas();
        };
    });

    /* Codigo para autocompletar sugerencia de precio en base al costo y la moneda seleccionada, al cargar.*/
    if (($('#proveedor_id').val()) && ($('#moneda_id').val())) {
       generarCuotas(); 
    }

    function generarCuotas() {

        var ProveedorId = $('select[name=proveedor_id]').val();
        var MonedaId = $('select[name=moneda_id]').val();

        var ArrayFacturasCompras = {!! json_encode( $facturasCompras, JSON_HEX_TAG ) !!} ;

        $(ArrayFacturasCompras).each(function( index ) {
            if (ArrayFacturasCompras[index].proveedor_id == ProveedorId) {
                var factura_nro = ArrayFacturasCompras[index].factura_nro;
                var cuota_detalle_id = ArrayFacturasCompras[index].cuota_detalle_id;
                var cuota_total = ArrayFacturasCompras[index].cantidad_cuotas;
                var fecha_factura = ArrayFacturasCompras[index].fecha_factura;
                
                $('#cuota_id').append('<option value="' + cuota_detalle_id + '">' + 'Factura Nro: ' + factura_nro + ' del ' + fecha_factura.substring(8,10) + '/' + fecha_factura.substring(5,7) + '/' + fecha_factura.substring(0,4) + ' - Cuota ' + cuota_detalle_id + ' de ' + cuota_total + ' cuota(s)' + '</option>');
            }; 
        });
    };

    /* Codigo para rellenar los campos de acuerdo al nro de cuota pendiente seleccionada */
    $('select[name=cuota_id]').change(function() {
        var ProveedorId = $('select[name=proveedor_id]').val();
        var MonedaId = $('select[name=moneda_id]').val();
        var CuotaId = $('select[name=cuota_id]').val();
        
        //$('#cuota_id').empty();
        $('#monto_cuota').val("");
        $('#sigla').val("");
        $('#vencimiento').val("");
        $('#forma_pago_id').val("");
        $('#cuota_detalle_id').val("");
        
        if (ProveedorId.length !== 0 && MonedaId.length !== 0 && CuotaId.length !== 0) {
            

            var ArrayFacturasCompras = {!! json_encode( $facturasCompras, JSON_HEX_TAG ) !!} ;
            var ArrayMonedas = {!! json_encode( $monedas, JSON_HEX_TAG ) !!} ;

            $(ArrayFacturasCompras).each(function( index ) {
                if (ArrayFacturasCompras[index].proveedor_id == ProveedorId && ArrayFacturasCompras[index].cuota_detalle_id == CuotaId) {
                    //var factura_nro = ArrayFacturasCompras[index].factura_nro;
                    var cuota_detalle_id = ArrayFacturasCompras[index].cuota_detalle_id;
                    var cuota_total = ArrayFacturasCompras[index].cantidad_cuotas;
                    var forma_pago_id = ArrayFacturasCompras[index].forma_pago_id;
                    var fecha_factura = ArrayFacturasCompras[index].fecha_factura;
                    var monto_cuota_actual = ArrayFacturasCompras[index].monto_cuota;
                    var vencimiento = new Date(ArrayFacturasCompras[index].vencimiento);
                    var moneda = ArrayFacturasCompras[index].moneda_id;
                    var dolar = ArrayMonedas[0].cotizacion;
                    var euro = ArrayMonedas[2].cotizacion;
                    
                    $('#forma_pago_id').val(forma_pago_id);
                    $('#cuota_detalle_id').val(cuota_detalle_id);
                    $('#monto_cuota').val(monto_cuota_actual);
                    //$('#sigla').val(moneda);
                    $('#vencimiento').val(vencimiento.getDate() +'/'+ (vencimiento.getMonth() + 1) + '/' + vencimiento.getFullYear());

                    switch (MonedaId) { 
                        case "1":  //Guaranies
                            monto_cuota.update({ decimalPlaces: 0 });
                            monto_cuota_decimales = 0;
                            $('#sigla').val("GS");
                            switch (moneda) { 
                                case 1:
                                    resultado = monto_cuota_actual;
                                    break;
                                case 2:
                                    resultado = monto_cuota_actual * dolar;
                                    break;
                                case 3:
                                    resultado = monto_cuota_actual * euro;
                                    break;
                            };
                            break;
                        case "2": //Dolares
                            monto_cuota.update({ decimalPlaces: 2 });
                            monto_cuota_decimales = 2;
                            $('#sigla').val("USD");
                            switch (moneda) { 
                                case 1:
                                    resultado = monto_cuota_actual / dolar;
                                    break;
                                case 2:
                                    resultado = monto_cuota_actual;
                                    break;
                                case 3:
                                    resultado = monto_cuota_actual / dolar * euro;
                                    break;
                            };
                            break;
                        case "3": //Euros
                            monto_cuota.update({ decimalPlaces: 2 });
                            monto_cuota_decimales = 2;
                            $('#sigla').val("EUR");
                            switch (moneda) { 
                                case 1:
                                    resultado = monto_cuota_actual / euro;
                                    break;
                                case 2:
                                    resultado = monto_cuota_actual / euro * dolar;
                                    break;
                                case 3:
                                    resultado = monto_cuota_actual;
                                    break;
                            };
                            break;      
                        default:
                            console.log('Error en la cotización.');
                    };
                    monto_cuota.set(resultado);
                }; 
            });
        } else {
            monto_cuota.set(0);
            //$('#sigla').val("");
        };

    });

    /* Codigo para rellenar el div de formulario_forma_pago */

    $('select[id=forma_pago_nombre]').change(function() {

        var formaPagoNombre = $('select[id=forma_pago_nombre]').val();

        var formaCheques = `<div class="row"> 
                                <div class="col-md-6"> 
                                    <div class="form-group"><label for="numero_cheque"><strong>Número de Cheque</strong></label> 
                                        <input class="form-control" type="text" placeholder="" id="numero_cheque" name="numero_cheque"> 
                                    </div> 
                                </div> 
                                <div class="col-md-6"> 
                                    <div class="form-group"><label for="fecha_cheque"><strong>Fecha del Cheque</strong></label> 
                                        <input type="date" class="form-control datepicker" id="fecha_cheque" name="fecha_cheque" value="{{date('d/m/Y')}}">
                                    </div> 
                                </div> 
                            </div>
                            <div class="row"> 
                                <div class="col-md-12"> 
                                    <div class="form-group"><label for="banco"><strong>Banco</strong></label> 
                                        <input class="form-control" type="text" placeholder="" id="banco" name="banco"> 
                                    </div> 
                                </div> 
                            </div>`;
        var formaDepositosBancarios = `<div class="row"> 
                                <div class="col-md-6"> 
                                    <div class="form-group"><label for="numero_transaccion"><strong>Número de Transacción</strong></label> 
                                        <input class="form-control" type="text" placeholder="" id="numero_transaccion" name="numero_transaccion"> 
                                    </div> 
                                </div> 
                                <div class="col-md-6"> 
                                    <div class="form-group"><label for="fecha_deposito"><strong>Fecha de Depósito</strong></label> 
                                        <input type="date" class="form-control datepicker" id="fecha_deposito" name="fecha_deposito" value="{{date('d/m/Y')}}">
                                    </div> 
                                </div> 
                            </div>
                            <div class="row"> 
                                <div class="col-md-12"> 
                                    <div class="form-group"><label for="banco_receptor"><strong>Banco Receptor</strong></label> 
                                        <input class="form-control" type="text" placeholder="" id="banco_receptor" name="banco_receptor"> 
                                    </div> 
                                </div> 
                            </div>`;

        switch (formaPagoNombre) { 
            case "efectivo":  
                //console.log("paso por aca");
                //$( ".formulario_forma_pago" ).append( "TEXTO" );
                $('.formulario_forma_pago').empty()
                break;
            case "cheques":
                $('.formulario_forma_pago').empty().append(formaCheques);
                break;
            case "depositos_bancarios":
                $('.formulario_forma_pago').empty().append(formaDepositosBancarios);
                break;
        };
    });

    /* Codigo para cambiar el monto de la cuota al cambiar la cantidad 
    $(document).on('change', '#cantidad_cuotas', function() {
        var cantCuotasSel = $('input[id=cantidad_cuotas]').val();
        $("#monto_cuoteado").val(Number.parseFloat(monto_cuota.getNumber() / cantCuotasSel).toFixed(monto_cuota_decimales));
    }); */

</script>

@endsection