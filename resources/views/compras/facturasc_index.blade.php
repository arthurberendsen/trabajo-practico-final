@section ('contenido')

<div class="container-fluid">
    @if (session('successMessage'))
        <div class="alert alert-success" role="alert">
           {{ session('successMessage') }}
        </div>
    @endif

    @if (session('errorMessage'))
        <div class="alert alert-danger" role="alert">
           {{ session('errorMessage') }}
        </div>
    @endif

    <h3 class="text-dark mb-4">Facturas de Compra</h3>
    <div class="card shadow">
        <div class="card-header py-3">
            <p class="text-primary m-0 font-weight-bold">Listado de las Facturas de Compra</p>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 col-lg-4 col-xl-3 text-nowrap">
                    <div id="dataTable_length" class="dataTables_length" aria-controls="dataTable"><label>Mostrar  <select class="form-control form-control-sm custom-select custom-select-sm" id="pagination">
                        <option value="5" @if($items == 5) selected @endif>5</option>
                        <option value="10" @if($items == 10) selected @endif>10</option>
                        <option value="25" @if($items == 25) selected @endif>25</option>
                        <option value="50" @if($items == 50) selected @endif>50</option>
                        <option value="100" @if($items == 100) selected @endif>100</option></select></label></div>
                </div>
                <div class="col-md-4 col-lg-3 col-xl-2 offset-xl-0" style="text-align: left;"><a class="btn btn-primary btn-sm" href="{{ route('facturasc_create') }}">+ Agregar...</a></div>
                <div class="col-md-6 col-lg-5 col-xl-7">
                    <form autocomplete="off" method="POST" action="{{ route('facturasc_index') }}"> {{ csrf_field() }}
                        <div class="text-md-right dataTables_filter" id="dataTable_filter">
                            <label><div class="input-group">
                                <input type="search" class="form-control form-control-sm" aria-controls="dataTable" placeholder="Buscar..." id="buscar" name="buscar" value="{{ $buscar }}">
                                <div class="input-group-append"><button class="btn btn-primary py-0" type="submit"><i class="fas fa-search"></i></button></div>
                            </div></label>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table-responsive table mt-2 table-striped" id="dataTable" role="grid" aria-describedby="dataTable_info">
                <table class="table my-0 table-hover" id="dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>FC Nro</th>
                            <th>Fecha</th>
                            <th>Proveedor</th>
                            <th>Vehículo</th>
                            <th>Forma de Pago</th>
                            <th>Compra</th>
                            <th>Estado</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = $facturasCompra->firstItem();
                        // print_r($facturasCompra); 
                        ?>

                        
                        @foreach($facturasCompra as $facturaCompra)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $facturaCompra->factura_nro }}</td>
                            <td>{{ date('d/m/Y', strtotime($facturaCompra->fecha_factura)) }}</td>
                            <td>{{ $facturaCompra->razon_social }}</td>
                            <td>{{ $facturaCompra->marca }} {{ $facturaCompra->modelo }} {{ $facturaCompra->anho }}</td>
                            <td>{{ $facturaCompra->nombre }}</td>
                            <td>@switch( $facturaCompra->sigla )
                                    @case('Gs')
                                        {{ number_format($facturaCompra->precio_compra, $decimals = 0 , $dec_point = "," , $thousands_sep = "." ) }}
                                        @break

                                    @default
                                        {{ number_format($facturaCompra->precio_compra, $decimals = 2 , $dec_point = "," , $thousands_sep = "." ) }}
                                @endswitch
                                {{ $facturaCompra->sigla }}
                            </td>
                            <td>@if ($facturaCompra->estado == 1) Activo <i class="far fa-check-circle" style="color:green"></i>@else Anulado <i class="far fa-times-circle" style="color:red"></i> @endif</td>
                            <td style="padding-left: 0px;">
                                <a class="btn btn-primary btn-sm rounded-circle mr-2 btn-light" role="button" style="padding-bottom: 0px;padding-top: 2px;padding-right: 5px;padding-left: 3px;" href="{{ route('facturasc_show', $facturaCompra->factura_compra_id) }}" title="Mostrar Factura"><i class="material-icons">remove_red_eye</i></a>
                                <!-- <a class="btn btn-primary btn-sm rounded-circle mr-2 btn-light" role="button" style="padding-bottom: 0px;padding-top: 2px;padding-right: 5px;padding-left: 3px;" href="#"><i class="material-icons">edit</i></a> -->
                                <a class="btn btn-primary btn-sm rounded-circle btn-light" href="javascript:;" role="button" style="padding: 2px;padding-top: 2px;padding-left: 5px;padding-right: 3px;" data-toggle="modal" data-id="{{ $facturaCompra->factura_compra_id }}" data-target="#modalborrarfacturac-{{ $facturaCompra->factura_compra_id }}" title="Borrar Factura"><i class="material-icons">delete_sweep</i></a>
                                <a class="btn btn-primary btn-sm rounded-circle mr-2 btn-light" role="button" style="padding-bottom: 0px;padding-top: 2px;padding-right: 5px;padding-left: 3px;" data-toggle="modal" data-id="{{ $facturaCompra->factura_compra_id }}" data-target="#modalanularproveedor-{{ $facturaCompra->factura_compra_id }}" href="javascript:;" title="Anular Factura"><i class="material-icons">do_not_disturb_alt</i></a>

                                <!-- INICIO MODALES -->

                                <div role="dialog" tabindex="-1" class="modal fade" id="modalborrarfacturac-{{ $facturaCompra->factura_compra_id }}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Borrar Factura de Compra</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                                            <div class="modal-body">
                                                <p>¿Esta seguro que desea borrar la Factura Nro <b>{{ $facturaCompra->factura_nro }}</b> del proveedor <b>{{ $facturaCompra->razon_social }}</b> del sistema?</p>
                                            </div>
                                            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">Cancelar</button>
                                            
                                            <a class="btn btn-danger" href="{{ route('facturasc_index') }}" onclick="event.preventDefault(); document.getElementById('delete-form-{{$facturaCompra->factura_compra_id}}').submit();">Borrar</a></div>
                                            
                                            <form id="delete-form-{{$facturaCompra->factura_compra_id}}" + action="{{route('facturasc_destroy', $facturaCompra->factura_compra_id)}}" method="post"> 
                                                @csrf @method('DELETE')
                                            </form>
                                        </div>
                                    </div>
                                </div>


                                <div role="dialog" tabindex="-1" class="modal fade" id="modalanularproveedor-{{ $facturaCompra->factura_compra_id }}">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Anular Factura de Compra</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                                            <div class="modal-body">
                                                <p>¿Esta seguro que desea anular/desanular la Factura Nro <b>{{ $facturaCompra->factura_nro }}</b> del proveedor <b>{{ $facturaCompra->razon_social }}</b> del sistema?</p>
                                            </div>
                                            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">Cancelar</button>
                                            
                                            <a class="btn btn-danger" href="{{ route('facturasc_index') }}" onclick="event.preventDefault(); document.getElementById('anular-form-{{$facturaCompra->factura_compra_id}}').submit();">Anular/Desanular</a></div>
                                            
                                            <form id="anular-form-{{$facturaCompra->factura_compra_id}}" + action="{{route('facturasc_disable', $facturaCompra->factura_compra_id)}}" method="post"> 
                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <!-- FIN MODALES -->

                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><strong>#</strong></td>
                            <td><strong>FC Nro</strong></td>
                            <td><strong>Fecha</strong></td>
                            <td><strong>Proveedor</strong></td>
                            <td><strong>Vehículo</strong></td>
                            <td><strong>Forma de Pago</strong></td>
                            <td><strong>Compra</strong></td>
                            <td><strong>Estado</strong></td>
                            <td><strong>Acción</strong></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
            <div class="row">
                <div class="col-md-6 align-self-center">
                    <p id="dataTable_info" class="dataTables_info" role="status" aria-live="polite">Mostrando el registro {{$facturasCompra->firstItem() ?? 0 }} al {{$facturasCompra->lastItem() ?? 0 }} de {{$facturasCompra->total()}} registros</p>

                </div>
                <div class="col-md-6">
                    <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                        {{ $facturasCompra->appends(compact('items'))->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.getElementById('pagination').onchange = function() { 
        window.location = "{{ URL::route('facturasc_index') }}?items=" + this.value; 
    };   

</script>

@endsection