@section ('contenido')

<div class="container-fluid">
    <h3 class="text-dark mb-4">Cuota</h3>
    <div class="row mb-3">
        <div class="col-lg-10 col-xl-10">
            <div class="row">
                <div class="col offset-xl-0">

                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Mostrar Cuota</p>
                        </div>
                        <div class="card-body">
                                <div class="form-row">
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="cuota_detalle_id"><strong>Cuota</strong></label><input class="form-control" type="text" placeholder="" id="cuota_detalle_id" name="cuota_detalle_id" value="{{ $cuota->cuota_detalle_id }} de {{ $cuota->cantidad_cuotas }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="form-group"><label for="razon_social"><strong>Proveedor</strong></label><input class="form-control" type="text" placeholder="" id="razon_social" name="razon_social" value="{{ $cuota->razon_social }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-3">
                                        <div class="form-group"><label for="factura_nro"><strong>Factura Nro</strong></label><input class="form-control" type="text" placeholder="" id="factura_nro" name="factura_nro" value="{{ $cuota->factura_nro }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-3">
                                        <div class="form-group"><label for="fecha_factura"><strong>Factura Fecha</strong></label>
                                        <input type="text" class="form-control" name="fecha_factura" value="{{ date('d/m/Y', strtotime($cuota->fecha_factura)) }}" readonly>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="form-row">
                                    
                                    <div class="col-xl-3">
                                        <div class="form-group"><label for="fecha_vencimiento"><strong>Fecha Vencimiento</strong></label><input class="form-control" type="text" name="fecha_vencimiento"  value="{{ date('d/m/Y', strtotime($cuota->vencimiento)) }}" readonly></div>
                                    </div>
                                    <div class="col-xl-3">
                                        <div class="form-group"><label for="monto_cuota"><strong>Monto Cuota</strong></label><input type="text" class="form-control" name="monto_cuota" value="@switch( $cuota->sigla ) @case('Gs') {{ number_format($cuota->monto_cuota, $decimals = 0 , $dec_point = ',' , $thousands_sep = '.' ) }} @break @default {{ number_format($cuota->monto_cuota, $decimals = 2 , $dec_point = ',' , $thousands_sep = '.' ) }} @endswitch {{ $cuota->sigla }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-3">
                                        <div class="form-group"><label for="moneda_id"><strong>Moneda</strong></label><input class="form-control" type="text" name="moneda_id" value="{{ $cuota->nombre }}" readonly>
                                        </div>
                                    </div>
                                     <div class="col-xl-3">
                                        <div class="form-group"><label for="esta_pagado"><strong>Esta Pagado</strong> @if ($cuota->esta_pagado == 1) <i class="far fa-check-circle" style="color:green"></i>@else <i class="far fa-times-circle" style="color:red"></i> @endif</label><input class="form-control" type="text" name="esta_pagado" value="@if ($cuota->esta_pagado == 1) Pagado @else Pendiente @endif" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                <a class="btn btn-danger ml-3" href="{{ route('cuotasc_index') }}">Volver al Listado</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection