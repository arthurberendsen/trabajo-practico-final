@section ('contenido')

<div class="container-fluid">
    <h3 class="text-dark mb-4">Recibo</h3>
    <div class="row mb-3">
        <div class="col-lg-10 col-xl-10">
            <div class="row">
                <div class="col offset-xl-0">

                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Mostrar Recibo</p>
                            <!-- Default switch -->
                                <div class="custom-control custom-switch col text-right">
                                    <input type="checkbox" class="form-control custom-control-input" id="customSwitches" name="estado" @if ($recibo->estado == True) checked @endif>
                                    <label class="custom-control-label" for="customSwitches">Estado</label>
                                </div>
                        </div>
                        <div class="card-body">
                                <div class="form-row">
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="factura_nro"><strong>Número</strong></label><input class="form-control" type="text" placeholder="" id="factura_nro" name="factura_nro" value="{{ $recibo->numero }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="form-group"><label for="cliente_id"><strong>Cliente</strong></label><input class="form-control" type="text" placeholder="" id="cliente_id" name="cliente_id" value="{{ $facturaVenta->razon_social }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="form-group"><label for="codeudor_id"><strong>Moneda de Pago</strong></label><input class="form-control" type="text" placeholder="" id="codeudor_id" name="codeudor_id" value="{{ $moneda->nombre }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="fecha_factura"><strong>Fecha</strong></label>
                                        <input type="text" class="form-control" name="fecha_factura" value="{{ date('d/m/Y', strtotime($recibo->fecha)) }}" readonly>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                        </div>
                    </div>


                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Detalle de la Cuota</p>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-xl-6">
                                    <div class="form-group"><label for="vehiculo_id"><strong>Cuotas Cancelada</strong></label><input class="form-control" type="text" name="vehiculo_id" value="Factura Nro: {{ $facturaVenta->factura_nro }} de la fecha {{ date('d/m/Y', strtotime($facturaVenta->fecha_factura)) }} - Cuota {{ $facturaVenta->cuota_detalle_id }} de {{ $facturaVenta->cantidad_cuotas }} cuota(s)" readonly>
                                    </div>
                                    
                                </div>
                                <div class="col-xl-3">
                                    <div class="form-group"><label for="precio_venta"><strong>Monto Cuota</strong></label><input class="form-control auto" type="text" placeholder="" name="precio_venta" value="@switch( $facturaVenta->sigla )
                                    @case('Gs')
                                        {{ number_format($facturaVenta->monto_cuota, $decimals = 0 , $dec_point = ',' , $thousands_sep = '.' ) }}
                                        @break
                                    @default
                                        {{ number_format($facturaVenta->monto_cuota, $decimals = 2 , $dec_point = ',' , $thousands_sep = '.' ) }}
                                @endswitch" id="precio_venta" readonly>
                                    </div>
                                </div>
                                <div class="col-xl-1">
                                    <div class="form-group"><label for="sigla"><strong>Sigla</strong></label><input class="form-control" type="text" placeholder="" name="sigla" value="{{ $facturaVenta->sigla }}" id="sigla" readonly>
                                    </div>
                                </div>
                                <div class="col-xl-2">
                                    <div class="form-group"><label for="sigla"><strong>Vencimiento</strong></label><input class="form-control" type="text" placeholder="" name="sigla" value="{{ date('d/m/Y', strtotime($facturaVenta->vencimiento)) }}" id="sigla" readonly>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <a class="btn btn-danger ml-3" href="{{ route('recibosv_index') }}">Volver al Listado</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection