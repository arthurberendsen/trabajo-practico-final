@section ('contenido')

<!-- INICIO MODALES -->

<div role="dialog" tabindex="-1" class="modal fade" id="modal_descartar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Descartar Cambios</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <p>¿Desea realmente descartar los cambios realizados?</p>
            </div>
            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">No</button><a class="btn btn-primary" role="button" href="{{ route('proveedores_index') }}">Si</a></div>
        </div>
    </div>
</div>

<!-- FIN MODALES -->

<div class="container-fluid">

<!-- 	@if($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger" role="alert">
		  		ERROR: {{ $error }}
			</div>
		@endforeach
	@endif -->

    <h3 class="text-dark mb-4">Proveedores</h3>
    <div class="row mb-3">
        <div class="col-lg-8 col-xl-8">
            <div class="row">
                <div class="col offset-xl-0">
                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Alta de Proveedores</p>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('proveedores_store') }}" method="POST">
                            	{{ csrf_field() }}
                                <div class="form-row">
                                    <div class="col-xl-6">
                                        <div class="form-group"><label for="nombre_razonsocial"><strong>Nombre / Razón Social</strong></label><input class="form-control @error('razon_social') is-invalid @enderror" type="text" name="razon_social" id="razon_social" value="{{ old('razon_social') }}">
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Provea un Nombre o Razón Social</strong>
                                            </span>
                                        </div>
                                        
                                    </div>
                                    <div class="col">
                                        <div class="form-group"><label for="ruc_ci"><strong>RUC / CI</strong></label><input class="form-control @error('ruc_ci') is-invalid @enderror" type="text" name="ruc_ci" id="ruc_ci" value="{{ old('ruc_ci') }}">
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Provea el RUC o CI</strong>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-xl-12 offset-xl-0">
                                        <div class="form-group"><label for="direccion"><strong>Dirección</strong></label><input class="form-control" type="text" name="direccion" id="direccion" value="{{ old('direccion') }}"></div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-xl-6 offset-xl-0">
                                        <div class="form-group"><label for="ciudad"><strong>Ciudad</strong></label><select class="form-control @error('ciudad_id') is-invalid @enderror" id="ciudad_id" name="ciudad_id">
                                                <option value="" @if (old('ciudad_id') == '') selected @endif>Elegir Ciudad...</option>
                                            @foreach($ciudades as $ciudad)
                                                <option value="{{ $ciudad->ciudad_id }}" @if (old('ciudad_id') == $ciudad->ciudad_id) selected @endif>{{ $ciudad->nombre }}</option>
                                            @endforeach
                                        </select>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Seleccione la Ciudad</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group"><label for="telefono"><strong>Teléfonos</strong></label><input class="form-control" type="text"  name="telefono" id="telefono" value="{{ old('telefono') }}"></div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label for="correo_electronico"><strong>Correo Electrónico</strong></label><input class="form-control" type="email" name="correo" id="correo" value="{{ old('correo') }}"></div>
                                    </div>
                                    
                                </div>
                                <div class="form-row mb-2">
                                    <div class="col-xl-12"><label for="last_name"><strong>Observaciones</strong><br /></label><textarea class="form-control" rows="4" name="observacion" id="observacion">{{ old('observacion') }}</textarea></div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Guardar</button>
                                    <a class="btn btn-danger ml-3" href="#" data-toggle="modal" data-target="#modal_descartar">Descartar</a>
                                    <button class="btn btn-secondary ml-3" type="button" id="limpiar" name="limpiar">Limpiar campos</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection