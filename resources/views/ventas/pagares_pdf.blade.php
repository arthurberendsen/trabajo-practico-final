<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Pagaré a la Orden</title>

    <link rel="stylesheet" href="{{ asset('public/bstudio/bootstrap/css/bootstrap.min.css') }} ">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="{{ asset('public/bstudio/fonts/fontawesome-all.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('public/bstudio/fonts/material-icons.min.css') }} ">

    <script src="{{ asset('public/bstudio/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/bstudio/js/chart.min.js') }}"></script>
    <script src="{{ asset('public/bstudio/js/bs-init.js') }}"></script>
    <script src="{{ asset('public/bstudio/js/theme.js') }}"></script>
</head>

<body>
	
	<style>
	.page-break {
	    page-break-after: always;}
    th .encabezado, td .encabezado {
		  padding: 15px;}
	</style>

	@php 
		$meses = ['', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'setiembre', 'octubre', 'noviembre', 'diciembre']; 
	@endphp
	<div class="content">
		
		@foreach ($pagares as $clave => $pagare)
		    <h2 style="text-align: center;">PAGARÉ A LA ORDEN</h2>

			<table style="width: 100%; ">
				<tr class="encabezado">
				    <td class="encabezado" style="text-align: left;"><h5>Pagare Nro: {{ $pagare->cuota_nro }}/{{ $pagare->cantidad_cuotas }}</h5></td>
				    <td class="encabezado" style="text-align: center;"><h5>Vencimiento: {{ date("d/m/Y", strtotime($pagare->vencimiento)) }}</h5></td>
				    <td class="encabezado" style="text-align: right;"><h5>Monto: {{ $pagare->sigla }} @switch( $pagare->sigla )
                                    @case('Gs')
                                        {{ number_format($pagare->monto_cuota, $decimals = 0 , $dec_point = "," , $thousands_sep = "." ) }}
                                        @break

                                    @default
                                        {{ number_format($pagare->monto_cuota, $decimals = 2 , $dec_point = "," , $thousands_sep = "." ) }}
                                @endswitch.-</h5></td>
				</tr>
			</table>

			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Asunción, {{ date("d", strtotime($pagare->fecha_factura)) }} de {{ $meses[date("n", strtotime($pagare->fecha_factura))] }} del {{ date("Y", strtotime($pagare->fecha_factura)) }}</p>

		    <p>El día <strong>{{ date("d", strtotime($pagare->vencimiento)) }} de {{ $meses[date("n", strtotime($pagare->vencimiento))] }} del año {{ date("Y", strtotime($pagare->vencimiento)) }}</strong>, pagare libre de gastos y sin protesto a la orden de <strong>FERNANDO BIEDERMANN MAYOR</strong>, en su domicilio de la calle Blas Garay 106 esq. Independencia Nacional de la Ciudad de Asunción.</p>

		    <p><strong>LA CANTIDAD DE {{ $monto_letras[$clave] }}.</strong> Si este documento no fuere pagado en su vencimiento, devengara un interés del 3,00 % mensual, hasta la fecha de su pago efectivo, en concepto de la clausula penal por retardo (Art. 454 y 458 del C.C.P.), desde la mora al cual se producirá automáticamente (Art. 424 del C.C.P.) sin la necesidad de interpelación judicial o extrajudicial alguna</p>

		    <p>A los efectos de las obligaciones emergentes del presente documento fijo domicilio en <strong>Ybyrayu Nº 2.668 c/ Amador de Montoya de la Ciudad de Lambaré</strong>  (Art. 62 C.C.P.) y a todos los efectos legales acepto someterme a la jurisdicción y competencia de los tribunales ordinarios de la Ciudad de Asunción, Capital de la República del Paraguay</p>

		    <p>Dicho pagaré corresponde a la compra del Vehículo, Marca <strong>{{ strtoupper($pagare->marca) }}</strong>, Modelo <strong>{{ strtoupper($pagare->modelo) }}</strong>, Tipo <strong>{{ strtoupper($pagare->tipo) }}</strong>, Año de Fabricación <strong>{{ $pagare->anho }}</strong>, Color <strong>{{ strtoupper($pagare->color) }}</strong>, Chassis Nº <strong>{{ strtoupper($pagare->chasis) }}</strong>@if (!is_null($pagare->matricula)), Matrícula Nº <strong> {{ strtoupper($pagare->matricula )}} </strong>@endif.</p>

		    <br><br><br>
		    <table style="width: 100%; height: auto;">
				<tr>
				    <td style="text-align: center;">{{ strtoupper($pagare->cliente) }}</td>
				    @if (!is_null($pagare->codeudor))
				    <td style="text-align: center;">{{ strtoupper($pagare->codeudor) }}</td>
				    @endif
				</tr>
				<tr>
				    <td style="text-align: center;">C.I. Policial Nº {{ strtoupper($pagare->ci_cliente) }}</td>
				    @if (!is_null($pagare->codeudor))
				    <td style="text-align: center;">C.I. Policial Nº {{ strtoupper($pagare->ci_codeudor) }}</td>
				    @endif
				</tr>
				<tr>
				    <td style="text-align: center;">Deudor</td>
				    @if (!is_null($pagare->codeudor))
				    <td style="text-align: center;">Codeudor</td>
				    @endif
				</tr>
				
			</table>

		    @if ($loop->odd)
		    	<hr style="height:3px; border-width: 0; color: black !important; background-color: black;">
		    @endif

		    @if (($loop->even) && ($loop->index != 0))
				<div class="page-break"></div>
			@endif
		@endforeach
	
	</div>

</body>

</html>