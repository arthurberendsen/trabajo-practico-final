@section ('contenido')

<div class="container-fluid">
    <h3 class="text-dark mb-4">Clientes</h3>
    <div class="row mb-3">
        <div class="col-lg-8 col-xl-8">
            <div class="row">
                <div class="col offset-xl-0">
                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Datos del Cliente</p>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-xl-6">
                                    <div class="form-group"><label for="nombre_razonsocial"><strong>Nombre / Razón Social</strong></label><input readonly class="form-control" type="text" value="{{ $cliente->razon_social }}" name="razon_social" /></div>
                                </div>
                                <div class="col">
                                    <div class="form-group"><label for="ruc_ci"><strong>RUC / CI</strong></label><input readonly class="form-control" type="text"  value="{{ $cliente->ruc_ci }}" name="ruc_ci" /></div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-12 offset-xl-0">
                                    <div class="form-group"><label for="direccion"><strong>Dirección</strong></label><input readonly class="form-control" type="text" value="{{ $cliente->direccion }}" name="direccion" /></div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-6 offset-xl-0">
                                    <div class="form-group"><label for="ciudad"><strong>Ciudad</strong></label><input readonly class="form-control" type="text" value="{{ $ciudad->nombre }}" name="ciudad" />
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group"><label for="telefono"><strong>Teléfonos</strong></label><input readonly class="form-control" type="text" value="{{ $cliente->telefono }}" name="telefono" /></div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <div class="form-group"><label for="correo_electronico"><strong>Correo Electrónico</strong></label><input readonly class="form-control" type="email" value="{{ $cliente->correo }}" name="correo" /></div>
                                </div>
                                <div class="col">
                                    <div class="form-group"><label for="estado_civil"><strong>Estado Civil</strong></label><input readonly class="form-control" type="text" value="@if ($cliente->estado_civil == 'S') Soltero @elseif ($cliente->estado_civil == 'C') Casado @elseif ($cliente->estado_civil == 'V') Viudo @elseif ($cliente->estado_civil == 'D') Divorciado @endif" name="estado_civil" /></div>
                                </div>
                            </div>

                            <div class="form-row mb-2">
                                <div class="col-xl-12"><label for="last_name"><strong>Observaciones</strong><br /></label><textarea readonly class="form-control" rows="4" name="observacion" >{{ $cliente->observacion }}</textarea></div>
                            </div>
                            <div class="form-group">
                                <a class="btn btn-danger ml-3" href="{{ route('clientes_index') }}">Volver al Listado</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection