@section ('contenido')

<div class="container-fluid">
    <h3 class="text-dark mb-4">Factura de Venta</h3>
    <div class="row mb-3">
        <div class="col-lg-10 col-xl-10">
            <div class="row">
                <div class="col offset-xl-0">

                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Mostrar Factura de Venta</p>
                            <!-- Default switch -->
                                <div class="custom-control custom-switch col text-right">
                                    <input type="checkbox" class="form-control custom-control-input" id="customSwitches" name="estado" @if ($facturaVenta->estado == True) checked @endif>
                                    <label class="custom-control-label" for="customSwitches">Estado</label>
                                </div>
                        </div>
                        <div class="card-body">
                                <div class="form-row">
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="factura_nro"><strong>Número</strong></label><input class="form-control" type="text" placeholder="" id="factura_nro" name="factura_nro" value="{{ $facturaVenta->factura_nro }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="form-group"><label for="cliente_id"><strong>Cliente</strong></label><input class="form-control" type="text" placeholder="" id="cliente_id" name="cliente_id" value="{{ $cliente->razon_social }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="form-group"><label for="codeudor_id"><strong>Co-Deudor</strong></label><input class="form-control" type="text" placeholder="" id="codeudor_id" name="codeudor_id" value="@if (is_null($codeudor)) -ninguno- @else {{ $codeudor->razon_social }} @endif" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="fecha_factura"><strong>Fecha</strong></label>
                                        <input type="text" class="form-control" name="fecha_factura" value="{{ date('d/m/Y', strtotime($facturaVenta->fecha_factura)) }}" readonly>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="form-row">
                                    @if ($formaPago->nombre === "Efectivo")
                                    <div class="col-xl-3">
                                        <div class="form-group"><label for="fecha_vencimiento"><strong>Fecha Vencimiento</strong></label><input class="form-control" type="text" name="fecha_vencimiento"  value="{{ date('d/m/Y', strtotime($facturaVenta->fecha_vencimiento)) }}" readonly></div>
                                    </div>
                                    <div class="col-xl-3">
                                        <div class="form-group"><label for="es_contado"><strong>Cond. Venta</strong></label><input type="text" class="form-control" name="es_contado" value="@if ($facturaVenta->es_contado == True) Contado @else Credito @endif" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-3">
                                        <div class="form-group"><label for="moneda_id"><strong>Moneda</strong></label><input class="form-control" type="text" name="moneda_id" value="{{ $moneda->nombre }}" readonly>
                                        </div>
                                    </div>
                                     <div class="col-xl-3">
                                        <div class="form-group"><label for="forma_pago_nombre"><strong>Forma Pago</strong></label><input class="form-control" type="text" name="forma_pago_nombre" value="{{ $formaPago->nombre }}" readonly>
                                        </div>
                                    </div>
                                    @elseif ($formaPago->nombre === "Cheque")
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="fecha_vencimiento"><strong>Vencimiento</strong></label><input class="form-control" type="text" name="fecha_vencimiento"  value="{{ $facturaVenta->fecha_vencimiento }}" readonly></div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="es_contado"><strong>Cond. Venta</strong></label><input type="text" class="form-control" name="es_contado" value="@if ($facturaVenta->es_contado == True) Contado @else Credito @endif" readonly>
                                        </div>
                                    </div>
                                     <div class="col-xl-2">
                                        <div class="form-group"><label for="forma_pago_nombre"><strong>Forma Pago</strong></label><input class="form-control" type="text" name="forma_pago_nombre" value="{{ $formaPago->nombre }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="banco"><strong>Banco</strong></label><input class="form-control" type="text" name="banco" value="{{ $cheque->banco }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="numero_cheque"><strong>Nro. Cheque</strong></label><input class="form-control" type="text" name="numero_cheque" value="{{ $cheque->numero_cheque }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="fecha_cheque"><strong>Fecha Cheque</strong></label><input class="form-control" type="text" name="fecha_cheque" value="{{ $cheque->fecha_cheque }}" readonly>
                                        </div>
                                    </div>
                                    @elseif ($formaPago->nombre === "DepositoBancario")
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="fecha_vencimiento"><strong>Vencimiento</strong></label><input class="form-control" type="text" name="fecha_vencimiento"  value="{{ $facturaVenta->fecha_vencimiento }}" readonly></div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="es_contado"><strong>Cond. Venta</strong></label><input type="text" class="form-control" name="es_contado" value="@if ($facturaVenta->es_contado == True) Contado @else Credito @endif" readonly>
                                        </div>
                                    </div>
                                     <div class="col-xl-2">
                                        <div class="form-group"><label for="forma_pago_nombre"><strong>Forma Pago</strong></label><input class="form-control" type="text" name="forma_pago_nombre" value="{{ $formaPago->nombre }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="banco_receptor"><strong>Banco</strong></label><input class="form-control" type="text" name="banco_receptor" value="{{ $depositoBancario->banco_receptor }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="numero_transaccion"><strong>Nro. Transacción</strong></label><input class="form-control" type="text" name="numero_transaccion" value="{{ $depositoBancario->numero_transaccion }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="fecha_deposito"><strong>Fecha Depósito</strong></label><input class="form-control" type="text" name="fecha_deposito" value="{{ $depositoBancario->fecha_deposito }}" readonly>
                                        </div>
                                    </div>
                                    @elseif ($formaPago->nombre === "Cuotas")
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="fecha_vencimiento"><strong>Vencimiento</strong></label><input class="form-control" type="text" name="fecha_vencimiento"  value="{{ $facturaVenta->fecha_vencimiento }}" readonly></div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="es_contado"><strong>Cond. Venta</strong></label><input type="text" class="form-control" name="es_contado" value="@if ($facturaVenta->es_contado == True) Contado @else Credito @endif" readonly>
                                        </div>
                                    </div>
                                     <div class="col-xl-2">
                                        <div class="form-group"><label for="forma_pago_nombre"><strong>Forma Pago</strong></label><input class="form-control" type="text" name="forma_pago_nombre" value="{{ $formaPago->nombre }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="moneda_id"><strong>Moneda</strong></label><input class="form-control" type="text" name="moneda_id" value="{{ $moneda->nombre }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="cantidad_cuotas"><strong>Cant. Cuotas</strong></label><input class="form-control" type="text" name="cantidad_cuotas" value="{{ $cuota->cantidad_cuotas }}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="monto_cuota"><strong>Cuota Aprox.</strong></label><input class="form-control" type="text" name="monto_cuota" id="monto_cuota" value="@if ( $moneda->sigla = 'Gs') {{ number_format($cuotaDetalle->monto_cuota, $decimals = 0 , $dec_point = ',' , $thousands_sep = '.' ) }} @else {{ number_format($cuotaDetalle->monto_cuota, $decimals = 2 , $dec_point = ',' , $thousands_sep = '.' ) }} @endif" readonly>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                        </div>
                    </div>


                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Detalle de la Factura</p>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-xl-8">
                                    <div class="form-group"><label for="vehiculo_id"><strong>Vehículos</strong></label><input class="form-control" type="text" name="vehiculo_id" value="{{ $marca->nombre }} {{ $modelo->nombre }} - Año: {{ $vehiculo->anho_fabricacion }} - Color: {{ $color->nombre }}" readonly>
                                    </div>
                                    
                                </div>
                                <div class="col-xl-3">
                                    <div class="form-group"><label for="precio_venta"><strong>Precio de Venta</strong></label><input class="form-control auto" type="text" placeholder="" name="precio_venta" value="@switch( $moneda->sigla )
                                    @case('Gs')
                                        {{ number_format($facturaVenta->precio_venta, $decimals = 0 , $dec_point = ',' , $thousands_sep = '.' ) }}
                                        @break
                                    @default
                                        {{ number_format($facturaVenta->precio_venta, $decimals = 2 , $dec_point = ',' , $thousands_sep = '.' ) }}
                                @endswitch" id="precio_venta" readonly>
                                    </div>
                                </div>
                                <div class="col-xl-1">
                                    <div class="form-group"><label for="sigla"><strong>Sigla</strong></label><input class="form-control" type="text" placeholder="" name="sigla" value="{{ $moneda->sigla }}" id="sigla" readonly>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <a class="btn btn-danger ml-3" href="{{ route('facturasv_index') }}">Volver al Listado</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection