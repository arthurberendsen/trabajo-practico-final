@section ('contenido')

<div class="container-fluid">
    @if (session('successMessage'))
        <div class="alert alert-success" role="alert">
           {{ session('successMessage') }}
        </div>
    @endif
    
    @if (session('errorMessage'))
        <div class="alert alert-danger" role="alert">
           {{ session('errorMessage') }}
        </div>
    @endif
    
    <h3 class="text-dark mb-4">Pagarés</h3>
    <div class="card shadow">
        <div class="card-header py-3">
            <p class="text-primary m-0 font-weight-bold">Listado de Pagarés</p>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 col-lg-4 col-xl-3 text-nowrap">
                    <div id="dataTable_length" class="dataTables_length" aria-controls="dataTable"><label>Mostrar  <select class="form-control form-control-sm custom-select custom-select-sm" id="pagination"><option value="5" @if($items == 5) selected @endif>5</option><option value="10" @if($items == 10) selected @endif>10</option><option value="25" @if($items == 25) selected @endif>25</option><option value="50" @if($items == 50) selected @endif>50</option><option value="100" @if($items == 100) selected @endif>100</option></select></label></div>
                </div>
                <div class="col-md-4 col-lg-3 col-xl-2 offset-xl-0" style="text-align: left;"></div>
                <div class="col-md-6 col-lg-5 col-xl-7">
                    <form autocomplete="off" method="POST" action="{{ route('pagares_index') }}"> {{ csrf_field() }}
                        <div class="text-md-right dataTables_filter" id="dataTable_filter">
                            <label><div class="input-group">
                                <input type="search" class="form-control form-control-sm" aria-controls="dataTable" placeholder="Buscar..." id="buscar" name="buscar" value="{{ $buscar }}">
                                <div class="input-group-append"><button class="btn btn-primary py-0" type="submit"><i class="fas fa-search"></i></button></div>
                            </div></label>
                        </div>
                    </form>
                </div>
            </div>
            <div class="table-responsive table mt-2 table-striped" id="dataTable" role="grid" aria-describedby="dataTable_info">
                <table class="table my-0 table-hover" id="dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>Cant. Cuotas</th>
                            <th>Fac. Nro</th>
                            <th>Fac. Fecha</th>
                            <th>Vencimiento</th>
                            <th>Precio Venta</th>
                            <th>Cancelado</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = $pagares->firstItem(); ?>
                        
                        @foreach($pagares as $pagare)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $pagare->razon_social }}</td>
                            <td>{{ $pagare->cantidad_cuotas }}</td>
                            <td>{{ $pagare->factura_nro }}</td>
                            <td>{{ date("d/m/Y", strtotime($pagare->fecha_factura)) }}</td>
                            <td>{{ date("d/m/Y", strtotime($pagare->vencimiento)) }}</td>
                            <td>@switch( $pagare->sigla )
                                    @case('Gs')
                                        {{ number_format($pagare->precio_venta, $decimals = 0 , $dec_point = "," , $thousands_sep = "." ) }}
                                        @break
                                    @default
                                        {{ number_format($pagare->precio_venta, $decimals = 2 , $dec_point = "," , $thousands_sep = "." ) }}
                                @endswitch
                                {{ $pagare->sigla }}
                            </td>
                            <td>@if ($pagare->esta_pagado == 1) Cancelado <i class="far fa-check-circle" style="color:green"></i>@else Pendiente <i class="far fa-times-circle" style="color:red"></i> @endif</td>
                            <td style="padding-left: 0px;">
                                <a class="btn btn-primary btn-sm rounded-circle mr-2 btn-light" role="button" style="padding-bottom: 0px;padding-top: 2px;padding-right: 5px;padding-left: 3px;" href="{{ route('pagares_pdf', $pagare->forma_pago_id) }}"><i class="material-icons">print</i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><strong>#</strong></td>
                            <td><strong>Cliente</strong></td>
                            <td><strong>Cant. Cuotas</strong></td>
                            <td><strong>Fac. Nro</strong></td>
                            <td><strong>Fac. Fecha</strong></td>
                            <td><strong>Vencimiento</strong></td>
                            <td><strong>Precio Venta</strong></td>
                            <td><strong>Cancelado</strong></td>
                            <td><strong>Acción</strong></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
            <div class="row">
                <div class="col-md-6 align-self-center">
                    <p id="dataTable_info" class="dataTables_info" role="status" aria-live="polite">Mostrando el registro {{$pagares->firstItem() ?? 0 }} al {{$pagares->lastItem() ?? 0 }} de {{$pagares->total()}} registros</p>

                </div>
                <div class="col-md-6">
                    <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                        {{ $pagares->appends(compact('items'))->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.getElementById('pagination').onchange = function() { 
        window.location = "{{ URL::route('pagares_index') }}?items=" + this.value; 
    };   

</script>

@endsection