@section ('contenido')

<script>
    console.log("Jquery version: " + jQuery().jquery);
    
</script>

<form action="{{ route('facturasv_store') }}" id="crear_factura" method="POST">
{{ csrf_field() }}

<!-- INICIO MODALES -->

<div role="dialog" tabindex="-1" class="modal fade" id="modal_descartar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Descartar Cambios</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <p>¿Desea realmente descartar los cambios realizados?</p>
            </div>
            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">No</button><a class="btn btn-primary" role="button" href="{{ route('facturasv_index') }}">Si</a></div>
        </div>
    </div>
</div>


<div role="dialog" tabindex="-1" class="modal fade" id="modal_formapago">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Definir Forma de Pago</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"><label for="forma_pago_nombre"><strong>Forma de Pago</strong></label>
                                <select class="form-control" id="forma_pago_nombre" name="forma_pago_nombre">
                                    <!-- <option value="efectivo" selected>Efectivo</option>
                                    <option value="cheques">Cheques</option>
                                    <option value="depositos_bancarios">Depósito Bancario</option>
                                    <option value="cuotas">Cuotas</option> -->
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-md-12">
                                <div class="form-group"><label for="monto"><strong>Monto</strong></label><input class="form-control" type="text" placeholder="" id="monto" name="monto" readonly>
                                </div>
                            </div>
                        </div>  
                    <div class="formulario_forma_pago">
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <div class="form-group"><label for="monto"><strong>Monto</strong></label><input class="form-control" type="text" placeholder="" id="monto" name="Monto" readonly>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-warning" type="button" data-dismiss="modal">Volver atrás</button>
                <a class="btn btn-primary" role="button" href="{{ route('facturasv_index') }}" onclick="event.preventDefault(); document.getElementById('crear_factura').submit();">Guardar Cambios</a>
            </div>
        </div>
    </div>
</div>

<!-- FIN MODALES -->

<div class="container-fluid">
    
    <!-- <div class="alert alert-danger" role="alert">
        <p id=error_container></p>
    </div> -->

<!-- 	@if($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger" role="alert">
		  		ERROR: {{ $error }}
			</div>
		@endforeach
	@endif -->

    <h3 class="text-dark mb-4">Factura de Venta</h3>
    <div class="row mb-3">
        <div class="col-lg-10 col-xl-10">
            <div class="row">
                <div class="col offset-xl-0">

                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Nueva Factura de Venta</p>
                        </div>
                        <div class="card-body">
                                <div class="form-row">
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="factura_nro"><strong>Número</strong></label><input class="form-control @error('factura_nro') is-invalid @enderror " type="text" placeholder="" id="factura_nro" name="factura_nro" value="{{ old('factura_nro') }}">
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Provea un Nro. de Factura</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="form-group"><label for="cliente_id"><strong>Cliente</strong></label><select class="form-control @error('cliente_id') is-invalid @enderror" id="cliente_id" name="cliente_id">
                                                <option value="" @if (old('cliente_id') == '') selected @endif>Elegir un Cliente...</option>
                                            @foreach($clientes as $cliente)
                                                <option value="{{ $cliente->cliente_id }}" @if (old('cliente_id') == $cliente->cliente_id) selected @endif>{{ $cliente->razon_social }}</option>
                                            @endforeach
                                        </select>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Seleccione el Cliente</strong>
                                            </span>
                                        </div>
                                        
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="form-group"><label for="codeudor_id"><strong>Co-Deudor</strong></label><select class="form-control" id="codeudor_id" name="codeudor_id">
                                                <option value="" @if (old('codeudor_id') == '') selected @endif>Elegir un Codeudor...</option>
                                            @foreach($clientes as $cliente)
                                                <option value="{{ $cliente->cliente_id }}" @if (old('codeudor_id') == $cliente->cliente_id) selected @endif>{{ $cliente->razon_social }}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-xl-2">
                                        <div class="form-group"><label for="fecha_factura"><strong>Fecha</strong></label>
                                        <input type="text" class="form-control datepicker @error('fecha_factura') is-invalid @enderror" name="fecha_factura" value="@if (old('fecha_factura')) {{ old('fecha_factura') }} @else {{ date('d/m/Y') }} @endif">
                                        <span class="invalid-feedback" role="alert">
                                                <strong>Seleccione una Fecha</strong>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-xl-3">
                                        <div class="form-group"><label for="fecha_vencimiento"><strong>Fecha Vencimiento</strong></label><input class="form-control datepicker" type="text" name="fecha_vencimiento"  value="{{date('d/m/Y')}}"></div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="form-group"><label for="es_contado"><strong>Cond. Venta</strong></label><select class="form-control @error('es_contado') is-invalid @enderror" id="es_contado" name="es_contado">
                                            <option value="" @if (old('es_contado') == '') selected @endif>Elegir...</option>
                                            <option value="True" @if (old('es_contado') == "True") selected @endif>Contado</option>
                                            <option value="False" @if (old('es_contado') == "False") selected @endif>Crédito</option>
                                        </select>
                                         <span class="invalid-feedback" role="alert">
                                                <strong>Seleccione la Cond. Venta</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-xl-5">
                                        <div class="form-group"><label for="moneda_id"><strong>Moneda</strong></label>
                                            <select class="form-control" id="moneda_id" name="moneda_id">
                                                <option value="" @if (old('moneda_id') == '') selected @endif>Elegir una moneda...</option>
                                            @foreach($monedas as $moneda)
                                                <option value="{{ $moneda->moneda_id }}" @if (old('moneda_id') == $moneda->moneda_id) selected @endif>{{ $moneda->nombre }}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>


                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Detalle</p>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-xl-8">
                                    <div class="form-group"><label for="vehiculo_id"><strong>Vehículos</strong></label>
                                        <select class="form-control @error('vehiculo_id') is-invalid @enderror" id="vehiculo_id" name="vehiculo_id">
                                            <option value="" @if (old('vehiculo_id') == '') selected @endif>Elegir un Vehículo...</option>
                                        @foreach($vehiculos as $vehiculo)
                                            <option value="{{ $vehiculo->vehiculo_id }}" @if (old('vehiculo_id') == $vehiculo->vehiculo_id) selected @endif>{{ $vehiculo->marca }} {{ $vehiculo->modelo }} - Año: {{ $vehiculo->anho }} - Color: {{ $vehiculo->color }} - Precio: {{ $vehiculo->costo }} {{ $vehiculo->sigla }} </option>
                                        @endforeach
                                    </select>
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Seleccione el vehículo</strong>
                                        </span>
                                    </div>
                                    
                                </div>
                                <div class="col-xl-3">
                                    <div class="form-group"><label for="precio_venta"><strong>Precio de Venta</strong></label><input class="form-control auto" type="text" placeholder="" name="precio_venta" id="precio_venta">
                                    </div>
                                </div>
                                <div class="col-xl-1">
                                    <div class="form-group"><label for="sigla"><strong>Sigla</strong></label><input class="form-control" type="text" placeholder="" name="sigla" value="" id="sigla" readonly>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <button class="btn btn-primary" type="button" id="seleccionarFormaDePago">Seleccionar Forma de Pago</button>
                                <a class="btn btn-danger ml-3" href="#" data-toggle="modal" data-target="#modal_descartar">Descartar</a>
                                <button class="btn btn-secondary ml-3" type="button" id="limpiar" name="limpiar">Limpiar campos</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>

<script type="text/javascript">

    /* Codigo para inicializar el DatePicker */
    $('.datepicker').datepicker({
        clearBtn: true,
        language: "es",
        autoclose: true,
        todayHighlight: true
    });
    
    var precioVenta_decimales = 0;

    /* Codigo para inicializar el AutoNumeric, y formatear el precio_venta automaticamente */
    var precioVenta = new AutoNumeric('#precio_venta', {
        //allowDecimalPadding: false,
        decimalCharacter: ",",
        decimalCharacterAlternative: ",",
        digitGroupSeparator: ".",
        formulaMode: false,
        minimumValue: "0",
        modifyValueOnWheel: false,
        outputFormat: "string"
    });

    /* Limpiar campo PrecioVenta al darle click al boton Limpiar Campos */
    $('button[id=limpiar]').click(function() {
        precioVenta.set(0);
    });

    /* Resetear las formas de pago en caso que se cambie la condicion de Venta */
    CondVenta = $('select[name=es_contado]').val();
    var cv_contado = `<option value="efectivo" selected>Efectivo</option>
                            <option value="cheques">Cheques</option>
                            <option value="depositos_bancarios">Depósito Bancario</option>`;
    var cv_credito = `<option value="cuotas" selected>Cuotas</option>`;

    if (CondVenta == 'True') {
        $('#forma_pago_nombre').empty().append(cv_contado);
    } else {
        $('#forma_pago_nombre').empty().append(cv_credito);
    };

    $('select[name=es_contado]').change(function() {
        CondVenta = $('select[name=es_contado]').val();
        //if(typeof page_name != 'undefined')
        if (CondVenta == 'True') {
            $('#forma_pago_nombre').empty().append(cv_contado);
        } else {
            if (CondVenta == 'False') {
                $('#forma_pago_nombre').empty().append(cv_credito);
            } else {
                CondVenta = undefined;
            }
        }

    });
    
    /* Desactiva/activa el cliente/codeudor en base al cliente/codeudor seleccionado */
    $('select[name=cliente_id], select[name=codeudor_id]').change(function() {
        
        $('select[name=codeudor_id] option').removeAttr('disabled');
        $('select[name=cliente_id] option').removeAttr('disabled');

        var ClienteId = $('select[name=cliente_id]').val();
        var CodeudorId = $('select[name=codeudor_id]').val();

        if (ClienteId.length !== 0) {
            $('select[name=codeudor_id] option[value="' + ClienteId + '"]').attr('disabled', 'disabled');
        };

        if (CodeudorId.length !== 0) {
            $('select[name=cliente_id] option[value="' + CodeudorId + '"]').attr('disabled', 'disabled');
        };
    });

    /* Abrir forma de pago si y solo si se seleccionó un producto primero */
    $('button[id=seleccionarFormaDePago]').click(function() {
        
        if (($('#precio_venta').val() !== "0,00") && (typeof CondVenta != 'undefined')) {
            if ($('#precio_venta').val().length !== 0) {
                $('#modal_formapago').modal('toggle'); //Abrir Modal
                $('#forma_pago_nombre').val('efectivo'); //Setear en valor "efectivo"
                $('#monto').val($('#precio_venta').val()); //Setea el Precio de Venta al Monto Total
                $('.formulario_forma_pago').empty(); // Vacia elementos insertados con anterioridad
                $('.cantidad_cuotas').empty();
            } else {
                //Mostar un error:
                console.log("El monto dentro de PRECIO DE VENTA no puede ser 0 o Nulo");
            };
        };
    });

    /* Codigo para autocompletar sugerencia de precio en base al costo y la moneda seleccionada, al cambiar.*/
    $('select[name=vehiculo_id], select[name=moneda_id]').change(function() {
        calcularPrecioVenta();
    });

    /* Codigo para autocompletar sugerencia de precio en base al costo y la moneda seleccionada, al empezar.*/
    if (($('#vehiculo_id').val()) && ($('#moneda_id').val())) {
       calcularPrecioVenta(); 
    }

    /* Funcion para autocompletar sugerencia de precio en base al costo y la moneda seleccionada.*/
    function calcularPrecioVenta() {
        var mult = 1.5; // valor del multiplicador del costo
        
        var VehiculoId = $('select[name=vehiculo_id]').val();
        var MonedaId = $('select[name=moneda_id]').val();

        if (VehiculoId.length !== 0 && MonedaId.length !== 0) {
            var ArrayVehiculos = {!! json_encode( $vehiculos, JSON_HEX_TAG ) !!} ;
            var ArrayMonedas = {!! json_encode( $monedas, JSON_HEX_TAG ) !!} ;

            $(ArrayVehiculos).each(function( index ) {
                if (ArrayVehiculos[index].vehiculo_id == VehiculoId) {
                    var costo = ArrayVehiculos[index].costo;
                    var moneda = ArrayVehiculos[index].moneda_id;
                    var dolar = ArrayMonedas[0].cotizacion;
                    var euro = ArrayMonedas[2].cotizacion;
            
                    var resultado;

                    switch (MonedaId) { 
                        case "1":  //Guaranies
                            precioVenta.update({ decimalPlaces: 0 });
                            precioVenta_decimales = 0;
                            $('#sigla').val("GS");
                            switch (moneda) { 
                                case 1:
                                    resultado = costo;
                                    break;
                                case 2:
                                    resultado = costo * dolar;
                                    break;
                                case 3:
                                    resultado = costo * euro;
                                    break;
                            };
                            break;
                        case "2": //Dolares
                            precioVenta.update({ decimalPlaces: 2 });
                            precioVenta_decimales = 2;
                            $('#sigla').val("USD");
                            switch (moneda) { 
                                case 1:
                                    resultado = costo / dolar;
                                    break;
                                case 2:
                                    resultado = costo;
                                    break;
                                case 3:
                                    resultado = costo / dolar * euro;
                                    break;
                            };
                            break;
                        case "3": //Euros
                            precioVenta.update({ decimalPlaces: 2 });
                            precioVenta_decimales = 2;
                            $('#sigla').val("EUR");
                            switch (moneda) { 
                                case 1:
                                    resultado = costo / euro;
                                    break;
                                case 2:
                                    resultado = costo / euro * dolar;
                                    break;
                                case 3:
                                    resultado = costo;
                                    break;
                            };
                            break;      
                        default:
                            console.log('Error en la cotización.');
                    };
                    precioVenta.set(resultado * mult);
                }; 
            });
        } else {
            precioVenta.set(0);
            $('#sigla').val("");
        };
    };

    /* Codigo para rellenar el div de formulario_forma_pago */

    $('select[id=forma_pago_nombre]').change(function() {

        var formaPagoNombre = $('select[id=forma_pago_nombre]').val();

        var formaCheques = `<div class="row"> 
                                <div class="col-md-6"> 
                                    <div class="form-group"><label for="numero_cheque"><strong>Número de Cheque</strong></label> 
                                        <input class="form-control" type="text" placeholder="" id="numero_cheque" name="numero_cheque"> 
                                    </div> 
                                </div> 
                                <div class="col-md-6"> 
                                    <div class="form-group"><label for="fecha_cheque"><strong>Fecha del Cheque</strong></label> 
                                        <input type="date" class="form-control datepicker" id="fecha_cheque" name="fecha_cheque" value="{{date('d/m/Y')}}">
                                    </div> 
                                </div> 
                            </div>
                            <div class="row"> 
                                <div class="col-md-12"> 
                                    <div class="form-group"><label for="banco"><strong>Banco</strong></label> 
                                        <input class="form-control" type="text" placeholder="" id="banco" name="banco"> 
                                    </div> 
                                </div> 
                            </div>`;
        var formaDepositosBancarios = `<div class="row"> 
                                <div class="col-md-6"> 
                                    <div class="form-group"><label for="numero_transaccion"><strong>Número de Transacción</strong></label> 
                                        <input class="form-control" type="text" placeholder="" id="numero_transaccion" name="numero_transaccion"> 
                                    </div> 
                                </div> 
                                <div class="col-md-6"> 
                                    <div class="form-group"><label for="fecha_deposito"><strong>Fecha de Depósito</strong></label> 
                                        <input type="date" class="form-control datepicker" id="fecha_deposito" name="fecha_deposito" value="{{date('d/m/Y')}}">
                                    </div> 
                                </div> 
                            </div>
                            <div class="row"> 
                                <div class="col-md-12"> 
                                    <div class="form-group"><label for="banco_receptor"><strong>Banco Receptor</strong></label> 
                                        <input class="form-control" type="text" placeholder="" id="banco_receptor" name="banco_receptor"> 
                                    </div> 
                                </div> 
                            </div>`;
        var formaCuotas = `<div class="row"> 
                                <div class="col-md-6"> 
                                    <div class="form-group"><label for="cantidad_cuotas"><strong>Cantidad de Cuotas</strong></label> 
                                        <input class="form-control" type="number" placeholder="" value=12 id="cantidad_cuotas" name="cantidad_cuotas" min="1" max="60"> 
                                    </div> 
                                </div> 
                                <div class="col-md-6"> 
                                    <div class="form-group"><label for="monto_cuoteado"><strong>Monto Cuota</strong></label> 
                                        <input class="form-control" type="text" placeholder="" value="` 
        formaCuotas += Number.parseFloat(precioVenta.getNumber() / 12).toFixed(precioVenta_decimales);
        formaCuotas += `" id="monto_cuoteado" name="monto_cuoteado" readonly> 
                                    </div> 
                                </div> 
                            </div>
                            <div class="cantidad_cuotas"></div>`;


        switch (formaPagoNombre) { 
            case "efectivo":  
                $('.formulario_forma_pago').empty()
                break;
            case "cheques":
                $('.formulario_forma_pago').empty().append(formaCheques);
                break;
            case "depositos_bancarios":
                $('.formulario_forma_pago').empty().append(formaDepositosBancarios);
                break;
            case "cuotas":
                $('.formulario_forma_pago').empty().append(formaCuotas);
                break;
        };
    });

    /* Codigo para cambiar el monto de la cuota al cambiar la cantidad */
    $(document).on('change', '#cantidad_cuotas', function() {
        var cantCuotasSel = $('input[id=cantidad_cuotas]').val();
        $("#monto_cuoteado").val(Number.parseFloat(precioVenta.getNumber() / cantCuotasSel).toFixed(precioVenta_decimales));
    });

</script>

@endsection