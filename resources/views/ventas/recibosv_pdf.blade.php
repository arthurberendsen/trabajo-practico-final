<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Recibo de Pago</title>

    <link rel="stylesheet" href="{{ asset('public/bstudio/bootstrap/css/bootstrap.min.css') }} ">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="{{ asset('public/bstudio/fonts/fontawesome-all.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('public/bstudio/fonts/material-icons.min.css') }} ">

    <script src="{{ asset('public/bstudio/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/bstudio/js/chart.min.js') }}"></script>
    <script src="{{ asset('public/bstudio/js/bs-init.js') }}"></script>
    <script src="{{ asset('public/bstudio/js/theme.js') }}"></script>
</head>

<body>
	
	<style>
		.page-break {
		    page-break-after: always;
		}
	    th .encabezado, td .encabezado {
			  padding: 10px;
		}
		h2, h4, h5 {
			margin-bottom: 6px;
			margin-top: 0px;
		}
	</style>

	<div class="content" style="width: 95%; margin-left: auto; margin-right: auto;">

		<table style="width: 100%;">
			<tr class="encabezado">
				<td class="encabezado" style="text-align: left; color: black;"><h2><b>RECIBO DE PAGO</b></h2> </td>
			    <td class="encabezado" style="text-align: right;"><h5><b>Fecha:&nbsp;&nbsp;</b></h5></td>
			    <td class="encabezado" style="text-align: center; color: black;"><h5>{{ date("d/m/Y", strtotime($recibo[0]->fecha)) }}</h5></td>
			    <td class="encabezado" style="text-align: right;"><h5><b>Número:&nbsp;&nbsp;</b></h5></td>
			    <td class="encabezado" style="text-align: left; color: black;"><h5>{{ $recibo[0]->nro_recibo }}</td>
			</tr>
		</table>

	    <br><br>

	    <table style="width: 100%;">
			<tr>
			    <td style="text-align: right; width: 15%;"><h5>Recibí de:&nbsp;&nbsp;</h5></td>
			    <td class="border-bottom" style="text-align: left; width: 50%; color: black;"><h4>{{ strtoupper($recibo[0]->cliente) }}<h4></td>
			    <td class="border-0" style="text-align: right; width: 15%;"><h5>Cantidad&nbsp;</h5></td>
			    <td class="border border-dark rounded m-0" style="text-align: left; width: 20%; color: black;"><h4>&nbsp;{{ strtoupper($recibo[0]->sigla) }} @switch( $recibo[0]->sigla )
                                    @case('Gs')
                                        {{ number_format($recibo[0]->monto_cuota, $decimals = 0 , $dec_point = "," , $thousands_sep = "." ) }}.-
                                        @break

                                    @default
                                        {{ number_format($recibo[0]->monto_cuota, $decimals = 2 , $dec_point = "," , $thousands_sep = "." ) }}.-
                                @endswitch<h4></td>
			</tr>
			<tr>
				<td style="text-align: right; width: 15%;"><h5>Cantidad:&nbsp;&nbsp;</h5></td>
			    <td colspan="3" class="border-bottom" style="text-align: left; width: 85%; color: black;"><h4>{{ strtoupper($monto_letras[0]) }}.-<h4></td>
			</tr>
			<tr>
				<td style="text-align: right; width: 15%;"><h5>Concepto:&nbsp;&nbsp;</h5></td>
			    <td colspan="3" class="border-bottom" style="text-align: left; width: 85%; color: black;"><h4>Pago de la Cuota Nro. {{ $recibo[0]->nro_cuota }} correspondiente a la Factura Nro: {{ $recibo[0]->factura_nro }} con fecha {{ date("d/m/Y", strtotime($recibo[0]->fecha_factura)) }}.-<h4></td>
			</tr>
		</table>

		<br><br>

		<table style="width: 100%;">
			<tr>
				<td style="text-align: right; width: 17%;"><h5>Recibido por:&nbsp;&nbsp;</h5></td>
			    <td class="border-bottom" style="text-align: left; width: 20%;">&nbsp;</td>
			    <td style="text-align: right; "><h5>Forma de Pago:&nbsp;&nbsp;</h5></td>
			    <td class="border-bottom" style="text-align: left; width: 45%; color: black;">@switch( $recibo[0]->forma_pago )
                                    @case('Efectivo')
                                        <h4>Efectivo.-</h4>
                                        @break

                                    @case('Cheque')
                                        <h5>Cheque Nro: {{ $recibo[0]->nro_cheque }} - {{ $recibo[0]->banco_cheque }}.-</h5>
                                        @break

                                    @case('DepositoBancario')
                                    	<h5>Transferencia Nro: {{ $recibo[0]->nro_deposito }} - {{ $recibo[0]->banco_depósito }}.-</h5>
                                    	@break
                                @endswitch</td>
			</tr>
		</table>

		<br><br>
		<hr style="height:3px; border-width: 0; color: black !important; background-color: black;">
	</div>

</body>

</html>