@section ('contenido')

<!-- INICIO MODALES -->

<div role="dialog" tabindex="-1" class="modal fade" id="modal_descartar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Descartar Cambios</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <p>¿Desea realmente descartar los cambios realizados?</p>
            </div>
            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">No</button><a class="btn btn-primary" role="button" href="{{ route('clientes_index') }}">Si</a></div>
        </div>
    </div>
</div>

<!-- FIN MODALES -->

<div class="container-fluid">

	@if($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger" role="alert">
		  		{{ $error }}
			</div>
		@endforeach
	@endif

    <h3 class="text-dark mb-4">Clientes</h3>
    <div class="row mb-3">
        <div class="col-lg-8 col-xl-8">
            <div class="row">
                <div class="col offset-xl-0">
                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <div class="form-row">
                                <p class="text-primary m-0 font-weight-bold col-xl-6">Modificación de Clientes</p>
                                <!-- Default switch -->
                                <div class="custom-control custom-switch col text-right">
                                      <form action="{{ route('clientes_update', $cliente->cliente_id) }}" method="POST">
                                {{ csrf_field() }}
                                    <input type="checkbox" class="form-control custom-control-input" id="customSwitches" name="estado" @if ($cliente->estado == True) checked @endif>
                                    <label class="custom-control-label" for="customSwitches">Estado</label>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                          
                                <div class="form-row">
                                    <div class="col-xl-6">
                                        <div class="form-group"><label for="nombre_razonsocial"><strong>Nombre / Razón Social</strong></label><input class="form-control @error('razon_social') is-invalid @enderror" type="text" value="{{ $cliente->razon_social }}" name="razon_social" />
                                             <span class="invalid-feedback" role="alert">
                                                <strong>Provea un Nombre o Razón Social</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group"><label for="ruc_ci"><strong>RUC / CI</strong></label><input class="form-control @error('ruc_ci') is-invalid @enderror" type="text" value="{{ $cliente->ruc_ci }}" name="ruc_ci" />
                                            <span class="invalid-feedback" role="alert"><strong>Provea el RUC o CI</strong>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-xl-12 offset-xl-0">
                                        <div class="form-group"><label for="direccion"><strong>Dirección</strong></label><input class="form-control" type="text" value="{{ $cliente->direccion }}" name="direccion" /></div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-xl-6 offset-xl-0">
                                        <div class="form-group"><label for="ciudad"><strong>Ciudad</strong></label><select class="form-control @error('ciudad_id') is-invalid @enderror" id="ciudad" name="ciudad_id">
                                        @foreach($ciudades as $ciudad)
                                            <option value="{{ $ciudad->ciudad_id }}" @if ($cliente->ciudad_id == $ciudad->ciudad_id) Selected @endif>{{ $ciudad->nombre }}</option>
                                        @endforeach
                                        </select>
                                        <span class="invalid-feedback" role="alert">
                                                <strong>Seleccione la Ciudad</strong>
                                        </span>
                                    </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group"><label for="telefono"><strong>Teléfonos</strong></label><input class="form-control" type="text" value="{{ $cliente->telefono }}" name="telefono" /></div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label for="correo_electronico"><strong>Correo Electrónico</strong></label><input class="form-control" type="email" value="{{ $cliente->correo }}" name="correo" /></div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group"><label for="estado_civil"><strong>Estado Civil</strong><br /></label><select class="form-control @error('estado_civil') is-invalid @enderror" name="estado_civil">
                                            <option value="S" @if ($cliente->estado_civil == 'S') Selected @endif>Soltero</option>
                                            <option value="C" @if ($cliente->estado_civil == 'C') Selected @endif>Casado</option>
                                            <option value="V" @if ($cliente->estado_civil == 'V') Selected @endif>Viudo</option>
                                            <option value="D" @if ($cliente->estado_civil == 'D') Selected @endif>Divorciado</option>
                                        </select>
                                        <span class="invalid-feedback" role="alert">
                                            <strong>Seleccione el Estado Civil</strong>
                                        </span>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-row mb-2">
                                    <div class="col-xl-12"><label for="last_name"><strong>Observaciones</strong><br /></label><textarea class="form-control" rows="4" name="observacion">{{ $cliente->observacion }}</textarea></div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Guardar</button><a class="btn btn-danger ml-3" href="#" data-toggle="modal" data-target="#modal_descartar">Descartar</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection