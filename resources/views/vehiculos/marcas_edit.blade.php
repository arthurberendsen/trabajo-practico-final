@section ('contenido')

<!-- INICIO MODALES -->

<div role="dialog" tabindex="-1" class="modal fade" id="modal_descartar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Descartar Cambios</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <p>¿Desea realmente descartar los cambios realizados?</p>
            </div>
            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">No</button><a class="btn btn-primary" role="button" href="{{ route('marcas_index') }}">Si</a></div>
        </div>
    </div>
</div>

<!-- FIN MODALES -->

<div class="container-fluid">

	@if($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger" role="alert">
		  		{{ $error }}
			</div>
		@endforeach
	@endif

    <h3 class="text-dark mb-4">Marcas</h3>
    <div class="row mb-3">
        <div class="col-lg-8 col-xl-8">
            <div class="row">
                <div class="col offset-xl-0">
                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <div class="form-row">
                                <p class="text-primary m-0 font-weight-bold col-xl-6">Modificación de Marcas</p>
                                <!-- Default switch -->
                                <div class="custom-control custom-switch col text-right">
                                    <form action="{{ route('marcas_update', $marca->marca_id) }}" method="POST">
                                    {{ csrf_field() }}
                                    
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                          
                                <div class="form-row">
                                    <div class="col-xl-12">
                                        <div class="form-group"><label for="nombre"><strong>Nombre</strong></label><input class="form-control @error('nombre') is-invalid @enderror" type="text" value="{{ $marca->nombre }}" name="nombre" />
                                             <span class="invalid-feedback" role="alert">
                                                <strong>Escriba una Marca</strong>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                  
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Guardar</button><a class="btn btn-danger ml-3" href="#" data-toggle="modal" data-target="#modal_descartar">Descartar</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection