@section ('contenido')

<!-- INICIO MODALES -->

<div role="dialog" tabindex="-1" class="modal fade" id="modal_descartar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Descartar Cambios</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <p>¿Desea realmente descartar los cambios realizados?</p>
            </div>
            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">No</button><a class="btn btn-primary" role="button" href="{{ route('vehiculos_index') }}">Si</a></div>
        </div>
    </div>
</div>

<!-- FIN MODALES -->

<div class="container-fluid">

	@if($errors->any())
		@foreach ($errors->all() as $error)
			<div class="alert alert-danger" role="alert">
		  		{{ $error }}
			</div>
		@endforeach
	@endif 

    <h3 class="text-dark mb-4">Vehículos</h3>
    <div class="row mb-3">
        <div class="col-lg-8 col-xl-8">
            <div class="row">
                <div class="col offset-xl-0">
                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <div class="form-row">
                                <p class="text-primary m-0 font-weight-bold col-xl-6">Modificación de Vehículos</p>
                                <!-- Default switch -->
                                <div class="custom-control custom-switch col text-right">
                                      <form action="{{ route('vehiculos_update', $vehiculo->vehiculo_id) }}" method="POST">
                                {{ csrf_field() }}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                          
                                <div class="form-row">
                                    <div class="col-xl-6 offset-xl-0">
                                        <div class="form-group">
                                            <label for="modelo_id"><strong>Modelo / Marca</strong></label>
                                            <select class="form-control @error('modelo_id') is-invalid @enderror " id="modelo" name="modelo_id">
                                                <option value="" selected>Elegir la Marca/Modelo...</option>
                                                @foreach($modelos as $modelo)
                                                <option value="{{ $modelo->modelo_id }}" @if ($vehiculo->modelo_id == $modelo->modelo_id) Selected @endif > {{ $modelo->marca }} - {{ $modelo->modelo }}</option>
                                                @endforeach
                                            </select>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Seleccione la Marca/Modelo</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 offset-xl-0">
                                        <div class="form-group"><label for="tipo_id"><strong>Tipo</strong></label><select class="form-control @error('tipo_id') is-invalid @enderror" id="tipo" name="tipo_id">
                                                <option value="" selected>Elegir el Tipo...</option>
                                            @foreach($tipos as $tipo)
                                                <option value="{{ $tipo->tipo_id }}" @if ($vehiculo->tipo_id == $tipo->tipo_id) Selected @endif >{{ $tipo->nombre }}</option>
                                            @endforeach
                                        </select>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Seleccione el Tipo de Vehículo</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 offset-xl-0">
                                        <div class="form-group"><label for="anho_fabricacion"><strong>Año</strong></label><input class="form-control @error('anho_fabricacion') is-invalid @enderror" type="number" id="anho_fabricacion" name="anho_fabricacion" min="1900" max="{{date('Y') + 10}}" step="1" value="{{ $vehiculo->anho_fabricacion }}">
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Seleccione el Año (1900-{{date('Y') + 10}})</strong>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-xl-5 offset-xl-0">
                                        <div class="form-group"><label for="chasis"><strong>Nro. de Chasis</strong></label><input class="form-control @error('chasis') is-invalid @enderror" type="text" id="chasis" name="chasis" value="{{ $vehiculo->chasis }}">
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Escriba el Número de Chasis</strong>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-xl-2 offset-xl-0">
                                        <div class="form-group"><label for="matricula"><strong>Matrícula</strong></label><input class="form-control" type="text" placeholder="" name="matricula" value="{{ $vehiculo->matricula }}" ></div>
                                    </div>
                                    <div class="col-xl-2 offset-xl-0">
                                        <div class="form-group"><label for="color"><strong>Color</strong></label><select class="form-control @error('color_id') is-invalid @enderror" id="color" name="color_id">
                                                <option value="" selected>Elegir el Color...</option>
                                                @foreach($colores as $color)
                                                <option value="{{ $color->color_id }}" @if ($vehiculo->color_id == $color->color_id) Selected @endif >{{ $color->nombre }}</option>
                                                @endforeach
                                            </select>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Seleccione un Color</strong>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-xl-3 offset-xl-0">
                                        <div class="form-group"><label for="transmision"><strong>Transmisión</strong><br /></label><select class="form-control @error('transmision') is-invalid @enderror" name="transmision">
                                            <option value="" selected>Elegir Transmisión...</option>
                                            <option value="M" @if ($vehiculo->transmision == 'M') Selected @endif>Manual</option>
                                            <option value="A" @if ($vehiculo->transmision == 'A') Selected @endif>Automático</option>
                                            <option value="S" @if ($vehiculo->transmision == 'S') Selected @endif>Secuencial</option>
                                            </select>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>Seleccione la Transmisión</strong>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Guardar</button><a class="btn btn-danger ml-3" href="#" data-toggle="modal" data-target="#modal_descartar">Descartar</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection