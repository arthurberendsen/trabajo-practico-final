@section ('contenido')

<div class="container-fluid">
    <h3 class="text-dark mb-4">Vehículos</h3>
    <div class="row mb-3">
        <div class="col-lg-8 col-xl-8">
            <div class="row">
                <div class="col offset-xl-0">
                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Datos del Vehículo</p>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-xl-6 offset-xl-0">
                                    <div class="form-group"><label for="marca_modelo"><strong>Marca / Modelo</strong></label><input readonly class="form-control" type="text" value="{{ $marca->nombre }} - {{ $modelo->nombre }}" name="marca_modelo" /></div>
                                </div>
                                <div class="col-xl-4 offset-xl-0">
                                    <div class="form-group"><label for="tipo"><strong>Tipo</strong></label><input readonly class="form-control" type="text"  value="{{ $tipo->nombre }}" name="tipo" /></div>
                                </div>
                                <div class="col-xl-2 offset-xl-0">
                                    <div class="form-group"><label for="anho_fabricacion"><strong>Año</strong></label><input readonly class="form-control" type="text" value="{{ $vehiculo->anho_fabricacion }}" name="anho_fabricacion" /></div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-5 offset-xl-0">
                                    <div class="form-group"><label for="chasis"><strong>Chasis</strong></label><input readonly class="form-control" type="text" value="{{ $vehiculo->chasis }}" name="chasis" /></div>
                                </div>
                                <div class="col-xl-2 offset-xl-0">
                                    <div class="form-group"><label for="direccion"><strong>Matrícula</strong></label><input readonly class="form-control" type="text" value="{{ $vehiculo->matricula }}" name="matricula" /></div>
                                </div>
                                <div class="col-xl-2 offset-xl-0">
                                    <div class="form-group"><label for="color"><strong>Color</strong></label><input readonly class="form-control" type="text" value="{{ $color->nombre }}" name="color" /></div>
                                </div>
                                <div class="col-xl-3 offset-xl-0">
                                    <div class="form-group"><label for="transmision"><strong>Transmisión</strong></label><input readonly class="form-control" type="text" value="@if ($vehiculo->transmision == 'M') Manual @elseif ($vehiculo->transmision == 'A') Automático @elseif ($vehiculo->transmision == 'S') Secuencial @endif" name="transmision" /></div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-5 offset-xl-0">
                                    <div class="form-group"><label for="deposito"><strong>Depósito</strong></label><input readonly class="form-control" type="text" value="{{ $deposito->nombre }}" name="deposito" /></div>
                                </div>
                                <div class="col-xl-4 offset-xl-0">
                                    <div class="form-group"><label for="costo_moneda"><strong>Costo / Moneda</strong></label><input readonly class="form-control" type="text" value="{{ number_format($vehiculo->costo, $decimals = 0 , $dec_point = "," , $thousands_sep = "." ) }} {{ $moneda->sigla }}" name="costo_moneda" /></div>
                                </div>
                                <div class="col-xl-3 offset-xl-0">
                                    <div class="form-group"><label for="enstock"><strong>En Stock @if ($vehiculo->estado == 1)<i class="far fa-check-circle" style="color:green"></i>@else<i class="far fa-times-circle" style="color:red"></i> @endif</strong></label><input readonly class="form-control" type="text" value="@if ($vehiculo->estado == 1) Si @else No @endif" name="enstock" /></div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <a class="btn btn-danger ml-3" href="{{ route('vehiculos_index') }}">Volver al Listado</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection