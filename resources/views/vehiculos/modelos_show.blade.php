@section ('contenido')

<div class="container-fluid">
    <h3 class="text-dark mb-4">Modelos</h3>
    <div class="row mb-3">
        <div class="col-lg-8 col-xl-8">
            <div class="row">
                <div class="col offset-xl-0">
                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Datos del Modelo</p>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-xl-6">
                                    <div class="form-group"><label for="nombre"><strong>Nombre</strong></label><input readonly class="form-control" type="text" value="{{ $modelo->nombre }}" name="nombre" /></div>
                                </div>
                                <div class="col">
                                    <div class="form-group"><label for="marca_nombre"><strong>Marca</strong></label><input readonly class="form-control" type="text"  value="{{ $marca->nombre }}" name="marca_nombre" /></div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <a class="btn btn-danger ml-3" href="{{ route('modelos_index') }}">Volver al Listado</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection