<nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
        <form class="form-inline d-none d-sm-inline-block mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search" autocomplete="off" method="POST" action="{{ route('main_autocomplete_filtrar') }}">
            {{ csrf_field() }}
            <div class="input-group">
                <input class="bg-light form-control border-1 small typeahead-main" type="search" id="main_buscar" name="main_buscar" placeholder="Buscar secciones..." />
                <div class="input-group-append"><button class="btn btn-primary py-0" type="submit"><i class="fas fa-search"></i></button></div>
            </div>
        </form>
        <ul class="nav navbar-nav flex-nowrap ml-auto">
            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-search"></i></a>
                <div class="dropdown-menu dropdown-menu-right p-3 animated--grow-in" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto navbar-search w-100">
                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ..." />
                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                        </div>
                    </form>
                </div>
            </li>
            <li class="nav-item dropdown no-arrow">
                <div class="modal fade" role="dialog" tabindex="-1" id="modalsalirdelsistema">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Salir del Sistema</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                            <div class="modal-body">
                                <p>¿Desea realmente salir del sistema?</p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-light" type="button" data-dismiss="modal">No</button>
                                <a class="btn btn-primary" role="button" href=" {{ route('logout') }} " onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Si</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="nav-item dropdown no-arrow" style="opacity: 1;border-width: 1px;">
                    <a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#" style="border-width: 1px;"><span class="d-none d-lg-inline mr-2 text-gray-600 small">{{ Auth::user()->nombre ?? 'Usuario Error'}} </span><img id="avatar" class="border rounded-circle img-profile" src="{{ asset(Auth::user()->avatar) }}" /></a>
                    <div class="dropdown-menu shadow dropdown-menu-right animated--grow-in">
                        <a class="dropdown-item" href="{{ route('perfil_index', Auth::user()->id) }}"><i class="fas fa-user fa-sm fa-fw mr-2 text-gray-600"></i>Perfil</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="" data-toggle="modal" data-target="#modalsalirdelsistema"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-600"></i> Salir</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>

<script type="text/javascript">

    var path = "{{ route('main_autocomplete') }}";

    $('input.typeahead-main').typeahead({
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });

</script>