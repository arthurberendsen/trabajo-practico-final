<nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0">
    <div class="container-fluid d-flex flex-column p-0">
        <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="{{ route('inicio') }}">
            <div class="sidebar-brand-icon rotate-n-30"><img src="{{ asset('public/bstudio/img/logo/MoustApp_logo_mini.jpg')}} " style="width: 48px;"></div>
            <div class="sidebar-brand-text mx-3"><span>MoustApp</span></div>
        </a>
        @php if (!isset($var)) { 
            $var = 'nada';
        }
        
        @endphp
        <hr class="sidebar-divider my-0">
        <ul class="nav navbar-nav text-light" id="accordionSidebar">
            <li class="nav-item">
                <a class="nav-link active" href="{{ route('inicio') }}"><i class="fas fa-tachometer-alt"></i><span>Inicio</span></a>
            </li>
            <li class="nav-item dropdown @if (Request::is('*vehiculos/*')) show @endif ">
                <a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-car-side"></i>Vehículos</a>
                <div class="dropdown-menu @if (Request::is('*vehiculos/*')) show @endif ">
                    <a class="dropdown-item @if (strpos($var, 'vehiculos_') === 0) active @endif " href="{{ route('vehiculos_index') }}">Vehículos</a>
                    <a class="dropdown-item @if (strpos($var, 'marcas_') === 0) active @endif " href="{{ route('marcas_index') }}">Marcas</a>
                    <a class="dropdown-item @if (strpos($var, 'modelos_') === 0) active @endif " href="{{ route('modelos_index') }}">Modelos</a>
                    <a class="dropdown-item @if (strpos($var, 'tipos_') === 0) active @endif " href="{{ route('tipos_index') }}">Tipos</a>
                    <a class="dropdown-item @if (strpos($var, 'colores_') === 0) active @endif " href="{{ route('colores_index') }}">Colores</a>
                </div>
            </li>
            <li class="nav-item dropdown @if (Request::is('*ventas/*')) show @endif ">
                <a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-file-invoice-dollar"></i>Ventas</a>
                <div class="dropdown-menu @if (Request::is('*ventas/*')) show @endif ">
                    <a class="dropdown-item @if (strpos($var, 'facturasv_') === 0) active @endif " href="{{ route('facturasv_index') }}">Facturas</a>
                    <a class="dropdown-item @if (strpos($var, 'clientes_') === 0) active @endif " href="{{ route('clientes_index') }}">Clientes</a>
                    <a class="dropdown-item @if (strpos($var, 'cuotasv_') === 0) active @endif " href="{{ route('cuotasv_index') }}">Cuotas</a>
                    <a class="dropdown-item @if (strpos($var, 'pagares_') === 0) active @endif " href="{{ route('pagares_index') }}">Pagarés</a>
                    <a class="dropdown-item @if (strpos($var, 'recibosv_') === 0) active @endif " href="{{ route('recibosv_index') }}">Recibos</a></div>
            </li>
            <li class="nav-item dropdown @if (Request::is('*compras/*')) show @endif ">
                <a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-money-bill-wave"></i>Compras</a>
                <div class="dropdown-menu @if (Request::is('*compras/*')) show @endif ">
                    <a class="dropdown-item @if (strpos($var, 'facturasc_') === 0) active @endif " href="{{ route('facturasc_index') }}">Facturas</a>
                    <a class="dropdown-item @if (strpos($var, 'proveedores_') === 0) active @endif " href="{{ route('proveedores_index') }}">Proveedores</a>
                    <a class="dropdown-item @if (strpos($var, 'cuotasc_') === 0) active @endif " href="{{ route('cuotasc_index') }}">Cuotas</a>
                    <a class="dropdown-item @if (strpos($var, 'recibosc_') === 0) active @endif " href="{{ route('recibosc_index') }}">Recibos</a></div>
            </li>
            <li class="nav-item dropdown @if (Request::is('*reportes/*')) show @endif ">
                <a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-chart-line"></i>Reportes</a>
                <div class="dropdown-menu @if (Request::is('*reportes/*')) show @endif ">
                    <a class="dropdown-item @if (strpos($var, 'r_vehiculos_') === 0) active @endif " href="{{ route('r_vehiculos_index') }}">Vehículos</a>
                    <a class="dropdown-item @if (strpos($var, 'r_ventas_') === 0) active @endif " href="{{ route('r_ventas_index') }}">Ventas</a>
                    <a class="dropdown-item @if (strpos($var, 'r_compras_') === 0) active @endif " href="{{ route('r_compras_index') }}">Compras</a>
                    <a class="dropdown-item @if (strpos($var, 'r_auditorias_') === 0) active @endif " href="{{ route('r_auditorias_index') }}">Auditorías</a></div>
            </li>
            <li class="nav-item dropdown @if (Request::is('*sistemas/*')) show @endif ">
                <a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-cogs"></i>Sistema</a>
                <div class="dropdown-menu @if (Request::is('*sistemas/*')) show @endif ">
                    <a class="dropdown-item @if (strpos($var, 'usuarios_') === 0) active @endif " href="{{ route('usuarios_index') }}">Usuarios</a>
                    <a class="dropdown-item @if (strpos($var, 'monedas_') === 0) active @endif " href="{{ route('monedas_index') }}">Monedas</a>
                    <a class="dropdown-item @if (strpos($var, 'depositos_') === 0) active @endif " href="{{ route('depositos_index') }}">Depósitos</a>
                    <a class="dropdown-item @if (strpos($var, 'ciudades') === 0) active @endif " href="{{ route('ciudades_index') }}">Ciudades</a>
                    <a class="dropdown-item @if (strpos($var, 'paises_') === 0) active @endif " href="{{ route('paises_index') }}">Países</a>
                </div>
            </li>
        </ul>
        <div class="text-center d-none d-md-inline"></div>
    </div>    
</nav>