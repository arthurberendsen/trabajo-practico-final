@section ('contenido')

<div class="container-fluid">
    <h3 class="text-dark mb-4">Mapa del Sitio</h3>
    <div class="card shadow">
        <div class="card-header py-3">
            <p class="text-primary m-0 font-weight-bold">Apartados del Sistema</p>
        </div>
        <div class="card-body">
            <div class="row mb-4">Si no está encontrando lo que busca, puede hacerlo directamente aquí, dando click sobre el apartado deseado.</div>
                @foreach ($apartados as $clave => $apartado)
                    <a href="{{ route($rutas[$clave]) }}">{{ $apartado }}</a> <br>
                @endforeach
            
        </div>
    </div>
</div>

@endsection