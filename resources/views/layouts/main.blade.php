<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Sistema MoustApp</title>
    <meta name="description" content="Sistema de compra-venta de vehiculos. Manejo de las cobranzas de los pagarés, cuotas y la gestión de clientes y vehículos.">
    <link rel="stylesheet" href="{{ asset('public/bstudio/bootstrap/css/bootstrap.min.css') }} ">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="{{ asset('public/bstudio/fonts/fontawesome-all.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('public/bstudio/fonts/material-icons.min.css') }} ">
    
    <!-- jQuery Librerias -->
    <!-- <script src="{{ asset('public/bstudio/js/jquery.min.js') }}"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="{{ asset('public/bstudio/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/bstudio/js/chart.min.js') }}"></script>
    <script src="{{ asset('public/bstudio/js/bs-init.js') }}"></script>
    <script src="{{ asset('public/bstudio/js/theme.js') }}"></script>

    <!-- DateJS Files -->
    <script src="{{ asset('public/dateJs/date.js') }}"></script>

    <!-- AutoNumeric Files -->
    <script src="{{ asset('public/autoNumeric/autoNumeric.js') }}"></script>

    <!-- Datepicker Files -->
    <link rel="stylesheet" href="{{ asset('public/datePicker/css/bootstrap-datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('public/datePicker/css/bootstrap-datepicker3.standalone.css') }}">
    <script src="{{ asset('public/datePicker/js/bootstrap-datepicker.js') }}"></script>
    
    <!-- Languaje -->
    <script src="{{ asset('public/datePicker/locales/bootstrap-datepicker.es.min.js') }}"></script>
    
    <!-- Twitter TypeAhead.js -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js" integrity="sha512-HWlJyU4ut5HkEj0QsK/IxBCY55n5ZpskyjVlAoV9Z7XQwwkqXoYdCIC93/htL3Gu5H3R4an/S0h2NXfbZk3g7w==" crossorigin="anonymous"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script> -->
</head>

<body id="page-top">
    <div id="wrapper">
        <!-- Codigo comienza aca -->
        @include ('layouts.leftbar')

        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                
                @include ('layouts.navbar')
                
                <div class="container-fluid">
                
                    @yield('contenido')
                                        
                </div>
            </div>
    
            @include ('layouts.footer')
        </div>

        <!-- Codigo termina aca -->
        <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>

</body>
</html>