<footer class="bg-white sticky-footer">
    <div class="container my-auto">
        <div class="text-center my-auto copyright"><span>Copyright © MoustApp 2020</span></div>
    </div>
</footer>

<script>
    $('button[name=limpiar]').click(function() {
        $(this).closest('form').find("input[type=text], input[type=number], input[type=email], input[type=password], textarea, select").val("");
    });
</script>