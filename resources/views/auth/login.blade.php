<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Login - MoustApp</title>
    <meta name="description" content="Sistema de compra-venta de vehiculos. Manejo de las cobranzas de los pagarés, cuotas y la gestión de clientes y vehículos.">
    <link rel="stylesheet" href="{{ asset('public/bstudio/bootstrap/css/bootstrap.min.css') }} ">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="{{ asset('public/bstudio/fonts/fontawesome-all.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('public/bstudio/fonts/material-icons.min.css') }} ">
</head>

<body class="bg-gradient-primary" style="background: #a8cee2;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-lg-12 col-xl-10">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-flex" style="height: 326px;">
                                <div class="flex-grow-1 bg-login-image" style="background: url('{{ asset('public/bstudio/img/logo/Artboard 1-100_login.jpg')}}') top / contain no-repeat;margin: 0px;padding: 0px;width: 468px;margin-top: 2px;margin-left: 2px;height: 323px;"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="p-4">
                                    <div class="text-center">
                                        <h4 class="text-dark mb-4" style="font-size: 24px;"><strong>Bienvenido a MoustApp</strong></h4>
                                    </div>

                                    <form class="user" method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="form-group">
                                            <input class="form-control form-control-user @error('email') is-invalid @enderror" type="email" id="email" aria-describedby="emailHelp" placeholder="Ingrese su correo" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control form-control-user @error('password') is-invalid @enderror" type="password" id="password" placeholder="Contraseña" name="password" required autocomplete="current-password">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group ml-3">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                <label class="form-check-label" for="remember">
                                                    Recordar Contraseña
                                                </label>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block text-white btn-user" type="submit" style="height: 45px;">Iniciar Sesión</button>
                                    </form>
                                    
                                    <div class="text-center"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('public/bstudio/js/jquery.min.js') }}"></script>
    <script src="{{ asset('public/bstudio/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/bstudio/js/chart.min.js') }}"></script>
    <script src="{{ asset('public/bstudio/js/bs-init.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="{{ asset('public/bstudio/js/theme.js') }}"></script>
</body>

</html>