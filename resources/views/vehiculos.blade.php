@extends ('layouts.main')

@section ('contenido')

    // VEHICULOS
    @if ($var == 'vehiculos_index')
        @include ('vehiculos.vehiculos_index')
    @endif

    @if ($var == 'vehiculos_show')
        @include ('vehiculos.vehiculos_show')
    @endif

    @if ($var == 'vehiculos_create')
        @include ('vehiculos.vehiculos_create')
    @endif

    @if ($var == 'vehiculos_edit')
        @include ('vehiculos.vehiculos_edit')
    @endif

    // MARCAS
    @if ($var == 'marcas_index')
        @include ('vehiculos.marcas_index')
    @endif

    @if ($var == 'marcas_show')
        @include ('vehiculos.marcas_show')
    @endif

    @if ($var == 'marcas_create')
        @include ('vehiculos.marcas_create')
    @endif

    @if ($var == 'marcas_edit')
        @include ('vehiculos.marcas_edit')
    @endif

    // MODELOS
    @if ($var == 'modelos_index')
        @include ('vehiculos.modelos_index')
    @endif

    @if ($var == 'modelos_show')
        @include ('vehiculos.modelos_show')
    @endif

    @if ($var == 'modelos_create')
        @include ('vehiculos.modelos_create')
    @endif

    @if ($var == 'modelos_edit')
        @include ('vehiculos.modelos_edit')
    @endif

    // TIPOS
    @if ($var == 'tipos_index')
        @include ('vehiculos.tipos_index')
    @endif

    @if ($var == 'tipos_show')
        @include ('vehiculos.tipos_show')
    @endif

    @if ($var == 'tipos_create')
        @include ('vehiculos.tipos_create')
    @endif

    @if ($var == 'tipos_edit')
        @include ('vehiculos.tipos_edit')
    @endif

    // COLORES
    @if ($var == 'colores_index')
        @include ('vehiculos.colores_index')
    @endif

    @if ($var == 'colores_show')
        @include ('vehiculos.colores_show')
    @endif

    @if ($var == 'colores_create')
        @include ('vehiculos.colores_create')
    @endif

    @if ($var == 'colores_edit')
        @include ('vehiculos.colores_edit')
    @endif
@endsection