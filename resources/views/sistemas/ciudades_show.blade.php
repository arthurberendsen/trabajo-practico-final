@section ('contenido')

<div class="container-fluid">
    <h3 class="text-dark mb-4">Ciudades</h3>
    <div class="row mb-3">
        <div class="col-lg-8 col-xl-8">
            <div class="row">
                <div class="col offset-xl-0">
                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Datos de la Ciudad</p>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                    <div class="col-xl-6">
                                        <div class="form-group"><label for="nombre"><strong>Nombre</strong></label><input class="form-control" readonly type="text" value="{{ $ciudad->nombre }}" name="nombre" />
                                        </div>
                                    </div>
                                    <div class="col-xl-6">
                                        <div class="form-group"><label for="pais"><strong>País</strong></label><input class="form-control" readonly type="text" value="{{ $pais->nombre }}" name="pais" />
                                        </div>
                                    </div>
                                </div>
                            <div class="form-group">
                                <a class="btn btn-danger ml-3" href="{{ route('ciudades_index') }}">Volver al Listado</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection