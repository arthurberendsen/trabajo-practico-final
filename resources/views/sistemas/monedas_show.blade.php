@section ('contenido')

<div class="container-fluid">
    <h3 class="text-dark mb-4">Monedas</h3>
    <div class="row mb-3">
        <div class="col-lg-8 col-xl-8">
            <div class="row">
                <div class="col offset-xl-0">
                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Datos de la Moneda</p>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-xl-6">
                                    <div class="form-group"><label for="nombre"><strong>Nombre</strong></label><input class="form-control" readonly type="text" value="{{ $moneda->nombre }}" name="nombre" />
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="form-group"><label for="estado"><strong>Estado @if ($moneda->estado == 1)<i class="far fa-check-circle" style="color:green"></i>@else<i class="far fa-times-circle" style="color:red"></i> @endif</strong></label><input class="form-control" readonly type="text" value="@if ($moneda->estado == 1) Activo @else Inactivo @endif" name="estado" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-4">
                                    <div class="form-group"><label for="cotizacion"><strong>Cotización</strong></label><input class="form-control" readonly type="text" value="{{ $moneda->cotizacion }}" name="cotizacion" />
                                    </div>
                                </div>
                                <div class="col-xl-4">
                                    <div class="form-group"><label for="sigla"><strong>Sigla</strong></label><input class="form-control" readonly type="text" value="{{ $moneda->sigla }}" name="sigla" />
                                    </div>
                                </div>
                                <div class="col-xl-4">
                                    <div class="form-group"><label for="decimales"><strong>Decimales</strong></label><input class="form-control" readonly type="text" value="{{ $moneda->decimales }}" name="decimales" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <a class="btn btn-danger ml-3" href="{{ route('monedas_index') }}">Volver al Listado</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection