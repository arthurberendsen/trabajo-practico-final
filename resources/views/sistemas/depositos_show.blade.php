@section ('contenido')

<div class="container-fluid">
    <h3 class="text-dark mb-4">Depósitos</h3>
    <div class="row mb-3">
        <div class="col-lg-8 col-xl-8">
            <div class="row">
                <div class="col offset-xl-0">
                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <div class="form-row">
                                <p class="text-primary m-0 font-weight-bold col-xl-6">Datos del Depósito</p>
                                <!-- <div class="custom-control custom-switch col text-right">
                                    <input type="checkbox" class="form-control custom-control-input" id="customSwitches" name="estado" @if ($deposito->estado == True) checked @endif>
                                    <label class="custom-control-label" for="customSwitches">Estado</label>
                                </div>
                                -->
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-row">
                                    <div class="col-xl-8">
                                        <div class="form-group"><label for="nombre"><strong>Nombre</strong></label><input class="form-control" readonly type="text" value="{{ $deposito->nombre }}" name="nombre" />
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="form-group"><label for="estado"><strong>Estado @if ($deposito->estado == 1)<i class="far fa-check-circle" style="color:green"></i>@else<i class="far fa-times-circle" style="color:red"></i> @endif</strong></label><input class="form-control" readonly type="text" value="@if ($deposito->estado == 1) Activo @else Inactivo @endif" name="estado" />
                                        </div>
                                    </div>
                                </div>
                            <div class="form-group">
                                <a class="btn btn-danger ml-3" href="{{ route('depositos_index') }}">Volver al Listado</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection