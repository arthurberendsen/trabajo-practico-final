@section ('contenido')

<div class="container-fluid">
    <h3 class="text-dark mb-4">Usuarios</h3>
    <div class="row mb-3">
        <div class="col-lg-8 col-xl-8">
            <div class="row">
                <div class="col offset-xl-0">
                    <div class="card shadow mb-3" style="width: 95%;">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Datos del Usuario</p>
                        </div>
                        <div class="row card-body">
                            <div class="col-lg-4">
                                <div class="card mb-3">
                                    <div class="card-body text-center shadow">
                                        <img id="file-image" src="{{asset($usuario->avatar)}}" alt="Preview" class="hidden rounded-circle mb-3 mt-4 img-thumbnail" width="160" height="160">
                                        <figcaption class="figure-caption">Avatar del Usuario</figcaption>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="col">
                                        <div class="col-xl-12">
                                            <div class="form-group"><label for="nombre"><strong>Nombre</strong></label><input class="form-control" readonly type="text" value="{{ $usuario->nombre }}" name="nombre" />
                                            </div>
                                        </div>
                                        <div class="col-xl-12">
                                            <div class="form-group"><label for="pais"><strong>Email</strong></label><input class="form-control" readonly type="text" value="{{ $usuario->email }}" name="pais" />
                                            </div>
                                        </div>
                                        <div class="col-xl-12">
                                            <div class="form-group"><label for="estado"><strong>Estado @if ($usuario->estado == 1)<i class="far fa-check-circle" style="color:green"></i>@else<i class="far fa-times-circle" style="color:red"></i> @endif</strong></label><input class="form-control" readonly type="text" value="@if ($usuario->estado == 1) Activo @else Inactivo @endif" name="estado" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="form-group">
                                <a class="btn btn-danger ml-3" href="{{ route('usuarios_index') }}">Volver al Listado</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection