<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Sistema MoustApp</title>
    <meta name="description" content="Sistema de compra-venta de vehiculos. Manejo de las cobranzas de los pagarés, cuotas y la gestión de clientes y vehículos.">
    <link rel="stylesheet" href="{{ asset('public/bstudio/bootstrap/css/bootstrap.min.css') }} ">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="{{ asset('public/bstudio/fonts/fontawesome-all.min.css') }} ">
    <link rel="stylesheet" href="{{ asset('public/bstudio/fonts/material-icons.min.css') }} ">
</head>

<body id="page-top">
    <div id="wrapper">
        <!-- Codigo comienza aca -->
        @php $left='none'; $var='none'; @endphp
         <!--  @include ('layouts.leftbar') -->

        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                
                <div class="container-fluid">

					<div class="text-center mt-5">
						<div class="error mx-auto" data-text="404"><p class="m-0">404</p></div>
					    <p class="text-dark mb-5 lead">Page Not Found</p>
					    <p class="text-black-50 mb-0">Parece que has encontrado una inconsistencia en la matrix...</p>
					    <a href="{{ route('inicio') }}">← Volver al Inicio</a>
					</div>
     			</div>
            </div>
    
            @include ('layouts.footer')
        </div>

        <!-- Codigo termina aca -->
        <a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>
    
    <script src="{{ asset('public/bstudio/js/jquery.min.js') }}"></script>
    <script src="{{ asset('public/bstudio/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/bstudio/js/chart.min.js') }}"></script>
    <script src="{{ asset('public/bstudio/js/bs-init.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="{{ asset('public/bstudio/js/theme.js') }}"></script>
</body>

</html>