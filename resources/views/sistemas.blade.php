@extends ('layouts.main')

@section ('contenido')

    // USUARIOS
    @if ($var == 'usuarios_index')
        @include ('sistemas.usuarios_index')
    @endif

    @if ($var == 'usuarios_show')
        @include ('sistemas.usuarios_show')
    @endif

    @if ($var == 'usuarios_create')
        @include ('sistemas.usuarios_create')
    @endif

    @if ($var == 'usuarios_edit')
        @include ('sistemas.usuarios_edit')
    @endif

    // MONEDAS
    @if ($var == 'monedas_index')
        @include ('sistemas.monedas_index')
    @endif

    @if ($var == 'monedas_show')
        @include ('sistemas.monedas_show')
    @endif

    @if ($var == 'monedas_create')
        @include ('sistemas.monedas_create')
    @endif

    @if ($var == 'monedas_edit')
        @include ('sistemas.monedas_edit')
    @endif

    // DEPOSITOS
    @if ($var == 'depositos_index')
        @include ('sistemas.depositos_index')
    @endif

    @if ($var == 'depositos_show')
        @include ('sistemas.depositos_show')
    @endif

    @if ($var == 'depositos_create')
        @include ('sistemas.depositos_create')
    @endif

    @if ($var == 'depositos_edit')
        @include ('sistemas.depositos_edit')
    @endif

    // CIUDADES
    @if ($var == 'ciudades_index')
        @include ('sistemas.ciudades_index')
    @endif

    @if ($var == 'ciudades_show')
        @include ('sistemas.ciudades_show')
    @endif

    @if ($var == 'ciudades_create')
        @include ('sistemas.ciudades_create')
    @endif

    @if ($var == 'ciudades_edit')
        @include ('sistemas.ciudades_edit')
    @endif

    // PAISES
    @if ($var == 'paises_index')
        @include ('sistemas.paises_index')
    @endif

    @if ($var == 'paises_show')
        @include ('sistemas.paises_show')
    @endif

    @if ($var == 'paises_create')
        @include ('sistemas.paises_create')
    @endif

    @if ($var == 'paises_edit')
        @include ('sistemas.paises_edit')
    @endif
@endsection