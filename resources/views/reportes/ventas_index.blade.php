@section ('contenido')

<div class="container-fluid">
    @if (session('successMessage'))
        <div class="alert alert-success" role="alert">
           {{ session('successMessage') }}
        </div>
    @endif

    @if (session('errorMessage'))
        <div class="alert alert-danger" role="alert">
           {{ session('errorMessage') }}
        </div>
    @endif

    <h3 class="text-dark mb-4">Reporte de Ventas</h3>
    <div class="card shadow">
        <div class="card-header py-3">
            <div class="row">
                <h4 class="col-3 text-primary m-2 font-weight-bold">Filtrar Reporte</h4>  

                <div class="col-4 text-md-right text-nowrap">
                    <a class="btn btn-sm btn-outline-danger ml-3" href="#" name="exportar_pdf" id="exportar_pdf"><i class="far fa-file-pdf" style="font-size: 14px;"></i> Exportar</a>
                    <a class="btn btn-sm btn-outline-success ml-3" href="#" name="exportar_xls" id="exportar_xls"><i class="far fa-file-excel" style="font-size: 14px;"></i> Exportar</a>
                </div>  
                <div class="col-2 text-md-right text-nowrap">
                    <label>Ordenar por  <select class="form-control form-control-sm custom-select custom-select-sm" id="sort_by" name="sort_by" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if($sort_by = '') selected @endif>Elegir Campo...</option>
                                <option value="factura_nro" @if($sort_sel == 'factura_nro') selected @endif>Nro. Factura</option>
                                <option value="cliente" @if($sort_sel == 'cliente') selected @endif>Cliente</option>
                                <option value="vehiculo" @if($sort_sel == 'vehiculo') selected @endif>Vehículo</option>
                                <option value="forma_pago" @if($sort_sel == 'forma_pago') selected @endif>Forma de Pago</option>
                                <option value="estado" @if($sort_sel == 'estado') selected @endif>Estado</option>
                            </select>
                    </label>
                </div>
                <div class="col text-md-right text-nowrap">
                    <label>Mostrar Items  <select class="form-control form-control-sm custom-select custom-select-sm" id="pagination" name="items" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="10" @if($items == 10) selected @endif>10</option>
                                <option value="25" @if($items == 25) selected @endif>25</option>
                                <option value="50" @if($items == 50) selected @endif>50</option>
                                <option value="100" @if($items == 100) selected @endif>100</option>
                            </select>
                    </label>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">
                    <div id="dataTable_length" class="dataTables_length text-nowrap" aria-controls="dataTable">
                        <label>Cliente  
                            <select class="form-control form-control-sm custom-select custom-select-sm" id="cliente_id" name="cliente_id" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if ($cliente_id = '') selected @endif>Filtrar por cliente...</option>
                                @foreach($clientes as $cliente)
                                    <option value="{{ $cliente->cliente_id }}" @if($cliente_sel == $cliente->cliente_id) selected @endif>{{ $cliente->razon_social }}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                </div>
                <div class="col-2">
                    <div id="dataTable_length" class="dataTables_length text-nowrap" aria-controls="dataTable">
                        <label>Vehículo  
                            <select class="form-control form-control-sm custom-select custom-select-sm" id="vehiculo_id" name="vehiculo_id" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if ($vehiculo_id = '') selected @endif>Filtrar por vehículo...</option>
                                @foreach($vehiculos as $vehiculo)
                                    <option value="{{ $vehiculo->nombre }}"  @if($vehiculo_sel == $vehiculo->nombre) selected @endif>{{ $vehiculo->nombre }}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                </div>
                <div class="col-3">
                    <form class="form-inline">
                        <div class="form-group"><label for="fecha_factura">Fecha&nbsp;&nbsp;</label>
                            <input type="text" class="form-control form-control-sm datepicker" name="fecha_desde" id="fecha_desde" value="@if($fecha_desde_sel != '') {{ $fecha_desde_sel }} @endif" style="font-size: 11px; height: 24px; width: 140px;">&nbsp; y &nbsp;
                            <input type="text" class="form-control form-control-sm datepicker" name="fecha_hasta" id="fecha_hasta"value="@if($fecha_hasta_sel != '') {{ $fecha_hasta_sel }} @endif" style="font-size: 11px; height: 24px; width: 140px;">
                        </div>
                    </form>
                </div>

                <div class="col">
                    <div id="dataTable_length" class="dataTables_length text-nowrap" aria-controls="dataTable">
                        <label>Forma de Pago  
                            <select class="form-control form-control-sm custom-select custom-select-sm" id="forma_pago_id" name="forma_pago_id" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if ($forma_pago_id = '') selected @endif>Filtrar por Forma de Pago...</option>
                                @foreach($formas_pagos as $forma_pago)
                                    <option value="{{ $forma_pago->nombre }}"  @if($forma_pago_sel == $forma_pago->nombre) selected @endif>{{ $forma_pago->nombre }}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                </div>

                <div class="col">
                    <div id="dataTable_length" class="dataTables_length text-nowrap" aria-controls="dataTable">
                        <label>Estado  
                            <select class="form-control form-control-sm custom-select custom-select-sm" id="estado_id" name="estado_id" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if ($estado_id = '') selected @endif>Filtrar por Estado...</option>
                                <option value="Activo" @if($estado_sel == "Activo") selected @endif>Activo</option>
                                <option value="Anulado" @if($estado_sel == "Anulado") selected @endif>Anulado</option>
                            </select>
                        </label>
                    </div>
                </div>

            </div>
            <div class="table-responsive table mt-2 table-striped" id="dataTable" role="grid" aria-describedby="dataTable_info">
                <table class="table my-0 table-hover" id="dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Fac. Nro</th>
                            <th>Fecha</th>
                            <th>Cliente</th>
                            <th>Vehículo</th>
                            <th>Precio</th>
                            <th>Forma Pago</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = $ventas->firstItem(); ?>

                        @foreach($ventas as $venta)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $venta->nro }}</td>
                            <td>{{ date("d/m/Y", strtotime($venta->fecha)) }}</td>
                            <td>{{ $venta->cliente }}</td>
                            <td>{{ $venta->vehiculo }}</td>
                            <td>@switch( $venta->sigla )
                                        @case('Gs')
                                            {{ number_format($venta->precio, $decimals = 0 , $dec_point = "," , $thousands_sep = "." ) }}
                                            @break

                                        @default
                                            {{ number_format($venta->precio, $decimals = 2 , $dec_point = "," , $thousands_sep = "." ) }}
                                    @endswitch
                                    {{ $venta->sigla }}
                            </td>
                            <td>{{ $venta->forma_pago }}</td>
                            <td>@if ($venta->estado == 1) Activo <i class="far fa-check-circle" style="color:green"></i>@else Anulado <i class="far fa-times-circle" style="color:red"></i> @endif</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><strong>#</strong></td>
                            <td><strong>Fac. Nro</strong></td>
                            <td><strong>Fecha</strong></td>
                            <td><strong>Cliente</strong></td>
                            <td><strong>Vehículo</strong></td>
                            <td><strong>Precio</strong></td>
                            <td><strong>Forma de Pago</strong></td>
                            <td><strong>Estado</strong></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
            <div class="row">
                <div class="col-md-6 align-self-center">
                    <p id="dataTable_info" class="dataTables_info" role="status" aria-live="polite">Mostrando el registro {{$ventas->firstItem() ?? 0 }} al {{$ventas->lastItem() ?? 0 }} de {{$ventas->total()}} registros</p>

                </div>

                <div class="col-md-6">
                    <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                        {{ $ventas->appends(compact('items'))->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function generarURL() { //Funcion para capturar los valores de todos los select's

        var Items = $('select[name=items]').val();
        var ClienteId = $('select[name=cliente_id]').val();
        var VehiculoId = $('select[name=vehiculo_id]').val();
        var EstadoId = $('select[name=estado_id]').val();
        var FechaDesde = $('input[name=fecha_desde]').val();
        var FechaHasta = $('input[name=fecha_hasta]').val();
        var FormaPagoId = $('select[name=forma_pago_id]').val();

        var SortBy = $('select[name=sort_by]').val();

        var URL = "";

        if($.trim(Items).length) {
            URL += "?items=" + Items; }
        
        if($.trim(ClienteId).length) {
            URL += "&cliente_id=" + ClienteId; }

        if($.trim(VehiculoId).length) {
            URL += "&vehiculo_id=" + VehiculoId; }

        if($.trim(EstadoId).length) {
            URL += "&estado_id=" + EstadoId; }

        if($.trim(FechaDesde).length) {
            URL += "&fecha_desde=" + FechaDesde; }
        
        if($.trim(FechaHasta).length) {
            URL += "&fecha_hasta=" + FechaHasta; }

         if($.trim(FormaPagoId).length) {
            URL += "&forma_pago_id=" + FormaPagoId; }

        
        if($.trim(SortBy).length) {
            URL += "&sort_by=" + SortBy; }

        return URL;   // The function returns the product of p1 and p2
    };

    $('select[name=items], select[name=cliente_id], select[name=vehiculo_id], select[name=estado_id], select[name=sort_by], input[name=fecha_desde], input[name=fecha_hasta], select[name=forma_pago_id]').change(function() {
        
        var URL_generado = generarURL();

        // Redirecciona las urls para filtrar
        window.location = "{{ URL::route('r_ventas_index') }}" + URL_generado;
    });

    $('a[name=exportar_xls]').click(function() {
        
        var URL_generado = generarURL();

        // Redirecciona las urls para filtrar
        window.location = "{{ URL::route('r_ventas_xls') }}" + URL_generado;
    });

    $('a[name=exportar_pdf]').click(function() {
        
        var URL_generado = generarURL();

        // Redirecciona las urls para filtrar
        window.location = "{{ URL::route('r_ventas_pdf') }}" + URL_generado;
    });

    $('.datepicker').datepicker({
        clearBtn: true,
        language: "es",
        autoclose: true,
        todayHighlight: true
    });

</script>

@endsection