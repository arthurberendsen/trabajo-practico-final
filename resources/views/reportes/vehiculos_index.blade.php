@section ('contenido')

<div class="container-fluid">
    @if (session('successMessage'))
        <div class="alert alert-success" role="alert">
           {{ session('successMessage') }}
        </div>
    @endif

    @if (session('errorMessage'))
        <div class="alert alert-danger" role="alert">
           {{ session('errorMessage') }}
        </div>
    @endif

    <h3 class="text-dark mb-4">Reporte de Vehículos</h3>
    <div class="card shadow">
        <div class="card-header py-3">
            <div class="row">
                <h4 class="col-3 text-primary m-2 font-weight-bold">Filtrar Reporte</h4>  

                <div class="col-4 text-md-right text-nowrap">
                    <a class="btn btn-sm btn-outline-danger ml-3" href="#" name="exportar_pdf" id="exportar_pdf"><i class="far fa-file-pdf" style="font-size: 14px;"></i> Exportar</a>
                    <a class="btn btn-sm btn-outline-success ml-3" href="#" name="exportar_xls" id="exportar_xls"><i class="far fa-file-excel" style="font-size: 14px;"></i> Exportar</a>
                </div>  
                <div class="col-2 text-md-right text-nowrap">
                    <label>Ordenar por  <select class="form-control form-control-sm custom-select custom-select-sm" id="sort_by" name="sort_by" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if($sort_by = '') selected @endif>Elegir Campo...</option>
                                <option value="marca" @if($sort_sel == 'marca') selected @endif>Marca</option>
                                <option value="modelo" @if($sort_sel == 'modelo') selected @endif>Modelo</option>
                                <option value="tipo" @if($sort_sel == 'tipo') selected @endif>Tipo</option>
                                <option value="color" @if($sort_sel == 'color') selected @endif>Color</option>
                                <option value="transmision" @if($sort_sel == 'transmision') selected @endif>Transmision</option>
                                <option value="anho" @if($sort_sel == 'anho') selected @endif>Año</option>
                                <option value="deposito" @if($sort_sel == 'deposito') selected @endif>Deposito</option>
                            </select>
                    </label>
                </div>
                <div class="col text-md-right text-nowrap">
                    <label>Mostrar Items  <select class="form-control form-control-sm custom-select custom-select-sm" id="pagination" name="items" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="10" @if($items == 10) selected @endif>10</option>
                                <option value="25" @if($items == 25) selected @endif>25</option>
                                <option value="50" @if($items == 50) selected @endif>50</option>
                                <option value="100" @if($items == 100) selected @endif>100</option>
                            </select>
                    </label>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">
                    <div id="dataTable_length" class="dataTables_length text-nowrap" aria-controls="dataTable">
                        <label>Marca  
                            <select class="form-control form-control-sm custom-select custom-select-sm" id="marca_id" name="marca_id" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if ($marca_id = '') selected @endif>Filtrar por marca...</option>
                                @foreach($marcas as $marca)
                                    <option value="{{ $marca->marca_id }}" @if($marca_sel == $marca->marca_id) selected @endif>{{ $marca->nombre }}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                </div>
                <div class="col-2">
                    <div id="dataTable_length" class="dataTables_length text-nowrap" aria-controls="dataTable">
                        <label>Tipo  
                            <select class="form-control form-control-sm custom-select custom-select-sm" id="tipo_id" name="tipo_id" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if ($tipo_id = '') selected @endif>Filtrar por tipo...</option>
                                @foreach($tipos as $tipo)
                                    <option value="{{ $tipo->tipo_id }}" @if($tipo_sel == $tipo->tipo_id) selected @endif>{{ $tipo->nombre }}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                </div>
                <div class="col-2">
                    <div id="dataTable_length" class="dataTables_length text-nowrap" aria-controls="dataTable">
                        <label>Color  
                            <select class="form-control form-control-sm custom-select custom-select-sm" id="color_id" name="color_id" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if ($color_id = '') selected @endif>Filtrar por color...</option>
                                @foreach($colores as $color)
                                    <option value="{{ $color->color_id }}" @if($color_sel == $color->color_id) selected @endif>{{ $color->nombre }}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                </div>

                <div class="col-2">
                    <div id="dataTable_length" class="dataTables_length text-nowrap" aria-controls="dataTable">
                        <label>Año  
                            <select class="form-control form-control-sm custom-select custom-select-sm" id="anho_id" name="anho_id" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if ($anho_id = '') selected @endif>Filtrar por año...</option>
                                @foreach($anhos as $anho)
                                    <option value="{{ $anho->anho_nro }}" @if($anho_sel == $anho->anho_nro) selected @endif>{{ $anho->anho_nro }}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                </div>
                <div class="col-2">
                    <div id="dataTable_length" class="dataTables_length text-nowrap" aria-controls="dataTable">
                        <label>Depósito  
                            <select class="form-control form-control-sm custom-select custom-select-sm" id="deposito_id" name="deposito_id" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if ($deposito_id = '') selected @endif>Filtrar por depósito...</option>
                                @foreach($depositos as $deposito)
                                    <option value="{{ $deposito->deposito_id }}" @if($deposito_sel == $deposito->deposito_id) selected @endif>{{ $deposito->nombre }}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                </div>
                <div class="col">
                    <div id="dataTable_length" class="dataTables_length text-nowrap" aria-controls="dataTable">
                        <label>Estado  
                            <select class="form-control form-control-sm custom-select custom-select-sm" id="disponibilidad_id" name="disponibilidad_id" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if ($disponibilidad_id = '') selected @endif>Filtrar por Estado...</option>
                                <option value="Disponible" @if($disponibilidad_sel == "Disponible") selected @endif>Disponible</option>
                                <option value="Vendido" @if($disponibilidad_sel == "Vendido") selected @endif>Vendido</option>
                                <option value="Inactivo" @if($disponibilidad_sel == "Inactivo") selected @endif>Inactivo</option>
                            </select>
                        </label>
                    </div>
                </div>

            </div>
            <div class="table-responsive table mt-2 table-striped" id="dataTable" role="grid" aria-describedby="dataTable_info">
                <table class="table my-0 table-hover" id="dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Tipo</th>
                            <th>Color</th>
                            <th>Transmisión</th>
                            <th>Año</th>
                            <th>Depósito</th>
                            <th>Costo</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = $vehiculos->firstItem(); ?>

                        @foreach($vehiculos as $vehiculo)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $vehiculo->marca }}</td>
                            <td>{{ $vehiculo->modelo }}</td>
                            <td>{{ $vehiculo->tipo }}</td>
                            <td>{{ $vehiculo->color }}</td>
                            <td>@switch( $vehiculo->transmision )
                                    @case('A') Automática @break
                                    @case('S') Secuencial @break
                                    @case('M') Manual @break
                                @endswitch
                            </td>
                            <td>{{ $vehiculo->anho }}</td>
                            <td>{{ $vehiculo->deposito }}</td>
                            <td>@isset( $vehiculo->costo )
                                    @switch( $vehiculo->sigla )
                                        @case('Gs')
                                            {{ number_format($vehiculo->costo, $decimals = 0 , $dec_point = "," , $thousands_sep = "." ) }}
                                            @break

                                        @default
                                            {{ number_format($vehiculo->costo, $decimals = 2 , $dec_point = "," , $thousands_sep = "." ) }}
                                    @endswitch
                                    {{ $vehiculo->sigla }}
                                @endisset
                            </td>
                            <td>@if ( $vehiculo->disponibilidad == "Disponible") {{$vehiculo->disponibilidad}} <i class="far fa-check-circle" style="color:green"></i>  @elseif ( $vehiculo->disponibilidad == "Vendido") {{$vehiculo->disponibilidad}} <i class="far fa-times-circle" style="color:red"></i>  @else {{$vehiculo->disponibilidad}} <i class="fas fa-ban" style="color:black"></i> @endif</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><strong>#</strong></td>
                            <td><strong>Marca</strong></td>
                            <td><strong>Modelo</strong></td>
                            <td><strong>Tipo</strong></td>
                            <td><strong>Color</strong></td>
                            <td><strong>Transmisión</strong></td>
                            <td><strong>Año</strong></td>
                            <td><strong>Depósito</strong></td>
                            <td><strong>Costo</strong></td>
                            <td><strong>Estado</strong></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
            <div class="row">
                <div class="col-md-6 align-self-center">
                    <p id="dataTable_info" class="dataTables_info" role="status" aria-live="polite">Mostrando el registro {{$vehiculos->firstItem() ?? 0 }} al {{$vehiculos->lastItem() ?? 0 }} de {{$vehiculos->total()}} registros</p>

                </div>

                <div class="col-md-6">
                    <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                        {{ $vehiculos->appends(compact('items'))->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function generarURL() { //Funcion para capturar los valores de todos los select's

        var Items = $('select[name=items]').val();
        var MarcaId = $('select[name=marca_id]').val();
        var TipoId = $('select[name=tipo_id]').val();
        var ColorId = $('select[name=color_id]').val();
        var AnhoId = $('select[name=anho_id]').val();
        var DepositoId = $('select[name=deposito_id]').val();
        var DisponibilidadId = $('select[name=disponibilidad_id]').val();
        var SortBy = $('select[name=sort_by]').val();

        var URL = "";

        if($.trim(Items).length) {
            URL += "?items=" + Items; }
        
        if($.trim(MarcaId).length) {
            URL += "&marca_id=" + MarcaId; }

        if($.trim(TipoId).length) {
            URL += "&tipo_id=" + TipoId; }

        if($.trim(ColorId).length) {
            URL += "&color_id=" + ColorId; }

        if($.trim(AnhoId).length) {
            URL += "&anho_id=" + AnhoId; }

        if($.trim(DepositoId).length) {
            URL += "&deposito_id=" + DepositoId; }

        if($.trim(DisponibilidadId).length) {
            URL += "&disponibilidad_id=" + DisponibilidadId; }
        
        if($.trim(SortBy).length) {
            URL += "&sort_by=" + SortBy; }

        return URL;   // The function returns the product of p1 and p2
    };

    $('select[name=items], select[name=marca_id], select[name=tipo_id], select[name=color_id], select[name=deposito_id], select[name=anho_id], select[name=disponibilidad_id], select[name=sort_by]').change(function() {
        
        var URL_generado = generarURL();

        // Redirecciona las urls para filtrar
        window.location = "{{ URL::route('r_vehiculos_index') }}" + URL_generado;
    });

    $('a[name=exportar_xls]').click(function() {
        
        var URL_generado = generarURL();

        // Redirecciona las urls para filtrar
        window.location = "{{ URL::route('r_vehiculos_xls') }}" + URL_generado;
    });

    $('a[name=exportar_pdf]').click(function() {
        
        var URL_generado = generarURL();

        // Redirecciona las urls para filtrar
        window.location = "{{ URL::route('r_vehiculos_pdf') }}" + URL_generado;
    });

</script>

@endsection