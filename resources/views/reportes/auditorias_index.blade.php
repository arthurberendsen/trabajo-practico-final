@section ('contenido')

<div class="container-fluid">
    @if (session('successMessage'))
        <div class="alert alert-success" role="alert">
           {{ session('successMessage') }}
        </div>
    @endif

    @if (session('errorMessage'))
        <div class="alert alert-danger" role="alert">
           {{ session('errorMessage') }}
        </div>
    @endif

    <h3 class="text-dark mb-4">Reporte de Auditoría</h3>
    <div class="card shadow">
        <div class="card-header py-3">
            <div class="row">
                <h4 class="col-3 text-primary m-2 font-weight-bold">Filtrar Reporte</h4>  

                <div class="col-4 text-md-right text-nowrap">
                    <a class="btn btn-sm btn-outline-danger ml-3" href="#" name="exportar_pdf" id="exportar_pdf"><i class="far fa-file-pdf" style="font-size: 14px;"></i> Exportar</a>
                    <a class="btn btn-sm btn-outline-success ml-3" href="#" name="exportar_xls" id="exportar_xls"><i class="far fa-file-excel" style="font-size: 14px;"></i> Exportar</a>
                </div>  
                <div class="col-2 text-md-right text-nowrap">
                    <label>Ordenar por  <select class="form-control form-control-sm custom-select custom-select-sm" id="sort_by" name="sort_by" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if($sort_by = '') selected @endif>Elegir Campo...</option>
                                <option value="usuario" @if($sort_sel == 'usuario') selected @endif>Usuario</option>
                                <option value="tabla" @if($sort_sel == 'tabla') selected @endif>Tabla</option>
                                <option value="tipo" @if($sort_sel == 'tipo') selected @endif>Tipo Transacción</option>
                                <option value="fecha" @if($sort_sel == 'fecha') selected @endif>Fecha</option>
                            </select>
                    </label>
                </div>
                <div class="col text-md-right text-nowrap">
                    <label>Mostrar Items  <select class="form-control form-control-sm custom-select custom-select-sm" id="pagination" name="items" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="10" @if($items == 10) selected @endif>10</option>
                                <option value="25" @if($items == 25) selected @endif>25</option>
                                <option value="50" @if($items == 50) selected @endif>50</option>
                                <option value="100" @if($items == 100) selected @endif>100</option>
                            </select>
                    </label>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-2">
                    <div id="dataTable_length" class="dataTables_length text-nowrap" aria-controls="dataTable">
                        <label>Usuario  
                            <select class="form-control form-control-sm custom-select custom-select-sm" id="usuario_id" name="usuario_id" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if ($usuario_id = '') selected @endif>Filtrar por usuario...</option>
                                @foreach($usuarios as $usuario)
                                    <option value="{{ $usuario->id }}" @if($usuario_sel == $usuario->id) selected @endif>{{ $usuario->nombre }}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                </div>
                <div class="col-3">
                    <div id="dataTable_length" class="dataTables_length text-nowrap" aria-controls="dataTable">
                        <label>Tipo Transacción  
                            <select class="form-control form-control-sm custom-select custom-select-sm" id="tipo_id" name="tipo_id" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if ($tipo_id = '') selected @endif>Filtrar por tipo de transacción...</option>
                                    <option value="Actualizar" @if($tipo_sel == "Actualizar") selected @endif>Actualizar</option>
                                    <option value="Borrar" @if($tipo_sel == "Borrar") selected @endif>Borrar</option>
                                    <option value="Insertar" @if($tipo_sel == "Insertar") selected @endif>Insertar</option>
                            </select>
                        </label>
                    </div>
                </div>

                <div class="col-2">
                    <div id="dataTable_length" class="dataTables_length text-nowrap" aria-controls="dataTable">
                        <label>Tabla  
                            <select class="form-control form-control-sm custom-select custom-select-sm" id="tabla_id" name="tabla_id" style="font-size: 11px; height: 24px; width: 140px;">
                                <option value="" @if ($tabla_id = '') selected @endif>Filtrar por tabla...</option>
                                @foreach($tablas as $tabla)
                                    <option value="{{ $tabla->nombre }}" @if($tabla_sel == $tabla->nombre) selected @endif>{{ $tabla->nombre }}</option>
                                @endforeach
                            </select>
                        </label>
                    </div>
                </div>

                <div class="col-3">
                    <form class="form-inline">
                        <div class="form-group"><label for="fecha_factura">Fecha&nbsp;&nbsp;</label>
                            <input type="text" class="form-control form-control-sm datepicker" name="fecha_desde" id="fecha_desde" value="@if($fecha_desde_sel != '') {{ $fecha_desde_sel }} @endif" style="font-size: 11px; height: 24px; width: 140px;">&nbsp; y &nbsp;
                            <input type="text" class="form-control form-control-sm datepicker" name="fecha_hasta" id="fecha_hasta"value="@if($fecha_hasta_sel != '') {{ $fecha_hasta_sel }} @endif" style="font-size: 11px; height: 24px; width: 140px;">
                        </div>
                    </form>
                </div>

            </div>
            <div class="table-responsive table mt-2 table-striped" id="dataTable" role="grid" aria-describedby="dataTable_info">
                <table class="table my-0 table-hover" id="dataTable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Usuario</th>
                            <th>Tipo</th>
                            <th>Tabla</th>
                            <th>PK de la Tabla</th>
                            <th>Campo</th>
                            <th>Valor Viejo</th>
                            <th>Valor Nuevo</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = $auditorias->firstItem(); ?>

                        @foreach($auditorias as $auditoria)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $auditoria->usuario }}</td>
                            <td>{{ $auditoria->tipo }}</td>
                            <td>{{ $auditoria->tabla }}</td>
                            <td>{{ $auditoria->pk }}</td>
                            <td>{{ $auditoria->campo }}</td>
                            <td>{{ $auditoria->valor_viejo }}</td>
                            <td>{{ $auditoria->valor_nuevo }}</td>
                            <td>{{ date("d/m/Y", strtotime($auditoria->fecha)) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><strong>#</strong></td>
                            <td><strong>Usuario</strong></td>
                            <td><strong>Tipo</strong></td>
                            <td><strong>Tabla</strong></td>
                            <td><strong>PK de la Tabla</strong></td>
                            <td><strong>Campo</strong></td>
                            <td><strong>Valor Viejo</strong></td>
                            <td><strong>Valor Nuevo</strong></td>
                            <td><strong>Fecha</strong></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
            <div class="row">
                <div class="col-md-6 align-self-center">
                    <p id="dataTable_info" class="dataTables_info" role="status" aria-live="polite">Mostrando el registro {{$auditorias->firstItem() ?? 0 }} al {{$auditorias->lastItem() ?? 0 }} de {{$auditorias->total()}} registros</p>

                </div>

                <div class="col-md-6">
                    <nav class="d-lg-flex justify-content-lg-end dataTables_paginate paging_simple_numbers">
                        {{ $auditorias->appends(compact('items'))->links() }}
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function generarURL() { //Funcion para capturar los valores de todos los select's

        var Items = $('select[name=items]').val();
        var UsuarioId = $('select[name=usuario_id]').val();
        var TipoId = $('select[name=tipo_id]').val();
        var TablaId = $('select[name=tabla_id]').val();
        
        var FechaDesde = $('input[name=fecha_desde]').val();
        var FechaHasta = $('input[name=fecha_hasta]').val();

        var SortBy = $('select[name=sort_by]').val();

        var URL = "";

        if($.trim(Items).length) {
            URL += "?items=" + Items; }
        
        if($.trim(UsuarioId).length) {
            URL += "&usuario_id=" + UsuarioId; }

        if($.trim(TipoId).length) {
            URL += "&tipo_id=" + TipoId; }

        if($.trim(TablaId).length) {
            URL += "&tabla_id=" + TablaId; }

        if($.trim(FechaDesde).length) {
            URL += "&fecha_desde=" + FechaDesde; }
        
        if($.trim(FechaHasta).length) {
            URL += "&fecha_hasta=" + FechaHasta; }

        
        if($.trim(SortBy).length) {
            URL += "&sort_by=" + SortBy; }

        return URL;   // The function returns the product of p1 and p2
    };

    $('select[name=items], select[name=usuario_id], select[name=tipo_id], select[name=tabla_id], select[name=sort_by], input[name=fecha_desde], input[name=fecha_hasta]').change(function() {
        
        var URL_generado = generarURL();

        // Redirecciona las urls para filtrar
        window.location = "{{ URL::route('r_auditorias_index') }}" + URL_generado;
    });

    $('a[name=exportar_xls]').click(function() {
        
        var URL_generado = generarURL();

        // Redirecciona las urls para filtrar
        window.location = "{{ URL::route('r_auditorias_xls') }}" + URL_generado;
    });

    $('a[name=exportar_pdf]').click(function() {
        
        var URL_generado = generarURL();

        // Redirecciona las urls para filtrar
        window.location = "{{ URL::route('r_auditorias_pdf') }}" + URL_generado;
    });

    $('.datepicker').datepicker({
        clearBtn: true,
        language: "es",
        autoclose: true,
        todayHighlight: true
    });

</script>

@endsection