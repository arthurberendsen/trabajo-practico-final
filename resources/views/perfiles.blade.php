@extends ('layouts.main')

@section ('contenido')

	// PERFIL
    @if ($var == 'perfil_index')
        @include ('perfiles.perfil_index')
    @endif

    @if ($var == 'perfil_show')
        @include ('perfiles.perfil_show')
    @endif

    @if ($var == 'perfil_create')
        @include ('perfiles.perfil_create')
    @endif

    @if ($var == 'perfil_edit')
        @include ('perfiles.perfil_edit')
    @endif

@endsection