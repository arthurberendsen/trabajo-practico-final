@extends ('layouts.main')

@section ('contenido')

    //FACTURAS DE VENTAS

    @if ($var == 'facturasv_index')
        @include ('ventas.facturasv_index')
    @endif

    @if ($var == 'facturasv_show')
        @include ('ventas.facturasv_show')
    @endif

    @if ($var == 'facturasv_create')
        @include ('ventas.facturasv_create')
    @endif

    @if ($var == 'facturasv_edit')
        @include ('ventas.facturasv_edit')
    @endif

     // CLIENTES

    @if ($var == 'clientes_index')
        @include ('ventas.clientes_index')
    @endif

    @if ($var == 'clientes_show')
        @include ('ventas.clientes_show')
    @endif

    @if ($var == 'clientes_create')
        @include ('ventas.clientes_create')
    @endif

    @if ($var == 'clientes_edit')
        @include ('ventas.clientes_edit')
    @endif

    //PAGARES

    @if ($var == 'pagares_index')
        @include ('ventas.pagares_index')
    @endif

    @if ($var == 'pagares_show')
        @include ('ventas.pagares_show')
    @endif

    //CUOTAS DE VENTAS

    @if ($var == 'cuotasv_index')
        @include ('ventas.cuotasv_index')
    @endif

    @if ($var == 'cuotasv_show')
        @include ('ventas.cuotasv_show')
    @endif

    @if ($var == 'cuotasv_create')
        @include ('ventas.cuotasv_create')
    @endif

    @if ($var == 'cuotasv_edit')
        @include ('ventas.cuotasv_edit')
    @endif

    //RECIBOS DE VENTAS

    @if ($var == 'recibosv_index')
        @include ('ventas.recibosv_index')
    @endif

    @if ($var == 'recibosv_show')
        @include ('ventas.recibosv_show')
    @endif

    @if ($var == 'recibosv_create')
        @include ('ventas.recibosv_create')
    @endif

    @if ($var == 'recibosv_edit')
        @include ('ventas.recibosv_edit')
    @endif

@endsection