@extends ('layouts.main')

@section ('contenido')

    // VEHICULOS
    @if ($var == 'r_vehiculos_index')
        @include ('reportes.vehiculos_index')
    @endif

    // VENTAS
    @if ($var == 'r_ventas_index')
        @include ('reportes.ventas_index')
    @endif

    // COMPRAS
    @if ($var == 'r_compras_index')
        @include ('reportes.compras_index')
    @endif

    // AUDITORIA
    @if ($var == 'r_auditorias_index')
        @include ('reportes.auditorias_index')
    @endif

@endsection