@section ('contenido')

<script>
    console.log("Jquery version: " + jQuery().jquery);

    function readURL(input, id) {
        id = id || '#file-image';
        
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(id).attr('src', e.target.result);
            };
            
            reader.readAsDataURL(input.files[0]);
            $('#file-image').removeClass('hidden');
            //$('#start').hide();
        };
    };
</script> 

<!-- INICIO MODALES -->

<div role="dialog" tabindex="-1" class="modal fade" id="modal_descartar">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Descartar Cambios</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <p>¿Desea realmente descartar los cambios realizados?</p>
            </div>
            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">No</button><a class="btn btn-primary" role="button" href="{{ route('inicio') }}">Si</a></div>
        </div>
    </div>
</div>

<div role="dialog" tabindex="-1" class="modal fade" id="modalborrarimagen">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Borrar Avatar</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button></div>
            <div class="modal-body">
                <p>¿Esta seguro que desea borrar el Avatar de su usuario?</p>
            </div>
            <div class="modal-footer"><button class="btn btn-light" type="button" data-dismiss="modal">Cancelar</button>
            
            <a class="btn btn-danger" href="{{ route('perfil_index', $usuario->id) }}" onclick="event.preventDefault(); document.getElementById('delete-form-{{$usuario->id}}').submit();">Borrar</a></div>
            
            <form id="delete-form-{{$usuario->id}}" + action="{{route('avatar_destroy', $usuario->id)}}" method="post"> 
                @csrf @method('DELETE')
            </form>
        </div>
    </div>
</div>

<!-- FIN MODALES -->

<div class="container-fluid">
    @if (session('successMessage'))
        <div class="alert alert-success" role="alert">
           {{ session('successMessage') }}
        </div>
    @endif

    @if (session('errorMessage'))
        <div class="alert alert-danger" role="alert">
           {{ session('errorMessage') }}
        </div>
    @endif

    <h3 class="text-dark mb-4">Perfil</h3>
    <div class="row mb-3">
        <div class="col-lg-4">
            <div class="card mb-3">
                <div class="card-body text-center shadow">
                    <!-- <img class="rounded-circle mb-3 mt-4 img-thumbnail" src="{{ asset('public/bstudio/img/avatars/bigote_avatar.jpg') }}" width="160" height="160" />
                    <div class="mb-3">
                        <button class="btn btn-primary btn-sm" type="button">Cambiar Foto</button>
                    </div> -->

                    <form id="subir-avatar-form" action="{{ route('avatar_update', $usuario->id) }}" method="POST" accept-charset="utf-8" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    
                        <img id="file-image" src="{{asset($usuario->avatar)}}" alt="Preview" class="hidden rounded-circle mb-3 mt-4 img-thumbnail" width="160" height="160">
                        <div class="mb-3">
                            <label for="getFile" class="btn btn-secondary btn-sm">Elegir Imagen...</label>
                        </div>
                    
                        <div class="mb-3">
                            <a class="btn btn-danger btn-sm mr-2" href="javascript:;" role="button" data-toggle="modal" data-id="{{ $usuario->id }}" data-target="#modalborrarimagen">Borrar Imagen</a>
                            <button class="btn btn-primary btn-sm" type="submit">Subir Imagen</button>
                            <input type='file' id="getFile" style="display: none;" name="fileUpload" accept="image/*" onchange="readURL(this);" >
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="row">
                <div class="col">
                    <div class="card shadow mb-3">
                        <div class="card-header py-3">
                            <p class="text-primary m-0 font-weight-bold">Configuración del Usuario</p>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('perfil_update', $usuario->id) }}" method="POST">
                                    {{ csrf_field() }}
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label for="username"><strong>Nombre Completo</strong></label><input class="form-control" type="text" placeholder="user.name" name="nombre" value="{{ $usuario->nombre }}" /></div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label for="mail"><strong>Correo Electrónico</strong></label><input class="form-control" type="mail" placeholder="user.mail" name="email" value="{{ $usuario->email }}" /></div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label for="password"><strong>Contraseña</strong></label><input class="form-control" type="password" name="password"></div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group"><label for="repassword"><strong>Repetir su Contraseña</strong></label><input class="form-control" type="password" name="repassword"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary btn-sm" type="submit">Guardar Cambios</button>
                                    <a class="btn btn-danger btn-sm ml-3" href="#" data-toggle="modal" data-target="#modal_descartar">Descartar</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection