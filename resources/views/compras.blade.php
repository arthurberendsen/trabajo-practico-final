@extends ('layouts.main')

@section ('contenido')

    //FACTURAS DE COMPRAS

    @if ($var == 'facturasc_index')
        @include ('compras.facturasc_index')
    @endif

    @if ($var == 'facturasc_show')
        @include ('compras.facturasc_show')
    @endif

    @if ($var == 'facturasc_create')
        @include ('compras.facturasc_create')
    @endif

    @if ($var == 'facturasc_edit')
        @include ('compras.facturasc_edit')
    @endif

     // PROVEEDORES

    @if ($var == 'proveedores_index')
        @include ('compras.proveedores_index')
    @endif

    @if ($var == 'proveedores_show')
        @include ('compras.proveedores_show')
    @endif

    @if ($var == 'proveedores_create')
        @include ('compras.proveedores_create')
    @endif

    @if ($var == 'proveedores_edit')
        @include ('compras.proveedores_edit')
    @endif

    //CUOTAS DE COMPRAS

    @if ($var == 'cuotasc_index')
        @include ('compras.cuotasc_index')
    @endif

    @if ($var == 'cuotasc_show')
        @include ('compras.cuotasc_show')
    @endif

    @if ($var == 'cuotasc_create')
        @include ('compras.cuotasc_create')
    @endif

    @if ($var == 'cuotasc_edit')
        @include ('compras.cuotasc_edit')
    @endif

    //RECIBOS DE COMPRAS

    @if ($var == 'recibosc_index')
        @include ('compras.recibosc_index')
    @endif

    @if ($var == 'recibosc_show')
        @include ('compras.recibosc_show')
    @endif

    @if ($var == 'recibosc_create')
        @include ('compras.recibosc_create')
    @endif

    @if ($var == 'recibosc_edit')
        @include ('compras.recibosc_edit')
    @endif

@endsection