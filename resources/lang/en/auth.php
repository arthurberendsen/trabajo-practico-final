<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'El correo y/o la contraseña no son correctas.',
    'password' => 'La constraseña no es correcta.',
    'throttle' => 'Demasiados intentos de Inicio de Sesión. Por favor intentelo nuevamente en :seconds segundos.',

];
